"""
start: voxelize meshes v.2
this script turns the selected meshes into cubes
over the current timeline range.

From Maya-Python mailing list
http://zoomy.net/2010/02/25/voxelize-meshes-script-v2
http://groups.google.com/group/python_inside_maya/browse_thread/thread/3f973f5bddf35e15#

TODO: Implement Dean's critique of this module
the overall algorithm being used to voxelize the mesh is flawed.
It shoots rays along the *edges* of the voxels then places the *centers* of the
cubes it creates at the intersection points, That guarantees a halo of 
half-empty voxels around the mesh.
Also, the shooting of rays is pretty hit-or-miss. There could be lots of 
geometry within a given voxel but none of it happens to lie along the ray, 
leading to that voxel being incorrectly discarded. A better algorithm would 
be to iterate over the mesh's triangles and clip them against the voxel boundaries,
which is trivial given that the voxels are axis-aligned.
"""

import pymel.core.animation as animation
import pymel.api as api
import maya.cmds as cmds
import pymel.core.context as context
import pymel.core.datatypes as datatypes
import pymel.core.datatypes as dt
import pymel.core.effects as effects
import functools
import pymel.core.general as general
import inspect
import itertools
import pymel.core.language as language
import logging
import pymel.core.modeling as modeling
import pymel.core.nodetypes as nodetypes
import pymel.core.nodetypes as nt
import maya.OpenMaya as om
import os
import pymel.core.other as other
import re
import pymel.core.rendering as rendering
import pymel.core.runtime as runtime
import sys
import pymel.core.system as system
import traceback
import pymel.core.uitypes as ui
import pymel.core.uitypes as uitypes
import pymel.util as util
import pymel.versions as versions
import pymel.core.windows as windows

from pymel.core.general import *
from pymel.core.animation import *
from pymel.core.other import *
from pymel.util.path import *
from pymel.core.context import *
from maya._OpenMaya import *
from pymel.core.modeling import *
from pymel.core.rendering import *
from pymel.internal.pwarnings import *
from pymel.util.scanf import *
from pymel.core.uitypes import *
from pymel.core.effects import *
from pymel.core.windows import *
from pymel.core.system import *
from pymel.util.decoration import *
from pymel.core.language import *
from pymel.internal.pmcmds import *

def voxelize(cubeSize=None):
    """
    voxelize the currently selected mesh
    \param cubeSize float size of voxels
    """

    pass

__all__ = ['voxelize']
