import logging
import pymel.all as pymel
import sys

from logging import *

class LoggingMenu(pymel.Menu):
    def __init__(self, name=None, parent=None):
        pass
    
    
    def addHandler(self, logger):
        pass
    
    
    def buildLevelMenu(self, parent, item):
        pass
    
    
    def buildSubMenu(self, parent, logger):
        pass
    
    
    def changeLevel(self, item, level):
        pass
    
    
    def refresh(self, *args):
        pass
    
    
    def refreshLoggingMenu(self):
        pass
    
    
    def setFormatter(self, handler):
        pass
    
    
    def __new__(cls, name='pymelLoggingControl', parent=None):
        pass
    
    
    __melui__ = None
    
    
    __readonly__ = None

def initMenu():
    pass


def refreshLoggerHierarchy():
    pass

levelsDict = None

logLevelNames = None

logger = None

n = None
