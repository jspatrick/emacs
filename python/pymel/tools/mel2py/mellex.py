"""
# ----------------------------------------------------------------------
# clex.py
#
# A lexer for ANSI C.
# ----------------------------------------------------------------------
"""

import pymel.util.external.ply.lex as lex
import sys

def t_CAPTURE(t):
    """
    `
    """

    pass


def t_COMMENT(t):
    """
    //.*
    """

    pass


def t_COMMENT_BLOCK(t):
    """
    /\*(.|\n)*?\*/|/\*(.|\n)*?$
    """

    pass


def t_COMPONENT(t):
    """
    \.[xyz]
    """

    pass


def t_ELLIPSIS(t):
    """
    \.\.
    """

    pass


def t_ID(t):
    """
    ([|]?([:]?([.]?[A-Za-z_][\w]*)+)+)+?
    """

    pass


def t_LBRACKET(t):
    """
    \[
    """

    pass


def t_LPAREN(t):
    """
    \(
    """

    pass


def t_NEWLINE(t):
    """
    \n+|\r+
    """

    pass


def t_RBRACKET(t):
    """
    \]
    """

    pass


def t_RPAREN(t):
    """
    \)
    """

    pass


def t_SEMI(t):
    """
    ;
    """

    pass


def t_VAR(t):
    """
    \$[A-Za-z_][\w_]*
    """

    pass

id_state = None

r = None

reserved = None

reserved_map = None

suspend_depth = None

t_COLON = None

t_COMMA = None

t_CONDOP = None

t_CROSS = None

t_CROSSEQUAL = None

t_DIVEQUAL = None

t_DIVIDE = None

t_EQ = None

t_EQUALS = None

t_FCONST = None

t_GE = None

t_GT = None

t_ICONST = None

t_LAND = None

t_LBRACE = None

t_LE = None

t_LOR = None

t_LT = None

t_LVEC = None

t_MINUS = None

t_MINUSEQUAL = None

t_MINUSMINUS = None

t_MOD = None

t_MODEQUAL = None

t_NE = None

t_NOT = None

t_PLUS = None

t_PLUSEQUAL = None

t_PLUSPLUS = None

t_RBRACE = None

t_RVEC = None

t_SCONST = None

t_TIMES = None

t_TIMESEQUAL = None

t_ignore = None

tokens = None
