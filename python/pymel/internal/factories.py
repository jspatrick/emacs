import exceptions

import pymel.api as api
import apicache
import cmdcache
import maya.cmds as cmds
import inspect
import maya.mel as mm
import os
import plogging
import pmcmds
import re
import sys
import textwrap
import time
import types
import pymel.util as util
import pymel.versions as versions

from operator import *
from pymel.internal.apicache import *
from logging import *
from pymel.util.conditions import *
from pymel.internal.cmdcache import *

class ApiTypeRegister(object):
    """
    "
    Use this class to register the classes and functions used to wrap api methods.
    
    there are 4 dictionaries of functions maintained by this class:
        - inCast : for casting input arguments to a type that the api method expects
        - outCast: for casting the result of the api method to a type that pymel expects (outCast expect two args (self, obj) )
        - refInit: for initializing types passed by reference or via pointer
        - refCast: for casting the pointers to pymel types after they have been passed to the method
    
    To register a new type call `ApiTypeRegister.register`.
    """
    
    
    
    def getPymelType(cls, apiType):
        """
        We need a way to map from api name to pymelName.  we start by looking up types which are registered
        and then fall back to naming convention for types that haven't been registered yet. Perhaps pre-register
        the names?
        """
    
        pass
    
    
    def isRegistered(cls, apiTypeName):
        pass
    
    
    def register(cls, apiTypeName, pymelType, inCast=None, outCast=None, apiArrayItemType=None):
        """
        pymelType is the type to be used internally by pymel.  apiType will be hidden from the user
        and converted to the pymel type.
        apiTypeName is the name of an apiType as a string
        if apiArrayItemType is set, it should be the api type that represents each item in the array
        """
    
        pass
    
    
    __dict__ = None
    
    __weakref__ = None
    
    arrayItemTypes = None
    
    
    doc = None
    
    
    inCast = None
    
    
    outCast = None
    
    
    refCast = None
    
    
    refInit = None
    
    
    types = None


class Flag(Condition):
    def __init__(self, longName, shortName, truthValue=True):
        """
        Conditional for evaluating if a given flag is present.
        
        Will also check that the given flag has the required
        truthValue (True, by default). If you don't care
        about the truthValue (ie, you want to have the condition
        evaluate to true as long as the flag is present),
        set truthValue to None.
        """
    
        pass
    
    
    def __str__(self):
        pass
    
    
    def eval(self, kwargs):
        pass


MetaMayaTypeWrapper = None

class ApiUndoItem(object):
    """
    A simple class that reprsents an undo item to be undone or redone.
    """
    
    
    
    def __init__(self, setter, redoArgs, undoArgs):
        pass
    
    
    def redoIt(self):
        pass
    
    
    def undoIt(self):
        pass


class ApiArgUtil(object):
    def __init__(self, apiClassName, methodName, methodIndex=0):
        """
        If methodInfo is None, then the methodIndex will be used to lookup the methodInfo from apiClassInfo
        """
    
        pass
    
    
    def argInfo(self):
        pass
    
    
    def argList(self):
        pass
    
    
    def canBeWrapped(self):
        pass
    
    
    def castInput(self, argName, input, cls):
        pass
    
    
    def castReferenceResult(self, argtype, outArg):
        pass
    
    
    def castResult(self, instance, result):
        pass
    
    
    def fromInternalUnits(self, result, instance=None):
        pass
    
    
    def getDefaults(self):
        """
        get a list of defaults
        """
    
        pass
    
    
    def getGetterInfo(self):
        pass
    
    
    def getInputTypes(self):
        pass
    
    
    def getMethodDocs(self):
        pass
    
    
    def getOutputTypes(self):
        pass
    
    
    def getPrototype(self, className=True, methodName=True, outputs=False, defaults=False):
        pass
    
    
    def getPymelName(self):
        pass
    
    
    def getReturnType(self):
        pass
    
    
    def hasOutput(self):
        pass
    
    
    def inArgs(self):
        pass
    
    
    def initReference(self, argtype):
        pass
    
    
    def isDeprecated(self):
        pass
    
    
    def isStatic(self):
        pass
    
    
    def iterArgs(self, inputs=True, outputs=True, infoKeys=[]):
        pass
    
    
    def outArgs(self):
        pass
    
    
    def toInternalUnits(self, arg, input):
        pass
    
    
    def isValidEnum(enumTuple):
        pass
    
    
    __dict__ = None
    
    __weakref__ = None


class ApiUndo(object):
    """
    this is based on a clever prototype that Dean Edmonds posted on python_inside_maya
    awhile back.  it works like this:
    
        - using the API, create a garbage node with an integer attribute,
          lock it and set it not to save with the scene.
        - add an API callback to the node, so that when the special attribute
          is changed, we get a callback
        - the API queue is a list of simple python classes with undoIt and
          redoIt methods.  each time we add a new one to the queue, we increment
          the garbage node's integer attribute using maya.cmds.
        - when maya's undo or redo is called, it triggers the undoing or
          redoing of the garbage node's attribute change (bc we changed it using
          MEL/maya.cmds), which in turn triggers our API callback.  the callback
          runs the undoIt or redoIt method of the class at the index taken from
          the numeric attribute.
    """
    
    
    
    def __init__(self):
        pass
    
    
    def append(self, cmdObj):
        pass
    
    
    def execute(self, cmdObj, args):
        pass
    
    
    def flushUndo(self, *args):
        pass
    
    
    def __new__(cls, *p, **k):
        """
        # redefine __new__
        """
    
        pass
    
    
    __dict__ = None
    
    __weakref__ = None


class CallbackError(RuntimeError):
    def __init__(self, callback, origException=None):
        pass
    
    
    __weakref__ = None


_MetaMayaCommandWrapper = None

MetaMayaComponentWrapper = None

MetaMayaNodeWrapper = None

MetaMayaUIWrapper = None
def addApiDocs(apiClass, methodName, overloadIndex=None, undoable=True):
    """
    decorator for adding API docs
    """

    pass


def addApiDocsCallback(apiClass, methodName, overloadIndex=None, undoable=True, origDocstring=''):
    pass


def addCmdDocsCallback(cmdName, docstring=''):
    pass


def addFlagCmdDocsCallback(cmdName, flag, docstring):
    pass


def addMayaType(mayaType, apiType=None):
    """
    Add a type to the MayaTypes lists. Fill as many dictionary caches as we have info for.
    
    - mayaTypesToApiTypes
    - mayaTypesToApiEnums
    """

    pass


def addMelDocs(cmdName, flag=None):
    """
    decorator for adding docs
    """

    pass


def addPyNode(dynModule, mayaType, parentMayaType):
    pass


def addPyNodeCallback(dynModule, mayaType, pyNodeTypeName, parentPyNodeTypeName):
    pass


def addPyNodeType(pyNodeType, parentPyNode):
    pass


def apiClassNameToPymelClassName(apiName, allowGuess=True):
    """
    Given the name of an api class, such as MFnTransform, MSpace, MAngle,
    returns the name of the corresponding pymel class.
    
    If allowGuessing, and we cannot find a registered type that matches, will
    try to do string parsing to guess the pymel name.
    
    Returns None if it was unable to determine the name.
    """

    pass


def clearPyNodeTypes():
    pass


def createFunctions(moduleName, returnFunc=None):
    pass


def createflag(cmdName, flag):
    """
    create flag decorator
    """

    pass


def editflag(cmdName, flag):
    """
    edit flag decorator
    """

    pass


def fixCallbacks(inFunc, commandFlags, funcName=None):
    """
    Prior to maya 2011, when a user provides a custom callback functions for a
    UI elements, such as a checkBox, when the callback is triggered it is passed
    a string instead of a real python values.
    
    For example, a checkBox changeCommand returns the string 'true' instead of
    the python boolean True. This function wraps UI commands to correct the
    problem and also adds an extra flag to all commands with callbacks called
    'passSelf'.  When set to True, an instance of the calling UI class will be
    passed as the first argument.
    
    if inFunc has been renamed, pass a funcName to lookup command info in apicache.cmdlist
    """

    pass


def functionFactory(funcNameOrObject, returnFunc=None, module=None, rename=None, uiWidget=False):
    """
    create a new function, apply the given returnFunc to the results (if any),
    and add to the module given by 'moduleName'.  Use pre-parsed command documentation
    to add to __doc__ strings for the command.
    """

    pass


def getComponentTypes():
    """
    # Keep around for debugging/info gathering...
    """

    pass


def getInheritance(mayaType):
    """
    Get parents as a list, starting from the node after dependNode, and ending with the mayaType itself.
    To get the inheritance we use nodeType, which requires a real node.  To do get these without poluting the scene
    we use a dag/dg modifier, call the doIt method, get the lineage, then call undoIt.
    """

    pass


def getUncachedCmds():
    pass


def isValidPyNode(arg):
    pass


def isValidPyNodeName(arg):
    pass


def listForNoneQuery(res, kwargs, flags):
    """
    convert a None to an empty list on the given query flags
    """

    pass


def loadApiCache():
    pass


def loadCmdCache():
    pass


def loadCmdDocCache():
    pass


def makeCreateFlagMethod(inFunc, flag, newMethodName=None, docstring='', cmdName=None, returnFunc=None):
    pass


def makeEditFlagMethod(inFunc, flag, newMethodName=None, docstring='', cmdName=None):
    pass


def makeQueryFlagMethod(inFunc, flag, newMethodName=None, docstring='', cmdName=None, returnFunc=None):
    pass


def mayaTypeToApiType(mayaType):
    """
    Get the Maya API type from the name of a Maya type
    """

    pass


def mergeApiClassOverrides():
    pass


def queryflag(cmdName, flag):
    """
    query flag decorator
    """

    pass


def raiseError(typ, *args):
    pass


def registerVirtualClass(cls, nameRequired=False):
    """
    Allows a user to create their own subclasses of leaf PyMEL node classes,
    which are returned by `general.PyNode` and all other pymel commands.
    
    The process is fairly simple:
        1.  Subclass a pymel node class.  Be sure that it is a leaf class, meaning that it represents an actual Maya node type
            and not an abstract type higher up in the hierarchy.
        2.  Register your subclass by calling this function
    
    :type  nameRequired: bool
    :param nameRequired: True if the _isVirtual callback requires the string name to operate on. The object's name is not always immediately
        avaiable and may take an extra calculation to retrieve.
    """

    pass


def removeMayaType(mayaType):
    """
    Remove a type from the MayaTypes lists.
    
    - mayaTypesToApiTypes
    - mayaTypesToApiEnums
    """

    pass


def removePyNode(dynModule, mayaType):
    pass


def removePyNodeType(pyNodeTypeName):
    pass


def saveApiCache():
    pass


def saveApiMelBridgeCache():
    pass


def splitToPyNodeList(res):
    """
    converts a whitespace-separated string of names to a list of PyNode objects
    """

    pass


def toApiFunctionSet(obj):
    pass


def toApiTypeEnum(obj, default=None):
    pass


def toApiTypeStr(obj, default=None):
    pass


def toPyNode(res):
    """
    returns a PyNode object
    """

    pass


def toPyNodeList(res):
    """
    returns a list of PyNode objects
    """

    pass


def toPyType(moduleName, objectName):
    """
    Returns a function which casts it's single argument to
    an object with the given name in the given module (name).
    
    The module / object are given as strings, so that the module
    may be imported when the function is called, to avoid
    making factories dependent on, say, pymel.core.general or
    pymel.core.uitypes
    """

    pass


def toPyTypeList(moduleName, objectName):
    """
    Returns a function which casts the members of it's iterable
    argument to the given class.
    """

    pass


def toPyUI(res):
    """
    returns a PyUI object
    """

    pass


def toPyUIList(res):
    """
    returns a list of PyUI objects
    """

    pass


def unwrapToPyNode(res):
    """
    unwraps a 1-item list, and returns a PyNode object
    """

    pass


def wrapApiMethod(apiClass, methodName, newName=None, proxy=True, overloadIndex=None):
    """
    create a wrapped, user-friendly API method that works the way a python method should: no MScriptUtil and
    no special API classes required.  Inputs go in the front door, and outputs come out the back door.
    
    
    Regarding Undo
    --------------
    
    The API provides many methods which are pairs -- one sets a value
    while the other one gets the value.  the naming convention of these
    methods follows a fairly consistent pattern.  so what I did was
    determine all the get and set pairs, which I can use to automatically
    register api undo items:  prior to setting something, we first *get*
    it's existing value, which we can later use to reset when undo is
    triggered.
    
    This API undo is only for PyMEL methods which are derived from API
    methods.  it's not meant to be used with plugins.  and since it just
    piggybacks maya's MEL undo system, it won't get cross-mojonated.
    
    Take `MFnTransform.setTranslation`, for example. PyMEL provides a wrapped copy of this as
    `Transform.setTranslation`.   when pymel.Transform.setTranslation is
    called, here's what happens in relation to undo:
    
        #. process input args, if any
        #. call MFnTransform.getTranslation() to get the current translation.
        #. append to the api undo queue, with necessary info to undo/redo
           later (the current method, the current args, and the current
           translation)
        #. call MFnTransform.setTranslation() with the passed args
        #. process result and return it
    
    
    :Parameters:
    
        apiClass : class
            the api class
        methodName : string
            the name of the api method
        newName : string
            optionally provided if a name other than that of api method is desired
        proxy : bool
            If True, then __apimfn__ function used to retrieve the proxy class. If False,
            then we assume that the class being wrapped inherits from the underlying api class.
        overloadIndex : None or int
            which of the overloaded C++ signatures to use as the basis of our wrapped function.
    """

    pass

Always = None

DOC_WIDTH = None

EXCLUDE_METHODS = None

apiClassInfo = None

apiClassNamesToPyNodeNames = None

apiClassOverrides = None

apiEnumsToApiTypes = None

apiEnumsToPyComponents = None

apiToMelData = None

apiTypesToApiClasses = None

apiTypesToApiEnums = None

apiUndo = None

classToMelMap = None

cmdlist = None

docCacheLoaded = None

includeDocExamples = None

mayaTypesToApiEnums = None

mayaTypesToApiTypes = None

moduleCmds = None

nodeCommandList = None

nodeHierarchy = None

nodeTypeToInfoCommand = None

overrideMethods = None

pyNodeNamesToPyNodes = None

pyNodeTypesHierarchy = None

reservedApiTypes = None

reservedMayaTypes = None

simpleCommandWraps = None

uiClassList = None

virtualClass = None
