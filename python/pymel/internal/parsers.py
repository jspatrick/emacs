import os
import platform
import plogging
import re
import pymel.util as util
import pymel.versions as versions

from logging import *
from pymel.mayautils import *
from HTMLParser import *
from pymel.util.external.BeautifulSoup import *

class NodeHierarchyDocParser(HTMLParser):
    def __init__(self, version=None):
        pass
    
    
    def handle_data(self, data):
        pass
    
    
    def handle_starttag(self, tag, attrs):
        pass
    
    
    def parse(self):
        pass


class CommandModuleDocParser(HTMLParser):
    def __init__(self, category, version=None):
        pass
    
    
    def handle_starttag(self, tag, attrs):
        pass
    
    
    def parse(self):
        pass


class ApiDocParser(object):
    def __init__(self, apiModule, version=None, verbose=False, enumClass="<type 'tuple'>"):
        pass
    
    
    def getClassFilename(self):
        pass
    
    
    def getOperatorName(self, methodName):
        pass
    
    
    def getPymelEnums(self, enumDict):
        """
        remove all common prefixes from list of enum values
        """
    
        pass
    
    
    def getPymelMethodNames(self):
        pass
    
    
    def handleEnumDefaults(self, default, type):
        pass
    
    
    def handleEnums(self, type):
        pass
    
    
    def isGetMethod(self):
        pass
    
    
    def isSetMethod(self):
        pass
    
    
    def parse(self, apiClassName):
        pass
    
    
    def xprint(self, *args):
        pass
    
    
    __dict__ = None
    
    __weakref__ = None
    
    DEPRECATED_MSG = None
    
    
    OBSOLETE_MSG = None


class CommandDocParser(HTMLParser):
    """
    #---------------------------------------------------------------
    #        Doc Parser
    #---------------------------------------------------------------
    """
    
    
    
    def __init__(self, command):
        pass
    
    
    def addFlagData(self, data):
        pass
    
    
    def endFlag(self):
        pass
    
    
    def handle_data(self, data):
        pass
    
    
    def handle_endtag(self, tag):
        pass
    
    
    def handle_entityref(self, name):
        pass
    
    
    def handle_starttag(self, tag, attrs):
        pass
    
    
    def startFlag(self, data):
        pass

def mayaDocsLocation(version=None):
    pass


def mayaIsRunning():
    """
    Returns True if maya.cmds have  False otherwise.
    
    Early in interactive startup it is possible for commands to exist but for Maya to not yet be initialized.
    
    :rtype: bool
    """

    pass


def printTree(tree, depth=0):
    pass

