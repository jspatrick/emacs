import gzip
import cPickle as pickle

def dump(object, filename, protocol=-1):
    """
    Save an compressed pickle to disk.
    """

    pass


def load(filename):
    """
    Load a compressed pickle from disk
    """

    pass

__all__ = ['dump', 'load']
