"""
# ----------------------------------------------------------------------
# ctokens.py
#
# Token specifications for symbols in ANSI C and C++.  This file is
# meant to be used as a library in other tokenizers.
# ----------------------------------------------------------------------
"""

def t_COMMENT(t):
    """
    /\*(.|\n)*?\*/
    """

    pass


def t_CPPCOMMENT(t):
    """
    //.*\n
    """

    pass

t_AND = None

t_ANDEQUAL = None

t_ARROW = None

t_CHARACTER = None

t_COLON = None

t_COMMA = None

t_DECREMENT = None

t_DIVEQUAL = None

t_DIVIDE = None

t_ELLIPSIS = None

t_EQ = None

t_EQUALS = None

t_FLOAT = None

t_GE = None

t_GT = None

t_ID = None

t_INCREMENT = None

t_INTEGER = None

t_LAND = None

t_LBRACE = None

t_LBRACKET = None

t_LE = None

t_LNOT = None

t_LOR = None

t_LPAREN = None

t_LSHIFT = None

t_LSHIFTEQUAL = None

t_LT = None

t_MINUS = None

t_MINUSEQUAL = None

t_MODEQUAL = None

t_MODULO = None

t_NE = None

t_NOT = None

t_OR = None

t_OREQUAL = None

t_PERIOD = None

t_PLUS = None

t_PLUSEQUAL = None

t_RBRACE = None

t_RBRACKET = None

t_RPAREN = None

t_RSHIFT = None

t_RSHIFTEQUAL = None

t_SEMI = None

t_STRING = None

t_TERNARY = None

t_TIMES = None

t_TIMESEQUAL = None

t_XOR = None

t_XOREQUAL = None

tokens = None
