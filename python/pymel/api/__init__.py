import maya.OpenMaya as OpenMaya
import maya.OpenMayaAnim as OpenMayaAnim
import maya.OpenMayaRender as OpenMayaRender
import maya.OpenMayaUI as OpenMayaUI
import allapi
import new
import pymel.util as util
import weakref

from maya._OpenMayaRender import *
from maya.OpenMayaFX import *
from maya._OpenMayaAnim import *
from pymel.api.allapi import *
from maya.OpenMayaUI import *
from maya._OpenMayaCloth import *
from maya.OpenMaya import *
from maya._OpenMaya import *
from maya.OpenMayaMPx import *
from maya.OpenMayaRender import *
from maya._OpenMayaUI import *
from maya.OpenMayaAnim import *
from maya._OpenMayaMPx import *
from maya.OpenMayaCloth import *
from maya._OpenMayaFX import *

def M3dView_active3dView(*args, **kwargs):
    pass


def M3dView_applicationShell(*args, **kwargs):
    pass


def M3dView_className(*args, **kwargs):
    pass


def M3dView_get3dView(*args, **kwargs):
    pass


def M3dView_getM3dViewFromModelEditor(*args, **kwargs):
    pass


def M3dView_getM3dViewFromModelPanel(*args, **kwargs):
    pass


def M3dView_numberOf3dViews(*args, **kwargs):
    pass


def M3dView_swigregister(*args, **kwargs):
    pass


def MAngle_className(*args, **kwargs):
    pass


def MAngle_internalToUI(*args, **kwargs):
    pass


def MAngle_internalUnit(*args, **kwargs):
    pass


def MAngle_setInternalUnit(*args, **kwargs):
    pass


def MAngle_setUIUnit(*args, **kwargs):
    pass


def MAngle_swigregister(*args, **kwargs):
    pass


def MAngle_uiToInternal(*args, **kwargs):
    pass


def MAngle_uiUnit(*args, **kwargs):
    pass


def MAnimControl_animationEndTime(*args, **kwargs):
    pass


def MAnimControl_animationStartTime(*args, **kwargs):
    pass


def MAnimControl_autoKeyMode(*args, **kwargs):
    pass


def MAnimControl_currentTime(*args, **kwargs):
    pass


def MAnimControl_globalInTangentType(*args, **kwargs):
    pass


def MAnimControl_globalOutTangentType(*args, **kwargs):
    pass


def MAnimControl_isPlaying(*args, **kwargs):
    pass


def MAnimControl_maxTime(*args, **kwargs):
    pass


def MAnimControl_minTime(*args, **kwargs):
    pass


def MAnimControl_playBackward(*args, **kwargs):
    pass


def MAnimControl_playForward(*args, **kwargs):
    pass


def MAnimControl_playbackBy(*args, **kwargs):
    pass


def MAnimControl_playbackMode(*args, **kwargs):
    pass


def MAnimControl_playbackSpeed(*args, **kwargs):
    pass


def MAnimControl_setAnimationEndTime(*args, **kwargs):
    pass


def MAnimControl_setAnimationStartEndTime(*args, **kwargs):
    pass


def MAnimControl_setAnimationStartTime(*args, **kwargs):
    pass


def MAnimControl_setAutoKeyMode(*args, **kwargs):
    pass


def MAnimControl_setCurrentTime(*args, **kwargs):
    pass


def MAnimControl_setGlobalInTangentType(*args, **kwargs):
    pass


def MAnimControl_setGlobalOutTangentType(*args, **kwargs):
    pass


def MAnimControl_setMaxTime(*args, **kwargs):
    pass


def MAnimControl_setMinMaxTime(*args, **kwargs):
    pass


def MAnimControl_setMinTime(*args, **kwargs):
    pass


def MAnimControl_setPlaybackBy(*args, **kwargs):
    pass


def MAnimControl_setPlaybackMode(*args, **kwargs):
    pass


def MAnimControl_setPlaybackSpeed(*args, **kwargs):
    pass


def MAnimControl_setViewMode(*args, **kwargs):
    pass


def MAnimControl_setWeightedTangents(*args, **kwargs):
    pass


def MAnimControl_stop(*args, **kwargs):
    pass


def MAnimControl_swigregister(*args, **kwargs):
    pass


def MAnimControl_viewMode(*args, **kwargs):
    pass


def MAnimControl_weightedTangents(*args, **kwargs):
    pass


def MAnimCurveChange_className(*args, **kwargs):
    pass


def MAnimCurveChange_swigregister(*args, **kwargs):
    pass


def MAnimCurveClipboardItemArray_className(*args, **kwargs):
    pass


def MAnimCurveClipboardItemArray_swigregister(*args, **kwargs):
    pass


def MAnimCurveClipboardItem_className(*args, **kwargs):
    pass


def MAnimCurveClipboardItem_swigregister(*args, **kwargs):
    pass


def MAnimCurveClipboard_swigregister(*args, **kwargs):
    pass


def MAnimCurveClipboard_theAPIClipboard(*args, **kwargs):
    pass


def MAnimMessage_addAnimCurveEditedCallback(*args, **kwargs):
    pass


def MAnimMessage_addAnimKeyframeEditCheckCallback(*args, **kwargs):
    pass


def MAnimMessage_addAnimKeyframeEditedCallback(*args, **kwargs):
    pass


def MAnimMessage_addNodeAnimKeyframeEditedCallback(*args, **kwargs):
    pass


def MAnimMessage_className(*args, **kwargs):
    pass


def MAnimMessage_flushAnimKeyframeEditedCallbacks(*args, **kwargs):
    pass


def MAnimMessage_swigregister(*args, **kwargs):
    pass


def MAnimUtil_className(*args, **kwargs):
    pass


def MAnimUtil_findAnimatedPlugs(*args, **kwargs):
    pass


def MAnimUtil_findAnimation(*args, **kwargs):
    pass


def MAnimUtil_isAnimated(*args, **kwargs):
    pass


def MAnimUtil_swigregister(*args, **kwargs):
    pass


def MArgDatabase_className(*args, **kwargs):
    pass


def MArgDatabase_swigregister(*args, **kwargs):
    pass


def MArgList_className(*args, **kwargs):
    pass


def MArgList_swigregister(*args, **kwargs):
    pass


def MArgParser_className(*args, **kwargs):
    pass


def MArgParser_swigregister(*args, **kwargs):
    pass


def MArrayDataBuilder_className(*args, **kwargs):
    pass


def MArrayDataBuilder_swigregister(*args, **kwargs):
    pass


def MArrayDataHandle_className(*args, **kwargs):
    pass


def MArrayDataHandle_swigregister(*args, **kwargs):
    pass


def MAttributeIndex_className(*args, **kwargs):
    pass


def MAttributeIndex_swigregister(*args, **kwargs):
    pass


def MAttributeSpecArray_className(*args, **kwargs):
    pass


def MAttributeSpecArray_swigregister(*args, **kwargs):
    pass


def MAttributeSpec_className(*args, **kwargs):
    pass


def MAttributeSpec_swigregister(*args, **kwargs):
    pass


def MBlendStateDesc_className(*args, **kwargs):
    pass


def MBlendStateDesc_swigregister(*args, **kwargs):
    pass


def MBlendState_className(*args, **kwargs):
    pass


def MBlendState_swigregister(*args, **kwargs):
    pass


def MBoundingBox_swigregister(*args, **kwargs):
    pass


def MCacheFormatDescription_className(*args, **kwargs):
    pass


def MCacheFormatDescription_swigregister(*args, **kwargs):
    pass


def MCallbackIdArray_className(*args, **kwargs):
    pass


def MCallbackIdArray_swigregister(*args, **kwargs):
    pass


def MCameraOverride_swigregister(*args, **kwargs):
    pass


def MCameraSetMessage_addCameraChangedCallback(*args, **kwargs):
    pass


def MCameraSetMessage_addCameraLayerCallback(*args, **kwargs):
    pass


def MCameraSetMessage_className(*args, **kwargs):
    pass


def MCameraSetMessage_swigregister(*args, **kwargs):
    pass


def MClearOperation_swigregister(*args, **kwargs):
    pass


def MClothConstraintBridge_swigregister(*args, **kwargs):
    pass


def MClothConstraintCmd_swigregister(*args, **kwargs):
    pass


def MClothConstraint_swigregister(*args, **kwargs):
    pass


def MClothControl_collisionCB(*args, **kwargs):
    pass


def MClothControl_currentSolver(*args, **kwargs):
    pass


def MClothControl_dgTimeGivenClothFrame(*args, **kwargs):
    pass


def MClothControl_externalForces(*args, **kwargs):
    pass


def MClothControl_getClothFromSolver(*args, **kwargs):
    pass


def MClothControl_getClothSystem(*args, **kwargs):
    pass


def MClothControl_getPrecedingFrame(*args, **kwargs):
    pass


def MClothControl_getSucceedingFrame(*args, **kwargs):
    pass


def MClothControl_getUVs(*args, **kwargs):
    pass


def MClothControl_interruptCheckCB(*args, **kwargs):
    pass


def MClothControl_isClothMesh(*args, **kwargs):
    pass


def MClothControl_isSolverCloth(*args, **kwargs):
    pass


def MClothControl_loadFrame(*args, **kwargs):
    pass


def MClothControl_panelFaces(*args, **kwargs):
    pass


def MClothControl_panelId(*args, **kwargs):
    pass


def MClothControl_pinch(*args, **kwargs):
    pass


def MClothControl_solveCB(*args, **kwargs):
    pass


def MClothControl_solverNode(*args, **kwargs):
    pass


def MClothControl_stepCB(*args, **kwargs):
    pass


def MClothControl_stitcherNode(*args, **kwargs):
    pass


def MClothControl_swigregister(*args, **kwargs):
    pass


def MClothEdge_swigregister(*args, **kwargs):
    pass


def MClothForce_swigregister(*args, **kwargs):
    pass


def MClothMaterial_swigregister(*args, **kwargs):
    pass


def MClothParticle_swigregister(*args, **kwargs):
    pass


def MClothPolyhedron_swigregister(*args, **kwargs):
    pass


def MClothSolverRegister_registerClothSolver(*args, **kwargs):
    pass


def MClothSolverRegister_swigregister(*args, **kwargs):
    pass


def MClothSolverRegister_unregisterClothSolver(*args, **kwargs):
    pass


def MClothSystem_swigregister(*args, **kwargs):
    pass


def MClothTriangle_swigregister(*args, **kwargs):
    pass


def MColorArray_className(*args, **kwargs):
    pass


def MColorArray_swigregister(*args, **kwargs):
    pass


def MColor_swigregister(*args, **kwargs):
    pass


def MCommandMessage_addCommandCallback(*args, **kwargs):
    pass


def MCommandMessage_addCommandOutputCallback(*args, **kwargs):
    pass


def MCommandMessage_addCommandOutputFilterCallback(*args, **kwargs):
    pass


def MCommandMessage_className(*args, **kwargs):
    pass


def MCommandMessage_swigregister(*args, **kwargs):
    pass


def MCommandResult_className(*args, **kwargs):
    pass


def MCommandResult_swigregister(*args, **kwargs):
    pass


def MCommonRenderSettingsData_className(*args, **kwargs):
    pass


def MCommonRenderSettingsData_swigregister(*args, **kwargs):
    pass


def MComputation_className(*args, **kwargs):
    pass


def MComputation_swigregister(*args, **kwargs):
    pass


def MConditionMessage_addConditionCallback(*args, **kwargs):
    pass


def MConditionMessage_className(*args, **kwargs):
    pass


def MConditionMessage_getConditionNames(*args, **kwargs):
    pass


def MConditionMessage_getConditionState(*args, **kwargs):
    pass


def MConditionMessage_swigregister(*args, **kwargs):
    pass


def MContainerMessage_addBoundAttrCallback(*args, **kwargs):
    pass


def MContainerMessage_addPublishAttrCallback(*args, **kwargs):
    pass


def MContainerMessage_className(*args, **kwargs):
    pass


def MContainerMessage_swigregister(*args, **kwargs):
    pass


def MCursor_swigregister(*args, **kwargs):
    pass


def MD3D9Renderer_swigregister(*args, **kwargs):
    pass


def MD3D9Renderer_theRenderer(*args, **kwargs):
    pass


def MDGContext_className(*args, **kwargs):
    pass


def MDGContext_swigregister(*args, **kwargs):
    pass


def MDGMessage_addConnectionCallback(*args, **kwargs):
    pass


def MDGMessage_addForceUpdateCallback(*args, **kwargs):
    pass


def MDGMessage_addNodeAddedCallback(*args, **kwargs):
    pass


def MDGMessage_addNodeRemovedCallback(*args, **kwargs):
    pass


def MDGMessage_addPreConnectionCallback(*args, **kwargs):
    pass


def MDGMessage_addTimeChangeCallback(*args, **kwargs):
    pass


def MDGMessage_swigregister(*args, **kwargs):
    pass


def MDGModifier_className(*args, **kwargs):
    pass


def MDGModifier_swigregister(*args, **kwargs):
    pass


def MDagMessage_addAllDagChangesCallback(*args, **kwargs):
    pass


def MDagMessage_addAllDagChangesDagPathCallback(*args, **kwargs):
    pass


def MDagMessage_addChildAddedCallback(*args, **kwargs):
    pass


def MDagMessage_addChildAddedDagPathCallback(*args, **kwargs):
    pass


def MDagMessage_addChildRemovedCallback(*args, **kwargs):
    pass


def MDagMessage_addChildRemovedDagPathCallback(*args, **kwargs):
    pass


def MDagMessage_addChildReorderedCallback(*args, **kwargs):
    pass


def MDagMessage_addChildReorderedDagPathCallback(*args, **kwargs):
    pass


def MDagMessage_addDagCallback(*args, **kwargs):
    pass


def MDagMessage_addDagDagPathCallback(*args, **kwargs):
    pass


def MDagMessage_addInstanceAddedCallback(*args, **kwargs):
    pass


def MDagMessage_addInstanceAddedDagPathCallback(*args, **kwargs):
    pass


def MDagMessage_addInstanceRemovedCallback(*args, **kwargs):
    pass


def MDagMessage_addInstanceRemovedDagPathCallback(*args, **kwargs):
    pass


def MDagMessage_addParentAddedCallback(*args, **kwargs):
    pass


def MDagMessage_addParentAddedDagPathCallback(*args, **kwargs):
    pass


def MDagMessage_addParentRemovedCallback(*args, **kwargs):
    pass


def MDagMessage_addParentRemovedDagPathCallback(*args, **kwargs):
    pass


def MDagMessage_className(*args, **kwargs):
    pass


def MDagMessage_swigregister(*args, **kwargs):
    pass


def MDagModifier_className(*args, **kwargs):
    pass


def MDagModifier_swigregister(*args, **kwargs):
    pass


def MDagPathArray_className(*args, **kwargs):
    pass


def MDagPathArray_swigregister(*args, **kwargs):
    pass


def MDagPath_className(*args, **kwargs):
    pass


def MDagPath_getAPathTo(*args, **kwargs):
    pass


def MDagPath_getAllPathsTo(*args, **kwargs):
    pass


def MDagPath_swigregister(*args, **kwargs):
    pass


def MDataBlock_className(*args, **kwargs):
    pass


def MDataBlock_swigregister(*args, **kwargs):
    pass


def MDataHandle_className(*args, **kwargs):
    pass


def MDataHandle_swigregister(*args, **kwargs):
    pass


def MDepthStencilStateDesc_className(*args, **kwargs):
    pass


def MDepthStencilStateDesc_swigregister(*args, **kwargs):
    pass


def MDepthStencilState_className(*args, **kwargs):
    pass


def MDepthStencilState_swigregister(*args, **kwargs):
    pass


def MDeviceChannel_swigregister(*args, **kwargs):
    pass


def MDeviceState_swigregister(*args, **kwargs):
    pass


def MDistance_className(*args, **kwargs):
    pass


def MDistance_internalToUI(*args, **kwargs):
    pass


def MDistance_internalUnit(*args, **kwargs):
    pass


def MDistance_setInternalUnit(*args, **kwargs):
    pass


def MDistance_setUIUnit(*args, **kwargs):
    pass


def MDistance_swigregister(*args, **kwargs):
    pass


def MDistance_uiToInternal(*args, **kwargs):
    pass


def MDistance_uiUnit(*args, **kwargs):
    pass


def MDoubleArray_className(*args, **kwargs):
    pass


def MDoubleArray_swigregister(*args, **kwargs):
    pass


def MDrawContext_className(*args, **kwargs):
    pass


def MDrawContext_swigregister(*args, **kwargs):
    pass


def MDrawData_className(*args, **kwargs):
    pass


def MDrawData_swigregister(*args, **kwargs):
    pass


def MDrawInfo_className(*args, **kwargs):
    pass


def MDrawInfo_swigregister(*args, **kwargs):
    pass


def MDrawProcedureBase_swigregister(*args, **kwargs):
    pass


def MDrawRegistry_className(*args, **kwargs):
    pass


def MDrawRegistry_deregisterDrawOverrideCreator(*args, **kwargs):
    pass


def MDrawRegistry_deregisterGeometryOverrideCreator(*args, **kwargs):
    pass


def MDrawRegistry_deregisterShaderOverrideCreator(*args, **kwargs):
    pass


def MDrawRegistry_registerDrawOverrideCreator(*args, **kwargs):
    pass


def MDrawRegistry_registerGeometryOverrideCreator(*args, **kwargs):
    pass


def MDrawRegistry_registerShaderOverrideCreator(*args, **kwargs):
    pass


def MDrawRegistry_swigregister(*args, **kwargs):
    pass


def MDrawRequestQueue_className(*args, **kwargs):
    pass


def MDrawRequestQueue_swigregister(*args, **kwargs):
    pass


def MDrawRequest_className(*args, **kwargs):
    pass


def MDrawRequest_swigregister(*args, **kwargs):
    pass


def MDrawTraversal_swigregister(*args, **kwargs):
    pass


def MDynSweptLine_className(*args, **kwargs):
    pass


def MDynSweptLine_swigregister(*args, **kwargs):
    pass


def MDynSweptTriangle_className(*args, **kwargs):
    pass


def MDynSweptTriangle_swigregister(*args, **kwargs):
    pass


def MDynamicsUtil_evalDynamics2dTexture(*args, **kwargs):
    pass


def MDynamicsUtil_hasValidDynamics2dTexture(*args, **kwargs):
    pass


def MDynamicsUtil_swigregister(*args, **kwargs):
    pass


def MEulerRotation_decompose(*args, **kwargs):
    pass


def MEulerRotation_swigregister(*args, **kwargs):
    pass


def MEventMessage_addEventCallback(*args, **kwargs):
    pass


def MEventMessage_className(*args, **kwargs):
    pass


def MEventMessage_getEventNames(*args, **kwargs):
    pass


def MEventMessage_swigregister(*args, **kwargs):
    pass


def MEvent_className(*args, **kwargs):
    pass


def MEvent_swigregister(*args, **kwargs):
    pass


def MFeedbackLine_className(*args, **kwargs):
    pass


def MFeedbackLine_clear(*args, **kwargs):
    pass


def MFeedbackLine_setFormat(*args, **kwargs):
    pass


def MFeedbackLine_setShowFeedback(*args, **kwargs):
    pass


def MFeedbackLine_setTitle(*args, **kwargs):
    pass


def MFeedbackLine_setValue(*args, **kwargs):
    pass


def MFeedbackLine_showFeedback(*args, **kwargs):
    pass


def MFeedbackLine_swigregister(*args, **kwargs):
    pass


def MFileIO_beforeExportFilename(*args, **kwargs):
    pass


def MFileIO_beforeExportUserFileTranslator(*args, **kwargs):
    pass


def MFileIO_beforeImportFilename(*args, **kwargs):
    pass


def MFileIO_beforeImportUserFileTranslator(*args, **kwargs):
    pass


def MFileIO_beforeOpenFilename(*args, **kwargs):
    pass


def MFileIO_beforeOpenUserFileTranslator(*args, **kwargs):
    pass


def MFileIO_beforeReferenceFilename(*args, **kwargs):
    pass


def MFileIO_beforeReferenceUserFileTranslator(*args, **kwargs):
    pass


def MFileIO_beforeSaveFilename(*args, **kwargs):
    pass


def MFileIO_beforeSaveUserFileTranslator(*args, **kwargs):
    pass


def MFileIO_className(*args, **kwargs):
    pass


def MFileIO_cleanReference(*args, **kwargs):
    pass


def MFileIO_currentFile(*args, **kwargs):
    pass


def MFileIO_currentlyReadingFileVersion(*args, **kwargs):
    pass


def MFileIO_exportAll(*args, **kwargs):
    pass


def MFileIO_exportAnim(*args, **kwargs):
    pass


def MFileIO_exportAnimFromReference(*args, **kwargs):
    pass


def MFileIO_exportAsReference(*args, **kwargs):
    pass


def MFileIO_exportSelected(*args, **kwargs):
    pass


def MFileIO_exportSelectedAnim(*args, **kwargs):
    pass


def MFileIO_exportSelectedAnimFromReference(*args, **kwargs):
    pass


def MFileIO_fileCurrentlyLoading(*args, **kwargs):
    pass


def MFileIO_fileType(*args, **kwargs):
    pass


def MFileIO_getErrorStatus(*args, **kwargs):
    pass


def MFileIO_getFileTypes(*args, **kwargs):
    pass


def MFileIO_getFiles(*args, **kwargs):
    pass


def MFileIO_getLastTempFile(*args, **kwargs):
    pass


def MFileIO_getReferenceConnectionsBroken(*args, **kwargs):
    pass


def MFileIO_getReferenceConnectionsMade(*args, **kwargs):
    pass


def MFileIO_getReferenceFileByNode(*args, **kwargs):
    pass


def MFileIO_getReferenceNodes(*args, **kwargs):
    pass


def MFileIO_getReferences(*args, **kwargs):
    pass


def MFileIO_importFile(*args, **kwargs):
    pass


def MFileIO_isImportingFile(*args, **kwargs):
    pass


def MFileIO_isNewingFile(*args, **kwargs):
    pass


def MFileIO_isOpeningFile(*args, **kwargs):
    pass


def MFileIO_isReadingFile(*args, **kwargs):
    pass


def MFileIO_isReferencingFile(*args, **kwargs):
    pass


def MFileIO_isSavingReference(*args, **kwargs):
    pass


def MFileIO_isWritingFile(*args, **kwargs):
    pass


def MFileIO_latestMayaFileVersion(*args, **kwargs):
    pass


def MFileIO_loadReference(*args, **kwargs):
    pass


def MFileIO_loadReferenceByNode(*args, **kwargs):
    pass


def MFileIO_mustRenameToSave(*args, **kwargs):
    pass


def MFileIO_mustRenameToSaveMsg(*args, **kwargs):
    pass


def MFileIO_newFile(*args, **kwargs):
    pass


def MFileIO_open(*args, **kwargs):
    pass


def MFileIO_reference(*args, **kwargs):
    pass


def MFileIO_removeReference(*args, **kwargs):
    pass


def MFileIO_resetError(*args, **kwargs):
    pass


def MFileIO_save(*args, **kwargs):
    pass


def MFileIO_saveAs(*args, **kwargs):
    pass


def MFileIO_saveReference(*args, **kwargs):
    pass


def MFileIO_setCurrentFile(*args, **kwargs):
    pass


def MFileIO_setMustRenameToSave(*args, **kwargs):
    pass


def MFileIO_setMustRenameToSaveMsg(*args, **kwargs):
    pass


def MFileIO_swigregister(*args, **kwargs):
    pass


def MFileIO_unloadReference(*args, **kwargs):
    pass


def MFileIO_unloadReferenceByNode(*args, **kwargs):
    pass


def MFileObject_swigregister(*args, **kwargs):
    pass


def MFloatArray_className(*args, **kwargs):
    pass


def MFloatArray_swigregister(*args, **kwargs):
    pass


def MFloatMatrix_className(*args, **kwargs):
    pass


def MFloatMatrix_swigregister(*args, **kwargs):
    pass


def MFloatPointArray_className(*args, **kwargs):
    pass


def MFloatPointArray_swigregister(*args, **kwargs):
    pass


def MFloatPoint_className(*args, **kwargs):
    pass


def MFloatPoint_swigregister(*args, **kwargs):
    pass


def MFloatVectorArray_className(*args, **kwargs):
    pass


def MFloatVectorArray_swigregister(*args, **kwargs):
    pass


def MFloatVector_swigregister(*args, **kwargs):
    pass


def MFnAirField_className(*args, **kwargs):
    pass


def MFnAirField_swigregister(*args, **kwargs):
    pass


def MFnAmbientLight_className(*args, **kwargs):
    pass


def MFnAmbientLight_swigregister(*args, **kwargs):
    pass


def MFnAnimCurve_className(*args, **kwargs):
    pass


def MFnAnimCurve_swigregister(*args, **kwargs):
    pass


def MFnAnisotropyShader_className(*args, **kwargs):
    pass


def MFnAnisotropyShader_swigregister(*args, **kwargs):
    pass


def MFnAreaLight_className(*args, **kwargs):
    pass


def MFnAreaLight_swigregister(*args, **kwargs):
    pass


def MFnArrayAttrsData_className(*args, **kwargs):
    pass


def MFnArrayAttrsData_swigregister(*args, **kwargs):
    pass


def MFnAttribute_className(*args, **kwargs):
    pass


def MFnAttribute_swigregister(*args, **kwargs):
    pass


def MFnBase_className(*args, **kwargs):
    pass


def MFnBase_swigregister(*args, **kwargs):
    pass


def MFnBlendShapeDeformer_className(*args, **kwargs):
    pass


def MFnBlendShapeDeformer_swigregister(*args, **kwargs):
    pass


def MFnBlinnShader_className(*args, **kwargs):
    pass


def MFnBlinnShader_swigregister(*args, **kwargs):
    pass


def MFnCameraSet_className(*args, **kwargs):
    pass


def MFnCameraSet_swigregister(*args, **kwargs):
    pass


def MFnCamera_className(*args, **kwargs):
    pass


def MFnCamera_swigregister(*args, **kwargs):
    pass


def MFnCharacter_className(*args, **kwargs):
    pass


def MFnCharacter_swigregister(*args, **kwargs):
    pass


def MFnCircleSweepManip_className(*args, **kwargs):
    pass


def MFnCircleSweepManip_swigregister(*args, **kwargs):
    pass


def MFnClip_className(*args, **kwargs):
    pass


def MFnClip_swigregister(*args, **kwargs):
    pass


def MFnComponentListData_className(*args, **kwargs):
    pass


def MFnComponentListData_swigregister(*args, **kwargs):
    pass


def MFnComponent_className(*args, **kwargs):
    pass


def MFnComponent_swigregister(*args, **kwargs):
    pass


def MFnCompoundAttribute_className(*args, **kwargs):
    pass


def MFnCompoundAttribute_swigregister(*args, **kwargs):
    pass


def MFnContainerNode_className(*args, **kwargs):
    pass


def MFnContainerNode_swigregister(*args, **kwargs):
    pass


def MFnCurveSegmentManip_className(*args, **kwargs):
    pass


def MFnCurveSegmentManip_swigregister(*args, **kwargs):
    pass


def MFnDagNode_className(*args, **kwargs):
    pass


def MFnDagNode_swigregister(*args, **kwargs):
    pass


def MFnData_className(*args, **kwargs):
    pass


def MFnData_swigregister(*args, **kwargs):
    pass


def MFnDependencyNode_allocateFlag(*args, **kwargs):
    pass


def MFnDependencyNode_className(*args, **kwargs):
    pass


def MFnDependencyNode_classification(*args, **kwargs):
    pass


def MFnDependencyNode_deallocateAllFlags(*args, **kwargs):
    pass


def MFnDependencyNode_deallocateFlag(*args, **kwargs):
    pass


def MFnDependencyNode_swigregister(*args, **kwargs):
    pass


def MFnDirectionManip_className(*args, **kwargs):
    pass


def MFnDirectionManip_swigregister(*args, **kwargs):
    pass


def MFnDirectionalLight_className(*args, **kwargs):
    pass


def MFnDirectionalLight_swigregister(*args, **kwargs):
    pass


def MFnDiscManip_className(*args, **kwargs):
    pass


def MFnDiscManip_swigregister(*args, **kwargs):
    pass


def MFnDistanceManip_className(*args, **kwargs):
    pass


def MFnDistanceManip_swigregister(*args, **kwargs):
    pass


def MFnDoubleArrayData_className(*args, **kwargs):
    pass


def MFnDoubleArrayData_swigregister(*args, **kwargs):
    pass


def MFnDoubleIndexedComponent_className(*args, **kwargs):
    pass


def MFnDoubleIndexedComponent_swigregister(*args, **kwargs):
    pass


def MFnDragField_className(*args, **kwargs):
    pass


def MFnDragField_swigregister(*args, **kwargs):
    pass


def MFnDynSweptGeometryData_className(*args, **kwargs):
    pass


def MFnDynSweptGeometryData_swigregister(*args, **kwargs):
    pass


def MFnEnumAttribute_className(*args, **kwargs):
    pass


def MFnEnumAttribute_swigregister(*args, **kwargs):
    pass


def MFnExpression_className(*args, **kwargs):
    pass


def MFnExpression_swigregister(*args, **kwargs):
    pass


def MFnField_className(*args, **kwargs):
    pass


def MFnField_swigregister(*args, **kwargs):
    pass


def MFnFluid_className(*args, **kwargs):
    pass


def MFnFluid_swigregister(*args, **kwargs):
    pass


def MFnFreePointTriadManip_className(*args, **kwargs):
    pass


def MFnFreePointTriadManip_swigregister(*args, **kwargs):
    pass


def MFnGenericAttribute_className(*args, **kwargs):
    pass


def MFnGenericAttribute_swigregister(*args, **kwargs):
    pass


def MFnGeometryData_className(*args, **kwargs):
    pass


def MFnGeometryData_swigregister(*args, **kwargs):
    pass


def MFnGeometryFilter_className(*args, **kwargs):
    pass


def MFnGeometryFilter_swigregister(*args, **kwargs):
    pass


def MFnGravityField_className(*args, **kwargs):
    pass


def MFnGravityField_swigregister(*args, **kwargs):
    pass


def MFnHikEffector_className(*args, **kwargs):
    pass


def MFnHikEffector_swigregister(*args, **kwargs):
    pass


def MFnIkEffector_className(*args, **kwargs):
    pass


def MFnIkEffector_swigregister(*args, **kwargs):
    pass


def MFnIkHandle_className(*args, **kwargs):
    pass


def MFnIkHandle_swigregister(*args, **kwargs):
    pass


def MFnIkJoint_className(*args, **kwargs):
    pass


def MFnIkJoint_swigregister(*args, **kwargs):
    pass


def MFnIkSolver_className(*args, **kwargs):
    pass


def MFnIkSolver_swigregister(*args, **kwargs):
    pass


def MFnImageSource_className(*args, **kwargs):
    pass


def MFnImageSource_swigregister(*args, **kwargs):
    pass


def MFnInstancer_className(*args, **kwargs):
    pass


def MFnInstancer_swigregister(*args, **kwargs):
    pass


def MFnIntArrayData_className(*args, **kwargs):
    pass


def MFnIntArrayData_swigregister(*args, **kwargs):
    pass


def MFnKeyframeDeltaAddRemove_className(*args, **kwargs):
    pass


def MFnKeyframeDeltaAddRemove_swigregister(*args, **kwargs):
    pass


def MFnKeyframeDeltaBlockAddRemove_className(*args, **kwargs):
    pass


def MFnKeyframeDeltaBlockAddRemove_swigregister(*args, **kwargs):
    pass


def MFnKeyframeDeltaBreakdown_className(*args, **kwargs):
    pass


def MFnKeyframeDeltaBreakdown_swigregister(*args, **kwargs):
    pass


def MFnKeyframeDeltaInfType_className(*args, **kwargs):
    pass


def MFnKeyframeDeltaInfType_swigregister(*args, **kwargs):
    pass


def MFnKeyframeDeltaMove_className(*args, **kwargs):
    pass


def MFnKeyframeDeltaMove_swigregister(*args, **kwargs):
    pass


def MFnKeyframeDeltaScale_className(*args, **kwargs):
    pass


def MFnKeyframeDeltaScale_swigregister(*args, **kwargs):
    pass


def MFnKeyframeDeltaTangent_className(*args, **kwargs):
    pass


def MFnKeyframeDeltaTangent_swigregister(*args, **kwargs):
    pass


def MFnKeyframeDeltaWeighted_className(*args, **kwargs):
    pass


def MFnKeyframeDeltaWeighted_swigregister(*args, **kwargs):
    pass


def MFnKeyframeDelta_className(*args, **kwargs):
    pass


def MFnKeyframeDelta_swigregister(*args, **kwargs):
    pass


def MFnLambertShader_className(*args, **kwargs):
    pass


def MFnLambertShader_swigregister(*args, **kwargs):
    pass


def MFnLatticeData_className(*args, **kwargs):
    pass


def MFnLatticeData_swigregister(*args, **kwargs):
    pass


def MFnLatticeDeformer_className(*args, **kwargs):
    pass


def MFnLatticeDeformer_swigregister(*args, **kwargs):
    pass


def MFnLattice_className(*args, **kwargs):
    pass


def MFnLattice_swigregister(*args, **kwargs):
    pass


def MFnLayeredShader_className(*args, **kwargs):
    pass


def MFnLayeredShader_swigregister(*args, **kwargs):
    pass


def MFnLightDataAttribute_className(*args, **kwargs):
    pass


def MFnLightDataAttribute_swigregister(*args, **kwargs):
    pass


def MFnLight_className(*args, **kwargs):
    pass


def MFnLight_swigregister(*args, **kwargs):
    pass


def MFnManip3D_className(*args, **kwargs):
    pass


def MFnManip3D_deleteManipulator(*args, **kwargs):
    pass


def MFnManip3D_globalSize(*args, **kwargs):
    pass


def MFnManip3D_handleSize(*args, **kwargs):
    pass


def MFnManip3D_lineSize(*args, **kwargs):
    pass


def MFnManip3D_setGlobalSize(*args, **kwargs):
    pass


def MFnManip3D_setHandleSize(*args, **kwargs):
    pass


def MFnManip3D_setLineSize(*args, **kwargs):
    pass


def MFnManip3D_swigregister(*args, **kwargs):
    pass


def MFnMatrixAttribute_className(*args, **kwargs):
    pass


def MFnMatrixAttribute_swigregister(*args, **kwargs):
    pass


def MFnMatrixData_className(*args, **kwargs):
    pass


def MFnMatrixData_swigregister(*args, **kwargs):
    pass


def MFnMeshData_className(*args, **kwargs):
    pass


def MFnMeshData_swigregister(*args, **kwargs):
    pass


def MFnMesh_autoUniformGridParams(*args, **kwargs):
    pass


def MFnMesh_className(*args, **kwargs):
    pass


def MFnMesh_clearGlobalIntersectionAcceleratorInfo(*args, **kwargs):
    pass


def MFnMesh_globalIntersectionAcceleratorsInfo(*args, **kwargs):
    pass


def MFnMesh_swigregister(*args, **kwargs):
    pass


def MFnMesh_uniformGridParams(*args, **kwargs):
    pass


def MFnMessageAttribute_className(*args, **kwargs):
    pass


def MFnMessageAttribute_swigregister(*args, **kwargs):
    pass


def MFnMotionPath_className(*args, **kwargs):
    pass


def MFnMotionPath_swigregister(*args, **kwargs):
    pass


def MFnNIdData_className(*args, **kwargs):
    pass


def MFnNIdData_swigregister(*args, **kwargs):
    pass


def MFnNObjectData_className(*args, **kwargs):
    pass


def MFnNObjectData_swigregister(*args, **kwargs):
    pass


def MFnNewtonField_className(*args, **kwargs):
    pass


def MFnNewtonField_swigregister(*args, **kwargs):
    pass


def MFnNonAmbientLight_className(*args, **kwargs):
    pass


def MFnNonAmbientLight_swigregister(*args, **kwargs):
    pass


def MFnNonExtendedLight_className(*args, **kwargs):
    pass


def MFnNonExtendedLight_swigregister(*args, **kwargs):
    pass


def MFnNumericAttribute_className(*args, **kwargs):
    pass


def MFnNumericAttribute_swigregister(*args, **kwargs):
    pass


def MFnNumericData_className(*args, **kwargs):
    pass


def MFnNumericData_swigregister(*args, **kwargs):
    pass


def MFnNurbsCurveData_className(*args, **kwargs):
    pass


def MFnNurbsCurveData_swigregister(*args, **kwargs):
    pass


def MFnNurbsCurve_className(*args, **kwargs):
    pass


def MFnNurbsCurve_swigregister(*args, **kwargs):
    pass


def MFnNurbsSurfaceData_className(*args, **kwargs):
    pass


def MFnNurbsSurfaceData_swigregister(*args, **kwargs):
    pass


def MFnNurbsSurface_className(*args, **kwargs):
    pass


def MFnNurbsSurface_swigregister(*args, **kwargs):
    pass


def MFnParticleSystem_className(*args, **kwargs):
    pass


def MFnParticleSystem_swigregister(*args, **kwargs):
    pass


def MFnPartition_className(*args, **kwargs):
    pass


def MFnPartition_swigregister(*args, **kwargs):
    pass


def MFnPfxGeometry_className(*args, **kwargs):
    pass


def MFnPfxGeometry_swigregister(*args, **kwargs):
    pass


def MFnPhongEShader_className(*args, **kwargs):
    pass


def MFnPhongEShader_swigregister(*args, **kwargs):
    pass


def MFnPhongShader_className(*args, **kwargs):
    pass


def MFnPhongShader_swigregister(*args, **kwargs):
    pass


def MFnPluginData_className(*args, **kwargs):
    pass


def MFnPluginData_swigregister(*args, **kwargs):
    pass


def MFnPlugin_className(*args, **kwargs):
    pass


def MFnPlugin_findPlugin(*args, **kwargs):
    pass


def MFnPlugin_isNodeRegistered(*args, **kwargs):
    pass


def MFnPlugin_registeringCallableScript(*args, **kwargs):
    pass


def MFnPlugin_setRegisteringCallableScript(*args, **kwargs):
    pass


def MFnPlugin_swigregister(*args, **kwargs):
    pass


def MFnPointArrayData_className(*args, **kwargs):
    pass


def MFnPointArrayData_swigregister(*args, **kwargs):
    pass


def MFnPointLight_className(*args, **kwargs):
    pass


def MFnPointLight_swigregister(*args, **kwargs):
    pass


def MFnPointOnCurveManip_className(*args, **kwargs):
    pass


def MFnPointOnCurveManip_swigregister(*args, **kwargs):
    pass


def MFnPointOnSurfaceManip_className(*args, **kwargs):
    pass


def MFnPointOnSurfaceManip_swigregister(*args, **kwargs):
    pass


def MFnRadialField_className(*args, **kwargs):
    pass


def MFnRadialField_swigregister(*args, **kwargs):
    pass


def MFnReflectShader_className(*args, **kwargs):
    pass


def MFnReflectShader_swigregister(*args, **kwargs):
    pass


def MFnRenderLayer_className(*args, **kwargs):
    pass


def MFnRenderLayer_currentLayer(*args, **kwargs):
    pass


def MFnRenderLayer_defaultRenderLayer(*args, **kwargs):
    pass


def MFnRenderLayer_findLayerByName(*args, **kwargs):
    pass


def MFnRenderLayer_listAllRenderLayers(*args, **kwargs):
    pass


def MFnRenderLayer_swigregister(*args, **kwargs):
    pass


def MFnRenderPass_className(*args, **kwargs):
    pass


def MFnRenderPass_swigregister(*args, **kwargs):
    pass


def MFnRotateManip_className(*args, **kwargs):
    pass


def MFnRotateManip_swigregister(*args, **kwargs):
    pass


def MFnScaleManip_className(*args, **kwargs):
    pass


def MFnScaleManip_swigregister(*args, **kwargs):
    pass


def MFnSet_className(*args, **kwargs):
    pass


def MFnSet_swigregister(*args, **kwargs):
    pass


def MFnSingleIndexedComponent_className(*args, **kwargs):
    pass


def MFnSingleIndexedComponent_swigregister(*args, **kwargs):
    pass


def MFnSkinCluster_className(*args, **kwargs):
    pass


def MFnSkinCluster_swigregister(*args, **kwargs):
    pass


def MFnSphereData_className(*args, **kwargs):
    pass


def MFnSphereData_swigregister(*args, **kwargs):
    pass


def MFnSpotLight_className(*args, **kwargs):
    pass


def MFnSpotLight_swigregister(*args, **kwargs):
    pass


def MFnStateManip_className(*args, **kwargs):
    pass


def MFnStateManip_swigregister(*args, **kwargs):
    pass


def MFnStringArrayData_className(*args, **kwargs):
    pass


def MFnStringArrayData_swigregister(*args, **kwargs):
    pass


def MFnStringData_className(*args, **kwargs):
    pass


def MFnStringData_swigregister(*args, **kwargs):
    pass


def MFnSubdData_className(*args, **kwargs):
    pass


def MFnSubdData_swigregister(*args, **kwargs):
    pass


def MFnSubdNames_base(*args, **kwargs):
    pass


def MFnSubdNames_baseFaceId(*args, **kwargs):
    pass


def MFnSubdNames_baseFaceIdFromIndex(*args, **kwargs):
    pass


def MFnSubdNames_baseFaceIdFromLong(*args, **kwargs):
    pass


def MFnSubdNames_baseFaceIndex(*args, **kwargs):
    pass


def MFnSubdNames_baseFaceIndexFromId(*args, **kwargs):
    pass


def MFnSubdNames_className(*args, **kwargs):
    pass


def MFnSubdNames_corner(*args, **kwargs):
    pass


def MFnSubdNames_first(*args, **kwargs):
    pass


def MFnSubdNames_fromMUint64(*args, **kwargs):
    pass


def MFnSubdNames_fromSelectionIndices(*args, **kwargs):
    pass


def MFnSubdNames_level(*args, **kwargs):
    pass


def MFnSubdNames_levelOneFaceAsLong(*args, **kwargs):
    pass


def MFnSubdNames_levelOneFaceId(*args, **kwargs):
    pass


def MFnSubdNames_levelOneFaceIdFromIndex(*args, **kwargs):
    pass


def MFnSubdNames_levelOneFaceIdFromLong(*args, **kwargs):
    pass


def MFnSubdNames_levelOneFaceIndexFromId(*args, **kwargs):
    pass


def MFnSubdNames_nonBaseFaceEdges(*args, **kwargs):
    pass


def MFnSubdNames_nonBaseFaceVertices(*args, **kwargs):
    pass


def MFnSubdNames_parentFaceId(*args, **kwargs):
    pass


def MFnSubdNames_path(*args, **kwargs):
    pass


def MFnSubdNames_swigregister(*args, **kwargs):
    pass


def MFnSubdNames_toMUint64(*args, **kwargs):
    pass


def MFnSubdNames_toSelectionIndices(*args, **kwargs):
    pass


def MFnSubd_className(*args, **kwargs):
    pass


def MFnSubd_swigregister(*args, **kwargs):
    pass


def MFnToggleManip_className(*args, **kwargs):
    pass


def MFnToggleManip_swigregister(*args, **kwargs):
    pass


def MFnTransform_className(*args, **kwargs):
    pass


def MFnTransform_swigregister(*args, **kwargs):
    pass


def MFnTripleIndexedComponent_className(*args, **kwargs):
    pass


def MFnTripleIndexedComponent_swigregister(*args, **kwargs):
    pass


def MFnTurbulenceField_className(*args, **kwargs):
    pass


def MFnTurbulenceField_swigregister(*args, **kwargs):
    pass


def MFnTypedAttribute_className(*args, **kwargs):
    pass


def MFnTypedAttribute_swigregister(*args, **kwargs):
    pass


def MFnUInt64ArrayData_className(*args, **kwargs):
    pass


def MFnUInt64ArrayData_swigregister(*args, **kwargs):
    pass


def MFnUint64SingleIndexedComponent_className(*args, **kwargs):
    pass


def MFnUint64SingleIndexedComponent_swigregister(*args, **kwargs):
    pass


def MFnUniformField_className(*args, **kwargs):
    pass


def MFnUniformField_swigregister(*args, **kwargs):
    pass


def MFnUnitAttribute_className(*args, **kwargs):
    pass


def MFnUnitAttribute_swigregister(*args, **kwargs):
    pass


def MFnVectorArrayData_className(*args, **kwargs):
    pass


def MFnVectorArrayData_swigregister(*args, **kwargs):
    pass


def MFnVolumeAxisField_className(*args, **kwargs):
    pass


def MFnVolumeAxisField_swigregister(*args, **kwargs):
    pass


def MFnVolumeLight_className(*args, **kwargs):
    pass


def MFnVolumeLight_swigregister(*args, **kwargs):
    pass


def MFnVortexField_className(*args, **kwargs):
    pass


def MFnVortexField_swigregister(*args, **kwargs):
    pass


def MFnWeightGeometryFilter_className(*args, **kwargs):
    pass


def MFnWeightGeometryFilter_swigregister(*args, **kwargs):
    pass


def MFnWireDeformer_className(*args, **kwargs):
    pass


def MFnWireDeformer_swigregister(*args, **kwargs):
    pass


def MFn_swigregister(*args, **kwargs):
    pass


def MGLFunctionTable_swigregister(*args, **kwargs):
    pass


def MGeometryData_swigregister(*args, **kwargs):
    pass


def MGeometryList_swigregister(*args, **kwargs):
    pass


def MGeometryManager_className(*args, **kwargs):
    pass


def MGeometryManager_dereferenceDefaultGeometry(*args, **kwargs):
    pass


def MGeometryManager_getGeometry(*args, **kwargs):
    pass


def MGeometryManager_referenceDefaultGeometry(*args, **kwargs):
    pass


def MGeometryManager_swigregister(*args, **kwargs):
    pass


def MGeometryPrimitive_swigregister(*args, **kwargs):
    pass


def MGeometryRequirements_className(*args, **kwargs):
    pass


def MGeometryRequirements_swigregister(*args, **kwargs):
    pass


def MGeometry_className(*args, **kwargs):
    pass


def MGeometry_dataTypeString(*args, **kwargs):
    pass


def MGeometry_drawModeString(*args, **kwargs):
    pass


def MGeometry_primitiveString(*args, **kwargs):
    pass


def MGeometry_semanticString(*args, **kwargs):
    pass


def MGeometry_swigregister(*args, **kwargs):
    pass


def MGlobal_addToModel(*args, **kwargs):
    pass


def MGlobal_addToModelAt(*args, **kwargs):
    pass


def MGlobal_animSelectionMask(*args, **kwargs):
    pass


def MGlobal_apiVersion(*args, **kwargs):
    pass


def MGlobal_className(*args, **kwargs):
    pass


def MGlobal_clearSelectionList(*args, **kwargs):
    pass


def MGlobal_closeErrorLog(*args, **kwargs):
    pass


def MGlobal_componentSelectionMask(*args, **kwargs):
    pass


def MGlobal_defaultErrorLogPathName(*args, **kwargs):
    pass


def MGlobal_deleteNode(*args, **kwargs):
    pass


def MGlobal_disableStow(*args, **kwargs):
    pass


def MGlobal_displayError(*args, **kwargs):
    pass


def MGlobal_displayInfo(*args, **kwargs):
    pass


def MGlobal_displayWarning(*args, **kwargs):
    pass


def MGlobal_doErrorLogEntry(*args, **kwargs):
    pass


def MGlobal_errorLogPathName(*args, **kwargs):
    pass


def MGlobal_errorLoggingIsOn(*args, **kwargs):
    pass


def MGlobal_executeCommand(*args, **kwargs):
    pass


def MGlobal_executeCommandOnIdle(*args, **kwargs):
    pass


def MGlobal_executeCommandStringResult(*args, **kwargs):
    pass


def MGlobal_getActiveSelectionList(*args, **kwargs):
    pass


def MGlobal_getAssociatedSets(*args, **kwargs):
    pass


def MGlobal_getFunctionSetList(*args, **kwargs):
    pass


def MGlobal_getHiliteList(*args, **kwargs):
    pass


def MGlobal_getLiveList(*args, **kwargs):
    pass


def MGlobal_getRichSelection(*args, **kwargs):
    pass


def MGlobal_getSelectionListByName(*args, **kwargs):
    pass


def MGlobal_isRedoing(*args, **kwargs):
    pass


def MGlobal_isSelected(*args, **kwargs):
    pass


def MGlobal_isUndoing(*args, **kwargs):
    pass


def MGlobal_isYAxisUp(*args, **kwargs):
    pass


def MGlobal_isZAxisUp(*args, **kwargs):
    pass


def MGlobal_mayaState(*args, **kwargs):
    pass


def MGlobal_mayaVersion(*args, **kwargs):
    pass


def MGlobal_miscSelectionMask(*args, **kwargs):
    pass


def MGlobal_objectSelectionMask(*args, **kwargs):
    pass


def MGlobal_optionVarDoubleValue(*args, **kwargs):
    pass


def MGlobal_optionVarIntValue(*args, **kwargs):
    pass


def MGlobal_optionVarStringValue(*args, **kwargs):
    pass


def MGlobal_removeFromModel(*args, **kwargs):
    pass


def MGlobal_resetToDefaultErrorLogPathName(*args, **kwargs):
    pass


def MGlobal_select(*args, **kwargs):
    pass


def MGlobal_selectByName(*args, **kwargs):
    pass


def MGlobal_selectCommand(*args, **kwargs):
    pass


def MGlobal_selectFromScreen(*args, **kwargs):
    pass


def MGlobal_selectionMethod(*args, **kwargs):
    pass


def MGlobal_selectionMode(*args, **kwargs):
    pass


def MGlobal_setActiveSelectionList(*args, **kwargs):
    pass


def MGlobal_setAnimSelectionMask(*args, **kwargs):
    pass


def MGlobal_setComponentSelectionMask(*args, **kwargs):
    pass


def MGlobal_setDisableStow(*args, **kwargs):
    pass


def MGlobal_setDisplayCVs(*args, **kwargs):
    pass


def MGlobal_setErrorLogPathName(*args, **kwargs):
    pass


def MGlobal_setHiliteList(*args, **kwargs):
    pass


def MGlobal_setMiscSelectionMask(*args, **kwargs):
    pass


def MGlobal_setObjectSelectionMask(*args, **kwargs):
    pass


def MGlobal_setOptionVarValue(*args, **kwargs):
    pass


def MGlobal_setSelectionMode(*args, **kwargs):
    pass


def MGlobal_setTrackSelectionOrderEnabled(*args, **kwargs):
    pass


def MGlobal_setYAxisUp(*args, **kwargs):
    pass


def MGlobal_setZAxisUp(*args, **kwargs):
    pass


def MGlobal_sourceFile(*args, **kwargs):
    pass


def MGlobal_startErrorLogging(*args, **kwargs):
    pass


def MGlobal_stopErrorLogging(*args, **kwargs):
    pass


def MGlobal_swigregister(*args, **kwargs):
    pass


def MGlobal_trackSelectionOrderEnabled(*args, **kwargs):
    pass


def MGlobal_unselect(*args, **kwargs):
    pass


def MGlobal_unselectByName(*args, **kwargs):
    pass


def MGlobal_upAxis(*args, **kwargs):
    pass


def MGlobal_viewFrame(*args, **kwargs):
    pass


def MHUDRender_swigregister(*args, **kwargs):
    pass


def MHWShaderSwatchGenerator_createObj(*args, **kwargs):
    pass


def MHWShaderSwatchGenerator_getSwatchBackgroundColor(*args, **kwargs):
    pass


def MHWShaderSwatchGenerator_initialize(*args, **kwargs):
    pass


def MHWShaderSwatchGenerator_swigregister(*args, **kwargs):
    pass


def MHairSystem_className(*args, **kwargs):
    pass


def MHairSystem_getCollisionObject(*args, **kwargs):
    pass


def MHairSystem_getFollicle(*args, **kwargs):
    pass


def MHairSystem_registerCollisionSolverCollide(*args, **kwargs):
    pass


def MHairSystem_registerCollisionSolverPreFrame(*args, **kwargs):
    pass


def MHairSystem_registeringCallableScript(*args, **kwargs):
    pass


def MHairSystem_setRegisteringCallableScript(*args, **kwargs):
    pass


def MHairSystem_swigregister(*args, **kwargs):
    pass


def MHairSystem_unregisterCollisionSolverCollide(*args, **kwargs):
    pass


def MHairSystem_unregisterCollisionSolverPreFrame(*args, **kwargs):
    pass


def MHardwareRenderer_swigregister(*args, **kwargs):
    pass


def MHardwareRenderer_theRenderer(*args, **kwargs):
    pass


def MHwTextureManager_className(*args, **kwargs):
    pass


def MHwTextureManager_deregisterTextureFile(*args, **kwargs):
    pass


def MHwTextureManager_glBind(*args, **kwargs):
    pass


def MHwTextureManager_registerTextureFile(*args, **kwargs):
    pass


def MHwTextureManager_swigregister(*args, **kwargs):
    pass


def MHwTextureManager_textureFile(*args, **kwargs):
    pass


def MHwrCallback_addCallback(*args, **kwargs):
    pass


def MHwrCallback_removeCallback(*args, **kwargs):
    pass


def MHwrCallback_swigregister(*args, **kwargs):
    pass


def MIffFile_className(*args, **kwargs):
    pass


def MIffFile_swigregister(*args, **kwargs):
    pass


def MIffTag_swigregister(*args, **kwargs):
    pass


def MIkHandleGroup_className(*args, **kwargs):
    pass


def MIkHandleGroup_swigregister(*args, **kwargs):
    pass


def MIkSystem_className(*args, **kwargs):
    pass


def MIkSystem_findSolver(*args, **kwargs):
    pass


def MIkSystem_getSolvers(*args, **kwargs):
    pass


def MIkSystem_isGlobalSnap(*args, **kwargs):
    pass


def MIkSystem_isGlobalSolve(*args, **kwargs):
    pass


def MIkSystem_setGlobalSnap(*args, **kwargs):
    pass


def MIkSystem_setGlobalSolve(*args, **kwargs):
    pass


def MIkSystem_swigregister(*args, **kwargs):
    pass


def MImageFileInfo_swigregister(*args, **kwargs):
    pass


def MImage_className(*args, **kwargs):
    pass


def MImage_filterExists(*args, **kwargs):
    pass


def MImage_swigregister(*args, **kwargs):
    pass


def MIndexBuffer_className(*args, **kwargs):
    pass


def MIndexBuffer_swigregister(*args, **kwargs):
    pass


def MIntArray_className(*args, **kwargs):
    pass


def MIntArray_swigregister(*args, **kwargs):
    pass


def MItCurveCV_className(*args, **kwargs):
    pass


def MItCurveCV_swigregister(*args, **kwargs):
    pass


def MItDag_className(*args, **kwargs):
    pass


def MItDag_swigregister(*args, **kwargs):
    pass


def MItDependencyGraph_className(*args, **kwargs):
    pass


def MItDependencyGraph_swigregister(*args, **kwargs):
    pass


def MItDependencyNodes_className(*args, **kwargs):
    pass


def MItDependencyNodes_swigregister(*args, **kwargs):
    pass


def MItGeometry_className(*args, **kwargs):
    pass


def MItGeometry_swigregister(*args, **kwargs):
    pass


def MItInstancer_className(*args, **kwargs):
    pass


def MItInstancer_swigregister(*args, **kwargs):
    pass


def MItKeyframe_className(*args, **kwargs):
    pass


def MItKeyframe_swigregister(*args, **kwargs):
    pass


def MItMeshEdge_className(*args, **kwargs):
    pass


def MItMeshEdge_swigregister(*args, **kwargs):
    pass


def MItMeshFaceVertex_className(*args, **kwargs):
    pass


def MItMeshFaceVertex_swigregister(*args, **kwargs):
    pass


def MItMeshPolygon_className(*args, **kwargs):
    pass


def MItMeshPolygon_swigregister(*args, **kwargs):
    pass


def MItMeshVertex_className(*args, **kwargs):
    pass


def MItMeshVertex_swigregister(*args, **kwargs):
    pass


def MItSelectionList_className(*args, **kwargs):
    pass


def MItSelectionList_swigregister(*args, **kwargs):
    pass


def MItSubdEdge_className(*args, **kwargs):
    pass


def MItSubdEdge_swigregister(*args, **kwargs):
    pass


def MItSubdFace_className(*args, **kwargs):
    pass


def MItSubdFace_swigregister(*args, **kwargs):
    pass


def MItSubdVertex_className(*args, **kwargs):
    pass


def MItSubdVertex_swigregister(*args, **kwargs):
    pass


def MItSurfaceCV_className(*args, **kwargs):
    pass


def MItSurfaceCV_swigregister(*args, **kwargs):
    pass


def MIteratorType_swigregister(*args, **kwargs):
    pass


def MLightLinks_swigregister(*args, **kwargs):
    pass


def MLockMessage_className(*args, **kwargs):
    pass


def MLockMessage_setNodeLockDAGQueryCallback(*args, **kwargs):
    pass


def MLockMessage_setNodeLockQueryCallback(*args, **kwargs):
    pass


def MLockMessage_setPlugLockQueryCallback(*args, **kwargs):
    pass


def MLockMessage_swigregister(*args, **kwargs):
    pass


def MManipData_className(*args, **kwargs):
    pass


def MManipData_swigregister(*args, **kwargs):
    pass


def MMaterial_className(*args, **kwargs):
    pass


def MMaterial_defaultMaterial(*args, **kwargs):
    pass


def MMaterial_swigregister(*args, **kwargs):
    pass


def MMatrixArray_className(*args, **kwargs):
    pass


def MMatrixArray_swigregister(*args, **kwargs):
    pass


def MMatrix_className(*args, **kwargs):
    pass


def MMatrix_swigregister(*args, **kwargs):
    pass


def MMeshIntersector_className(*args, **kwargs):
    pass


def MMeshIntersector_swigregister(*args, **kwargs):
    pass


def MMeshIsectAccelParams_swigregister(*args, **kwargs):
    pass


def MMeshSmoothOptions_className(*args, **kwargs):
    pass


def MMeshSmoothOptions_swigregister(*args, **kwargs):
    pass


def MMessageNode_swigregister(*args, **kwargs):
    pass


def MMessage_className(*args, **kwargs):
    pass


def MMessage_currentCallbackId(*args, **kwargs):
    pass


def MMessage_nodeCallbacks(*args, **kwargs):
    pass


def MMessage_registeringCallableScript(*args, **kwargs):
    pass


def MMessage_removeCallback(*args, **kwargs):
    pass


def MMessage_removeCallbacks(*args, **kwargs):
    pass


def MMessage_setRegisteringCallableScript(*args, **kwargs):
    pass


def MMessage_swigregister(*args, **kwargs):
    pass


def MModelMessage_addAfterDuplicateCallback(*args, **kwargs):
    pass


def MModelMessage_addBeforeDuplicateCallback(*args, **kwargs):
    pass


def MModelMessage_addCallback(*args, **kwargs):
    pass


def MModelMessage_addNodeAddedToModelCallback(*args, **kwargs):
    pass


def MModelMessage_addNodeRemovedFromModelCallback(*args, **kwargs):
    pass


def MModelMessage_className(*args, **kwargs):
    pass


def MModelMessage_swigregister(*args, **kwargs):
    pass


def MNamespace_addNamespace(*args, **kwargs):
    pass


def MNamespace_className(*args, **kwargs):
    pass


def MNamespace_currentNamespace(*args, **kwargs):
    pass


def MNamespace_getNamespaceFromName(*args, **kwargs):
    pass


def MNamespace_getNamespaceObjects(*args, **kwargs):
    pass


def MNamespace_getNamespaces(*args, **kwargs):
    pass


def MNamespace_makeNamepathAbsolute(*args, **kwargs):
    pass


def MNamespace_moveNamespace(*args, **kwargs):
    pass


def MNamespace_namespaceExists(*args, **kwargs):
    pass


def MNamespace_parentNamespace(*args, **kwargs):
    pass


def MNamespace_relativeNames(*args, **kwargs):
    pass


def MNamespace_removeNamespace(*args, **kwargs):
    pass


def MNamespace_renameNamespace(*args, **kwargs):
    pass


def MNamespace_rootNamespace(*args, **kwargs):
    pass


def MNamespace_setCurrentNamespace(*args, **kwargs):
    pass


def MNamespace_setRelativeNames(*args, **kwargs):
    pass


def MNamespace_stripNamespaceFromName(*args, **kwargs):
    pass


def MNamespace_swigregister(*args, **kwargs):
    pass


def MNodeClass_className(*args, **kwargs):
    pass


def MNodeClass_swigregister(*args, **kwargs):
    pass


def MNodeMessage_addAttributeAddedOrRemovedCallback(*args, **kwargs):
    pass


def MNodeMessage_addAttributeChangedCallback(*args, **kwargs):
    pass


def MNodeMessage_addKeyableChangeOverride(*args, **kwargs):
    pass


def MNodeMessage_addNameChangedCallback(*args, **kwargs):
    pass


def MNodeMessage_addNodeAboutToDeleteCallback(*args, **kwargs):
    pass


def MNodeMessage_addNodeDestroyedCallback(*args, **kwargs):
    pass


def MNodeMessage_addNodeDirtyCallback(*args, **kwargs):
    pass


def MNodeMessage_addNodeDirtyPlugCallback(*args, **kwargs):
    pass


def MNodeMessage_addNodePreRemovalCallback(*args, **kwargs):
    pass


def MNodeMessage_className(*args, **kwargs):
    pass


def MNodeMessage_swigregister(*args, **kwargs):
    pass


def MNurbsIntersector_className(*args, **kwargs):
    pass


def MNurbsIntersector_swigregister(*args, **kwargs):
    pass


def MObjectArray_className(*args, **kwargs):
    pass


def MObjectArray_swigregister(*args, **kwargs):
    pass


def MObjectHandle_swigregister(*args, **kwargs):
    pass


def MObjectListFilter_className(*args, **kwargs):
    pass


def MObjectListFilter_deregisterFilter(*args, **kwargs):
    pass


def MObjectListFilter_registerFilter(*args, **kwargs):
    pass


def MObjectListFilter_swigregister(*args, **kwargs):
    pass


def MObjectSetMessage_addSetMembersModifiedCallback(*args, **kwargs):
    pass


def MObjectSetMessage_className(*args, **kwargs):
    pass


def MObjectSetMessage_swigregister(*args, **kwargs):
    pass


def MObject_swigregister(*args, **kwargs):
    pass


def MPlane_className(*args, **kwargs):
    pass


def MPlane_swigregister(*args, **kwargs):
    pass


def MPlugArray_className(*args, **kwargs):
    pass


def MPlugArray_swigregister(*args, **kwargs):
    pass


def MPlug_className(*args, **kwargs):
    pass


def MPlug_swigregister(*args, **kwargs):
    pass


def MPointArray_className(*args, **kwargs):
    pass


def MPointArray_swigregister(*args, **kwargs):
    pass


def MPointOnMesh_swigregister(*args, **kwargs):
    pass


def MPointOnNurbs_swigregister(*args, **kwargs):
    pass


def MPoint_className(*args, **kwargs):
    pass


def MPoint_swigregister(*args, **kwargs):
    pass


def MPolyMessage_addPolyComponentIdChangedCallback(*args, **kwargs):
    pass


def MPolyMessage_addPolyTopologyChangedCallback(*args, **kwargs):
    pass


def MPolyMessage_className(*args, **kwargs):
    pass


def MPolyMessage_deletedId(*args, **kwargs):
    pass


def MPolyMessage_swigregister(*args, **kwargs):
    pass


def MPresentTarget_swigregister(*args, **kwargs):
    pass


def MProgressWindow_advanceProgress(*args, **kwargs):
    pass


def MProgressWindow_className(*args, **kwargs):
    pass


def MProgressWindow_endProgress(*args, **kwargs):
    pass


def MProgressWindow_isCancelled(*args, **kwargs):
    pass


def MProgressWindow_isInterruptable(*args, **kwargs):
    pass


def MProgressWindow_progress(*args, **kwargs):
    pass


def MProgressWindow_progressMax(*args, **kwargs):
    pass


def MProgressWindow_progressMin(*args, **kwargs):
    pass


def MProgressWindow_progressStatus(*args, **kwargs):
    pass


def MProgressWindow_reserve(*args, **kwargs):
    pass


def MProgressWindow_setInterruptable(*args, **kwargs):
    pass


def MProgressWindow_setProgress(*args, **kwargs):
    pass


def MProgressWindow_setProgressMax(*args, **kwargs):
    pass


def MProgressWindow_setProgressMin(*args, **kwargs):
    pass


def MProgressWindow_setProgressRange(*args, **kwargs):
    pass


def MProgressWindow_setProgressStatus(*args, **kwargs):
    pass


def MProgressWindow_setTitle(*args, **kwargs):
    pass


def MProgressWindow_startProgress(*args, **kwargs):
    pass


def MProgressWindow_swigregister(*args, **kwargs):
    pass


def MProgressWindow_title(*args, **kwargs):
    pass


def MPx3dModelView_className(*args, **kwargs):
    pass


def MPx3dModelView_getModelView(*args, **kwargs):
    pass


def MPx3dModelView_swigregister(*args, **kwargs):
    pass


def MPxBakeEngine_swigregister(*args, **kwargs):
    pass


def MPxCacheFormat_className(*args, **kwargs):
    pass


def MPxCacheFormat_swigregister(*args, **kwargs):
    pass


def MPxCameraSet_className(*args, **kwargs):
    pass


def MPxCameraSet_swigregister(*args, **kwargs):
    pass


def MPxCommand_appendToResult(*args, **kwargs):
    pass


def MPxCommand_className(*args, **kwargs):
    pass


def MPxCommand_clearResult(*args, **kwargs):
    pass


def MPxCommand_currentDoubleResult(*args, **kwargs):
    pass


def MPxCommand_currentIntResult(*args, **kwargs):
    pass


def MPxCommand_currentResultType(*args, **kwargs):
    pass


def MPxCommand_currentStringResult(*args, **kwargs):
    pass


def MPxCommand_displayError(*args, **kwargs):
    pass


def MPxCommand_displayInfo(*args, **kwargs):
    pass


def MPxCommand_displayWarning(*args, **kwargs):
    pass


def MPxCommand_getCurrentResult(*args, **kwargs):
    pass


def MPxCommand_isCurrentResultArray(*args, **kwargs):
    pass


def MPxCommand_setResult(*args, **kwargs):
    pass


def MPxCommand_swigregister(*args, **kwargs):
    pass


def MPxComponentShape_swigregister(*args, **kwargs):
    pass


def MPxConstraintCommand_swigregister(*args, **kwargs):
    pass


def MPxConstraint_className(*args, **kwargs):
    pass


def MPxConstraint_swigregister(*args, **kwargs):
    pass


def MPxContextCommand_className(*args, **kwargs):
    pass


def MPxContextCommand_swigregister(*args, **kwargs):
    pass


def MPxContext__ignoreEntry(*args, **kwargs):
    pass


def MPxContext_className(*args, **kwargs):
    pass


def MPxContext_swigregister(*args, **kwargs):
    pass


def MPxControlCommand_className(*args, **kwargs):
    pass


def MPxControlCommand_swigregister(*args, **kwargs):
    pass


def MPxData_swigregister(*args, **kwargs):
    pass


def MPxDeformerNode_className(*args, **kwargs):
    pass


def MPxDeformerNode_swigregister(*args, **kwargs):
    pass


def MPxDragAndDropBehavior_className(*args, **kwargs):
    pass


def MPxDragAndDropBehavior_swigregister(*args, **kwargs):
    pass


def MPxDrawOverride_className(*args, **kwargs):
    pass


def MPxDrawOverride_swigregister(*args, **kwargs):
    pass


def MPxEmitterNode_className(*args, **kwargs):
    pass


def MPxEmitterNode_swigregister(*args, **kwargs):
    pass


def MPxFieldNode_className(*args, **kwargs):
    pass


def MPxFieldNode_swigregister(*args, **kwargs):
    pass


def MPxFileTranslator_fileAccessMode(*args, **kwargs):
    pass


def MPxFileTranslator_swigregister(*args, **kwargs):
    pass


def MPxFluidEmitterNode_className(*args, **kwargs):
    pass


def MPxFluidEmitterNode_swigregister(*args, **kwargs):
    pass


def MPxGeometryData_swigregister(*args, **kwargs):
    pass


def MPxGeometryIterator_className(*args, **kwargs):
    pass


def MPxGeometryIterator_swigregister(*args, **kwargs):
    pass


def MPxGeometryOverride_className(*args, **kwargs):
    pass


def MPxGeometryOverride_swigregister(*args, **kwargs):
    pass


def MPxGlBuffer_className(*args, **kwargs):
    pass


def MPxGlBuffer_swigregister(*args, **kwargs):
    pass


def MPxHardwareShader_className(*args, **kwargs):
    pass


def MPxHardwareShader_findResource(*args, **kwargs):
    pass


def MPxHardwareShader_getHardwareShaderPtr(*args, **kwargs):
    pass


def MPxHardwareShader_swigregister(*args, **kwargs):
    pass


def MPxHwShaderNode_className(*args, **kwargs):
    pass


def MPxHwShaderNode_getHwShaderNodePtr(*args, **kwargs):
    pass


def MPxHwShaderNode_swigregister(*args, **kwargs):
    pass


def MPxIkSolverNode_className(*args, **kwargs):
    pass


def MPxIkSolverNode_swigregister(*args, **kwargs):
    pass


def MPxImageFile_swigregister(*args, **kwargs):
    pass


def MPxImagePlane_className(*args, **kwargs):
    pass


def MPxImagePlane_swigregister(*args, **kwargs):
    pass


def MPxLocatorNode_className(*args, **kwargs):
    pass


def MPxLocatorNode_swigregister(*args, **kwargs):
    pass


def MPxManipContainer_addToManipConnectTable(*args, **kwargs):
    pass


def MPxManipContainer_className(*args, **kwargs):
    pass


def MPxManipContainer_initialize(*args, **kwargs):
    pass


def MPxManipContainer_newManipulator(*args, **kwargs):
    pass


def MPxManipContainer_removeFromManipConnectTable(*args, **kwargs):
    pass


def MPxManipContainer_swigregister(*args, **kwargs):
    pass


def MPxManipulatorNode_className(*args, **kwargs):
    pass


def MPxManipulatorNode_newManipulator(*args, **kwargs):
    pass


def MPxManipulatorNode_swigregister(*args, **kwargs):
    pass


def MPxMaterialInformation_swigregister(*args, **kwargs):
    pass


def MPxMayaAsciiFilterOutput_swigregister(*args, **kwargs):
    pass


def MPxMayaAsciiFilter_swigregister(*args, **kwargs):
    pass


def MPxMidiInputDevice_className(*args, **kwargs):
    pass


def MPxMidiInputDevice_swigregister(*args, **kwargs):
    pass


def MPxModelEditorCommand_className(*args, **kwargs):
    pass


def MPxModelEditorCommand_swigregister(*args, **kwargs):
    pass


def MPxNode_addAttribute(*args, **kwargs):
    pass


def MPxNode_attributeAffects(*args, **kwargs):
    pass


def MPxNode_className(*args, **kwargs):
    pass


def MPxNode_inheritAttributesFrom(*args, **kwargs):
    pass


def MPxNode_swigregister(*args, **kwargs):
    pass


def MPxObjectSet_className(*args, **kwargs):
    pass


def MPxObjectSet_swigregister(*args, **kwargs):
    pass


def MPxParticleAttributeMapperNode_className(*args, **kwargs):
    pass


def MPxParticleAttributeMapperNode_swigregister(*args, **kwargs):
    pass


def MPxPolyTrg_swigregister(*args, **kwargs):
    pass


def MPxPolyTweakUVCommand_newSyntax(*args, **kwargs):
    pass


def MPxPolyTweakUVCommand_swigregister(*args, **kwargs):
    pass


def MPxRenderPassImpl_swigregister(*args, **kwargs):
    pass


def MPxSelectionContext_className(*args, **kwargs):
    pass


def MPxSelectionContext_swigregister(*args, **kwargs):
    pass


def MPxShaderOverride_className(*args, **kwargs):
    pass


def MPxShaderOverride_swigregister(*args, **kwargs):
    pass


def MPxSpringNode_className(*args, **kwargs):
    pass


def MPxSpringNode_swigregister(*args, **kwargs):
    pass


def MPxSurfaceShapeUI_className(*args, **kwargs):
    pass


def MPxSurfaceShapeUI_surfaceShapeUI(*args, **kwargs):
    pass


def MPxSurfaceShapeUI_swigregister(*args, **kwargs):
    pass


def MPxSurfaceShape_className(*args, **kwargs):
    pass


def MPxSurfaceShape_swigregister(*args, **kwargs):
    pass


def MPxToolCommand_className(*args, **kwargs):
    pass


def MPxToolCommand_swigregister(*args, **kwargs):
    pass


def MPxTransform_className(*args, **kwargs):
    pass


def MPxTransform_isNonAffineMatricesEnabled(*args, **kwargs):
    pass


def MPxTransform_mustCallValidateAndSet(*args, **kwargs):
    pass


def MPxTransform_setNonAffineMatricesEnabled(*args, **kwargs):
    pass


def MPxTransform_swigregister(*args, **kwargs):
    pass


def MPxTransformationMatrix_convertEulerRotationOrder(*args, **kwargs):
    pass


def MPxTransformationMatrix_convertTransformationRotationOrder(*args, **kwargs):
    pass


def MPxTransformationMatrix_creator(*args, **kwargs):
    pass


def MPxTransformationMatrix_swigregister(*args, **kwargs):
    pass


def MPxUIControl_className(*args, **kwargs):
    pass


def MPxUIControl_swigregister(*args, **kwargs):
    pass


def MPxUITableControl_className(*args, **kwargs):
    pass


def MPxUITableControl_swigregister(*args, **kwargs):
    pass


def MQtUtil_addWidgetToMayaLayout(*args, **kwargs):
    pass


def MQtUtil_className(*args, **kwargs):
    pass


def MQtUtil_deregisterUIType(*args, **kwargs):
    pass


def MQtUtil_findControl(*args, **kwargs):
    pass


def MQtUtil_findLayout(*args, **kwargs):
    pass


def MQtUtil_findMenuItem(*args, **kwargs):
    pass


def MQtUtil_findWindow(*args, **kwargs):
    pass


def MQtUtil_fullName(*args, **kwargs):
    pass


def MQtUtil_getCurrentParent(*args, **kwargs):
    pass


def MQtUtil_getLayoutChildren(*args, **kwargs):
    pass


def MQtUtil_getParent(*args, **kwargs):
    pass


def MQtUtil_mainWindow(*args, **kwargs):
    pass


def MQtUtil_nativeWindow(*args, **kwargs):
    pass


def MQtUtil_registerUIType(*args, **kwargs):
    pass


def MQtUtil_swigregister(*args, **kwargs):
    pass


def MQtUtil_toMString(*args, **kwargs):
    pass


def MQtUtil_toQString(*args, **kwargs):
    pass


def MQuadRender_swigregister(*args, **kwargs):
    pass


def MQuaternion_swigregister(*args, **kwargs):
    pass


def MRampAttribute_className(*args, **kwargs):
    pass


def MRampAttribute_createColorRamp(*args, **kwargs):
    pass


def MRampAttribute_createCurveRamp(*args, **kwargs):
    pass


def MRampAttribute_swigregister(*args, **kwargs):
    pass


def MRasterizerStateDesc_className(*args, **kwargs):
    pass


def MRasterizerStateDesc_swigregister(*args, **kwargs):
    pass


def MRasterizerState_className(*args, **kwargs):
    pass


def MRasterizerState_swigregister(*args, **kwargs):
    pass


def MRenderCallback_addCallback(*args, **kwargs):
    pass


def MRenderCallback_removeCallback(*args, **kwargs):
    pass


def MRenderCallback_swigregister(*args, **kwargs):
    pass


def MRenderData_swigregister(*args, **kwargs):
    pass


def MRenderItemList_className(*args, **kwargs):
    pass


def MRenderItemList_swigregister(*args, **kwargs):
    pass


def MRenderItem_className(*args, **kwargs):
    pass


def MRenderItem_swigregister(*args, **kwargs):
    pass


def MRenderLineArray_className(*args, **kwargs):
    pass


def MRenderLineArray_swigregister(*args, **kwargs):
    pass


def MRenderLine_className(*args, **kwargs):
    pass


def MRenderLine_swigregister(*args, **kwargs):
    pass


def MRenderOperation_swigregister(*args, **kwargs):
    pass


def MRenderOverride_swigregister(*args, **kwargs):
    pass


def MRenderPassDef_className(*args, **kwargs):
    pass


def MRenderPassDef_swigregister(*args, **kwargs):
    pass


def MRenderPassRegistry_className(*args, **kwargs):
    pass


def MRenderPassRegistry_getRenderPassDefinition(*args, **kwargs):
    pass


def MRenderPassRegistry_registerRenderPassDefinition(*args, **kwargs):
    pass


def MRenderPassRegistry_swigregister(*args, **kwargs):
    pass


def MRenderProfile_swigregister(*args, **kwargs):
    pass


def MRenderShadowData_swigregister(*args, **kwargs):
    pass


def MRenderTargetAssignment_swigregister(*args, **kwargs):
    pass


def MRenderTargetDescription_swigregister(*args, **kwargs):
    pass


def MRenderTargetManager_className(*args, **kwargs):
    pass


def MRenderTargetManager_swigregister(*args, **kwargs):
    pass


def MRenderTarget_className(*args, **kwargs):
    pass


def MRenderTarget_swigregister(*args, **kwargs):
    pass


def MRenderUtil_className(*args, **kwargs):
    pass


def MRenderUtil_convertPsdFile(*args, **kwargs):
    pass


def MRenderUtil_diffuseReflectance(*args, **kwargs):
    pass


def MRenderUtil_exactFileTextureName(*args, **kwargs):
    pass


def MRenderUtil_exactImagePlaneFileName(*args, **kwargs):
    pass


def MRenderUtil_generatingIprFile(*args, **kwargs):
    pass


def MRenderUtil_getCommonRenderSettings(*args, **kwargs):
    pass


def MRenderUtil_hemisphereCoverage(*args, **kwargs):
    pass


def MRenderUtil_inCurrentRenderLayer(*args, **kwargs):
    pass


def MRenderUtil_lightAttenuation(*args, **kwargs):
    pass


def MRenderUtil_mainBeautyPassCustomTokenString(*args, **kwargs):
    pass


def MRenderUtil_mainBeautyPassName(*args, **kwargs):
    pass


def MRenderUtil_maximumSpecularReflection(*args, **kwargs):
    pass


def MRenderUtil_mayaRenderState(*args, **kwargs):
    pass


def MRenderUtil_raytrace(*args, **kwargs):
    pass


def MRenderUtil_raytraceFirstGeometryIntersections(*args, **kwargs):
    pass


def MRenderUtil_relativeFileName(*args, **kwargs):
    pass


def MRenderUtil_renderObjectItem(*args, **kwargs):
    pass


def MRenderUtil_renderPass(*args, **kwargs):
    pass


def MRenderUtil_sampleShadingNetwork(*args, **kwargs):
    pass


def MRenderUtil_sendRenderProgressInfo(*args, **kwargs):
    pass


def MRenderUtil_swigregister(*args, **kwargs):
    pass


def MRenderView_className(*args, **kwargs):
    pass


def MRenderView_doesRenderEditorExist(*args, **kwargs):
    pass


def MRenderView_endRender(*args, **kwargs):
    pass


def MRenderView_getRenderRegion(*args, **kwargs):
    pass


def MRenderView_refresh(*args, **kwargs):
    pass


def MRenderView_setCurrentCamera(*args, **kwargs):
    pass


def MRenderView_startRegionRender(*args, **kwargs):
    pass


def MRenderView_startRender(*args, **kwargs):
    pass


def MRenderView_swigregister(*args, **kwargs):
    pass


def MRenderView_updatePixels(*args, **kwargs):
    pass


def MRenderer_setGeometryDrawDirty(*args, **kwargs):
    pass


def MRenderer_swigregister(*args, **kwargs):
    pass


def MRenderer_theRenderer(*args, **kwargs):
    pass


def MRenderingInfo_swigregister(*args, **kwargs):
    pass


def MRichSelection_className(*args, **kwargs):
    pass


def MRichSelection_swigregister(*args, **kwargs):
    pass


def MSamplerStateDesc_className(*args, **kwargs):
    pass


def MSamplerStateDesc_swigregister(*args, **kwargs):
    pass


def MSamplerState_className(*args, **kwargs):
    pass


def MSamplerState_swigregister(*args, **kwargs):
    pass


def MSceneMessage_addCallback(*args, **kwargs):
    pass


def MSceneMessage_addCheckCallback(*args, **kwargs):
    pass


def MSceneMessage_addCheckFileCallback(*args, **kwargs):
    pass


def MSceneMessage_addStringArrayCallback(*args, **kwargs):
    pass


def MSceneMessage_className(*args, **kwargs):
    pass


def MSceneMessage_swigregister(*args, **kwargs):
    pass


def MSceneRender_swigregister(*args, **kwargs):
    pass


def MScriptUtil_createFloatArrayFromList(*args, **kwargs):
    pass


def MScriptUtil_createFloatMatrixFromList(*args, **kwargs):
    pass


def MScriptUtil_createIntArrayFromList(*args, **kwargs):
    pass


def MScriptUtil_createMatrixFromList(*args, **kwargs):
    pass


def MScriptUtil_getBool(*args, **kwargs):
    pass


def MScriptUtil_getBoolArrayItem(*args, **kwargs):
    pass


def MScriptUtil_getChar(*args, **kwargs):
    pass


def MScriptUtil_getCharArrayItem(*args, **kwargs):
    pass


def MScriptUtil_getDouble(*args, **kwargs):
    pass


def MScriptUtil_getDouble2ArrayItem(*args, **kwargs):
    pass


def MScriptUtil_getDouble3ArrayItem(*args, **kwargs):
    pass


def MScriptUtil_getDouble4ArrayItem(*args, **kwargs):
    pass


def MScriptUtil_getDoubleArrayItem(*args, **kwargs):
    pass


def MScriptUtil_getFloat(*args, **kwargs):
    pass


def MScriptUtil_getFloat2ArrayItem(*args, **kwargs):
    pass


def MScriptUtil_getFloat3ArrayItem(*args, **kwargs):
    pass


def MScriptUtil_getFloat4ArrayItem(*args, **kwargs):
    pass


def MScriptUtil_getFloatArrayItem(*args, **kwargs):
    pass


def MScriptUtil_getInt(*args, **kwargs):
    pass


def MScriptUtil_getInt2ArrayItem(*args, **kwargs):
    pass


def MScriptUtil_getInt3ArrayItem(*args, **kwargs):
    pass


def MScriptUtil_getInt4ArrayItem(*args, **kwargs):
    pass


def MScriptUtil_getIntArrayItem(*args, **kwargs):
    pass


def MScriptUtil_getShort(*args, **kwargs):
    pass


def MScriptUtil_getShort2ArrayItem(*args, **kwargs):
    pass


def MScriptUtil_getShort3ArrayItem(*args, **kwargs):
    pass


def MScriptUtil_getShort4ArrayItem(*args, **kwargs):
    pass


def MScriptUtil_getShortArrayItem(*args, **kwargs):
    pass


def MScriptUtil_getUchar(*args, **kwargs):
    pass


def MScriptUtil_getUcharArrayItem(*args, **kwargs):
    pass


def MScriptUtil_getUint(*args, **kwargs):
    pass


def MScriptUtil_getUint2ArrayItem(*args, **kwargs):
    pass


def MScriptUtil_getUint3ArrayItem(*args, **kwargs):
    pass


def MScriptUtil_getUint4ArrayItem(*args, **kwargs):
    pass


def MScriptUtil_getUintArrayItem(*args, **kwargs):
    pass


def MScriptUtil_setBool(*args, **kwargs):
    pass


def MScriptUtil_setBoolArray(*args, **kwargs):
    pass


def MScriptUtil_setChar(*args, **kwargs):
    pass


def MScriptUtil_setCharArray(*args, **kwargs):
    pass


def MScriptUtil_setDouble(*args, **kwargs):
    pass


def MScriptUtil_setDouble2ArrayItem(*args, **kwargs):
    pass


def MScriptUtil_setDouble3ArrayItem(*args, **kwargs):
    pass


def MScriptUtil_setDouble4ArrayItem(*args, **kwargs):
    pass


def MScriptUtil_setDoubleArray(*args, **kwargs):
    pass


def MScriptUtil_setFloat(*args, **kwargs):
    pass


def MScriptUtil_setFloat2ArrayItem(*args, **kwargs):
    pass


def MScriptUtil_setFloat3ArrayItem(*args, **kwargs):
    pass


def MScriptUtil_setFloat4ArrayItem(*args, **kwargs):
    pass


def MScriptUtil_setFloatArray(*args, **kwargs):
    pass


def MScriptUtil_setInt(*args, **kwargs):
    pass


def MScriptUtil_setInt2ArrayItem(*args, **kwargs):
    pass


def MScriptUtil_setInt3ArrayItem(*args, **kwargs):
    pass


def MScriptUtil_setInt4ArrayItem(*args, **kwargs):
    pass


def MScriptUtil_setIntArray(*args, **kwargs):
    pass


def MScriptUtil_setShort(*args, **kwargs):
    pass


def MScriptUtil_setShort2ArrayItem(*args, **kwargs):
    pass


def MScriptUtil_setShort3ArrayItem(*args, **kwargs):
    pass


def MScriptUtil_setShort4ArrayItem(*args, **kwargs):
    pass


def MScriptUtil_setShortArray(*args, **kwargs):
    pass


def MScriptUtil_setUchar(*args, **kwargs):
    pass


def MScriptUtil_setUcharArray(*args, **kwargs):
    pass


def MScriptUtil_setUint(*args, **kwargs):
    pass


def MScriptUtil_setUint2ArrayItem(*args, **kwargs):
    pass


def MScriptUtil_setUint3ArrayItem(*args, **kwargs):
    pass


def MScriptUtil_setUint4ArrayItem(*args, **kwargs):
    pass


def MScriptUtil_setUintArray(*args, **kwargs):
    pass


def MScriptUtil_swigregister(*args, **kwargs):
    pass


def MSeamInfo_swigregister(*args, **kwargs):
    pass


def MSelectInfo_className(*args, **kwargs):
    pass


def MSelectInfo_swigregister(*args, **kwargs):
    pass


def MSelectionList_className(*args, **kwargs):
    pass


def MSelectionList_swigregister(*args, **kwargs):
    pass


def MSelectionMask_swigregister(*args, **kwargs):
    pass


def MShaderCompileMacro_swigregister(*args, **kwargs):
    pass


def MShaderInstance_className(*args, **kwargs):
    pass


def MShaderInstance_swigregister(*args, **kwargs):
    pass


def MShaderManager_className(*args, **kwargs):
    pass


def MShaderManager_swigregister(*args, **kwargs):
    pass


def MSpace_swigregister(*args, **kwargs):
    pass


def MStateManager_className(*args, **kwargs):
    pass


def MStateManager_swigregister(*args, **kwargs):
    pass


def MStatus_swigregister(*args, **kwargs):
    pass


def MStencilOpDesc_className(*args, **kwargs):
    pass


def MStencilOpDesc_swigregister(*args, **kwargs):
    pass


def MStreamUtils_readChar(*args, **kwargs):
    pass


def MStreamUtils_readCharBuffer(*args, **kwargs):
    pass


def MStreamUtils_readDouble(*args, **kwargs):
    pass


def MStreamUtils_readFloat(*args, **kwargs):
    pass


def MStreamUtils_readInt(*args, **kwargs):
    pass


def MStreamUtils_stdErrorStream(*args, **kwargs):
    pass


def MStreamUtils_stdOutStream(*args, **kwargs):
    pass


def MStreamUtils_swigregister(*args, **kwargs):
    pass


def MStreamUtils_writeChar(*args, **kwargs):
    pass


def MStreamUtils_writeCharBuffer(*args, **kwargs):
    pass


def MStreamUtils_writeDouble(*args, **kwargs):
    pass


def MStreamUtils_writeFloat(*args, **kwargs):
    pass


def MStreamUtils_writeInt(*args, **kwargs):
    pass


def MSwatchRenderBase_swigregister(*args, **kwargs):
    pass


def MSwatchRenderRegister_registerSwatchRender(*args, **kwargs):
    pass


def MSwatchRenderRegister_swigregister(*args, **kwargs):
    pass


def MSwatchRenderRegister_unregisterSwatchRender(*args, **kwargs):
    pass


def MSyntax_className(*args, **kwargs):
    pass


def MSyntax_swigregister(*args, **kwargs):
    pass


def MTargetBlendDesc_className(*args, **kwargs):
    pass


def MTargetBlendDesc_swigregister(*args, **kwargs):
    pass


def MTesselationParams_className(*args, **kwargs):
    pass


def MTesselationParams_swigregister(*args, **kwargs):
    pass


def MTextureAssignment_swigregister(*args, **kwargs):
    pass


def MTextureDescription_swigregister(*args, **kwargs):
    pass


def MTextureEditorDrawInfo_className(*args, **kwargs):
    pass


def MTextureEditorDrawInfo_swigregister(*args, **kwargs):
    pass


def MTextureManager_className(*args, **kwargs):
    pass


def MTextureManager_swigregister(*args, **kwargs):
    pass


def MTexture_className(*args, **kwargs):
    pass


def MTexture_swigregister(*args, **kwargs):
    pass


def MTimeArray_className(*args, **kwargs):
    pass


def MTimeArray_swigregister(*args, **kwargs):
    pass


def MTime_className(*args, **kwargs):
    pass


def MTime_setUIUnit(*args, **kwargs):
    pass


def MTime_swigregister(*args, **kwargs):
    pass


def MTime_uiUnit(*args, **kwargs):
    pass


def MTimerMessage_addTimerCallback(*args, **kwargs):
    pass


def MTimerMessage_className(*args, **kwargs):
    pass


def MTimerMessage_setSleepCallback(*args, **kwargs):
    pass


def MTimerMessage_sleepCallback(*args, **kwargs):
    pass


def MTimerMessage_swigregister(*args, **kwargs):
    pass


def MTimer_swigregister(*args, **kwargs):
    pass


def MToolsInfo_className(*args, **kwargs):
    pass


def MToolsInfo_isDirty(*args, **kwargs):
    pass


def MToolsInfo_resetDirtyFlag(*args, **kwargs):
    pass


def MToolsInfo_setDirtyFlag(*args, **kwargs):
    pass


def MToolsInfo_swigregister(*args, **kwargs):
    pass


def MTransformationMatrix_className(*args, **kwargs):
    pass


def MTransformationMatrix_swigregister(*args, **kwargs):
    pass


def MTrimBoundaryArray_className(*args, **kwargs):
    pass


def MTrimBoundaryArray_swigregister(*args, **kwargs):
    pass


def MTypeId_className(*args, **kwargs):
    pass


def MTypeId_swigregister(*args, **kwargs):
    pass


def MUiMessage_add3dViewDestroyMsgCallback(*args, **kwargs):
    pass


def MUiMessage_add3dViewPostMultipleDrawPassMsgCallback(*args, **kwargs):
    pass


def MUiMessage_add3dViewPostRenderMsgCallback(*args, **kwargs):
    pass


def MUiMessage_add3dViewPreMultipleDrawPassMsgCallback(*args, **kwargs):
    pass


def MUiMessage_add3dViewPreRenderMsgCallback(*args, **kwargs):
    pass


def MUiMessage_addCameraChangedCallback(*args, **kwargs):
    pass


def MUiMessage_addUiDeletedCallback(*args, **kwargs):
    pass


def MUiMessage_className(*args, **kwargs):
    pass


def MUiMessage_swigregister(*args, **kwargs):
    pass


def MUint64Array_className(*args, **kwargs):
    pass


def MUint64Array_swigregister(*args, **kwargs):
    pass


def MUintArray_className(*args, **kwargs):
    pass


def MUintArray_swigregister(*args, **kwargs):
    pass


def MUniformParameterList_swigregister(*args, **kwargs):
    pass


def MUniformParameter_swigregister(*args, **kwargs):
    pass


def MUserData_className(*args, **kwargs):
    pass


def MUserData_swigregister(*args, **kwargs):
    pass


def MUserEventMessage_addUserEventCallback(*args, **kwargs):
    pass


def MUserEventMessage_className(*args, **kwargs):
    pass


def MUserEventMessage_deregisterUserEvent(*args, **kwargs):
    pass


def MUserEventMessage_isUserEvent(*args, **kwargs):
    pass


def MUserEventMessage_postUserEvent(*args, **kwargs):
    pass


def MUserEventMessage_registerUserEvent(*args, **kwargs):
    pass


def MUserEventMessage_swigregister(*args, **kwargs):
    pass


def MUserRenderOperation_swigregister(*args, **kwargs):
    pass


def MVaryingParameterList_swigregister(*args, **kwargs):
    pass


def MVaryingParameter_swigregister(*args, **kwargs):
    pass


def MVectorArray_className(*args, **kwargs):
    pass


def MVectorArray_swigregister(*args, **kwargs):
    pass


def MVector_swigregister(*args, **kwargs):
    pass


def MVertexBufferDescriptorList_className(*args, **kwargs):
    pass


def MVertexBufferDescriptorList_swigregister(*args, **kwargs):
    pass


def MVertexBufferDescriptor_className(*args, **kwargs):
    pass


def MVertexBufferDescriptor_swigregister(*args, **kwargs):
    pass


def MVertexBuffer_className(*args, **kwargs):
    pass


def MVertexBuffer_swigregister(*args, **kwargs):
    pass


def MViewportRenderer_swigregister(*args, **kwargs):
    pass


def MWeight_className(*args, **kwargs):
    pass


def MWeight_swigregister(*args, **kwargs):
    pass


def MaterialInputData_swigregister(*args, **kwargs):
    pass


def MnCloth_swigregister(*args, **kwargs):
    pass


def MnObject_swigregister(*args, **kwargs):
    pass


def MnParticle_swigregister(*args, **kwargs):
    pass


def MnRigid_swigregister(*args, **kwargs):
    pass


def MnSolver_swigregister(*args, **kwargs):
    pass


def RV_PIXEL_swigregister(*args, **kwargs):
    pass


def array2dDouble_swigregister(*args, **kwargs):
    pass


def array2dFloat_swigregister(*args, **kwargs):
    pass


def array3dDouble_swigregister(*args, **kwargs):
    pass


def array3dFloat_swigregister(*args, **kwargs):
    pass


def array3dInt_swigregister(*args, **kwargs):
    pass


def array4dDouble_swigregister(*args, **kwargs):
    pass


def array4dFloat_swigregister(*args, **kwargs):
    pass


def array4dInt_swigregister(*args, **kwargs):
    pass


def boolPtr_frompointer(*args, **kwargs):
    pass


def boolPtr_swigregister(*args, **kwargs):
    pass


def charPtr_frompointer(*args, **kwargs):
    pass


def charPtr_swigregister(*args, **kwargs):
    pass


def doublePtr_frompointer(*args, **kwargs):
    pass


def doublePtr_swigregister(*args, **kwargs):
    pass


def floatPtr_frompointer(*args, **kwargs):
    pass


def floatPtr_swigregister(*args, **kwargs):
    pass


def getLockCaptureCount(*args, **kwargs):
    pass


def intPtr_frompointer(*args, **kwargs):
    pass


def intPtr_swigregister(*args, **kwargs):
    pass


def shortPtr_frompointer(*args, **kwargs):
    pass


def shortPtr_swigregister(*args, **kwargs):
    pass


def uCharPtr_frompointer(*args, **kwargs):
    pass


def uCharPtr_swigregister(*args, **kwargs):
    pass


def uIntPtr_frompointer(*args, **kwargs):
    pass


def uIntPtr_swigregister(*args, **kwargs):
    pass


def weakref_proxy(*args, **kwargs):
    """
    proxy(object[, callback]) -- create a proxy object that weakly
    references 'object'.  'callback', if given, is called with a
    reference to the proxy when 'object' is about to be finalized.
    """

    pass

MAYA_API_VERSION = None

MFloatMatrix_kTol = None

MFloatPoint_kTol = None

MFloatVector_kTol = None

MGL_2D = None

MGL_2X_BIT_ATI = None

MGL_2_BYTES = None

MGL_3D = None

MGL_3D_COLOR = None

MGL_3D_COLOR_TEXTURE = None

MGL_3_BYTES = None

MGL_4D_COLOR_TEXTURE = None

MGL_4X_BIT_ATI = None

MGL_4_BYTES = None

MGL_8X_BIT_ATI = None

MGL_ABGR_EXT = None

MGL_ACCUM = None

MGL_ACCUM_ALPHA_BITS = None

MGL_ACCUM_BLUE_BITS = None

MGL_ACCUM_BUFFER_BIT = None

MGL_ACCUM_CLEAR_VALUE = None

MGL_ACCUM_GREEN_BITS = None

MGL_ACCUM_RED_BITS = None

MGL_ACTIVE_TEXTURE = None

MGL_ACTIVE_TEXTURE_ARB = None

MGL_ADD = None

MGL_ADD_ATI = None

MGL_ADD_SIGNED = None

MGL_ADD_SIGNED_EXT = None

MGL_ALL_ATTRIB_BITS = None

MGL_ALL_COMPLETED_NV = None

MGL_ALPHA = None

MGL_ALPHA12 = None

MGL_ALPHA16 = None

MGL_ALPHA16F = None

MGL_ALPHA32F = None

MGL_ALPHA4 = None

MGL_ALPHA8 = None

MGL_ALPHA_BIAS = None

MGL_ALPHA_BITS = None

MGL_ALPHA_SCALE = None

MGL_ALPHA_TEST = None

MGL_ALPHA_TEST_FUNC = None

MGL_ALPHA_TEST_REF = None

MGL_ALWAYS = None

MGL_AMBIENT = None

MGL_AMBIENT_AND_DIFFUSE = None

MGL_AND = None

MGL_AND_INVERTED = None

MGL_AND_REVERSE = None

MGL_ARB_depth_texture = None

MGL_ARB_shadow = None

MGL_ARB_shadow_ambient = None

MGL_ARB_texture_env_dot3 = None

MGL_ARRAY_BUFFER_ARB = None

MGL_ARRAY_BUFFER_BINDING_ARB = None

MGL_ARRAY_ELEMENT_LOCK_COUNT_EXT = None

MGL_ARRAY_ELEMENT_LOCK_FIRST_EXT = None

MGL_ATTRIB_ARRAY_POINTER_NV = None

MGL_ATTRIB_ARRAY_SIZE_NV = None

MGL_ATTRIB_ARRAY_STRIDE_NV = None

MGL_ATTRIB_ARRAY_TYPE_NV = None

MGL_ATTRIB_STACK_DEPTH = None

MGL_AUTO_NORMAL = None

MGL_AUX0 = None

MGL_AUX1 = None

MGL_AUX2 = None

MGL_AUX3 = None

MGL_AUX_BUFFERS = None

MGL_BACK = None

MGL_BACK_LEFT = None

MGL_BACK_RIGHT = None

MGL_BGRA_EXT = None

MGL_BGR_EXT = None

MGL_BIAS_BIT_ATI = None

MGL_BIAS_BY_NEGATIVE_ONE_HALF_NV = None

MGL_BITMAP = None

MGL_BITMAP_TOKEN = None

MGL_BLEND = None

MGL_BLEND_COLOR = None

MGL_BLEND_COLOR_EXT = None

MGL_BLEND_DST = None

MGL_BLEND_EQUATION = None

MGL_BLEND_EQUATION_EXT = None

MGL_BLEND_SRC = None

MGL_BLUE = None

MGL_BLUE_BIAS = None

MGL_BLUE_BITS = None

MGL_BLUE_BIT_ATI = None

MGL_BLUE_SCALE = None

MGL_BOOL_ARB = None

MGL_BOOL_VEC2_ARB = None

MGL_BOOL_VEC3_ARB = None

MGL_BOOL_VEC4_ARB = None

MGL_BUFFER_ACCESS_ARB = None

MGL_BUFFER_MAPPED_ARB = None

MGL_BUFFER_MAP_POINTER_ARB = None

MGL_BUFFER_SIZE_ARB = None

MGL_BUFFER_USAGE_ARB = None

MGL_BYTE = None

MGL_C3F_V3F = None

MGL_C4F_N3F_V3F = None

MGL_C4UB_V2F = None

MGL_C4UB_V3F = None

MGL_CCW = None

MGL_CLAMP = None

MGL_CLAMP_FRAGMENT_COLOR = None

MGL_CLAMP_READ_COLOR = None

MGL_CLAMP_TO_BORDER = None

MGL_CLAMP_TO_BORDER_ARB = None

MGL_CLAMP_TO_BORDER_SGIS = None

MGL_CLAMP_TO_EDGE = None

MGL_CLAMP_VERTEX_COLOR = None

MGL_CLEAR = None

MGL_CLIENT_ACTIVE_TEXTURE = None

MGL_CLIENT_ACTIVE_TEXTURE_ARB = None

MGL_CLIENT_ALL_ATTRIB_BITS = None

MGL_CLIENT_ATTRIB_STACK_DEPTH = None

MGL_CLIENT_PIXEL_STORE_BIT = None

MGL_CLIENT_VERTEX_ARRAY_BIT = None

MGL_CLIP_PLANE0 = None

MGL_CLIP_PLANE1 = None

MGL_CLIP_PLANE2 = None

MGL_CLIP_PLANE3 = None

MGL_CLIP_PLANE4 = None

MGL_CLIP_PLANE5 = None

MGL_CLIP_VOLUME_CLIPPING_HINT_EXT = None

MGL_CND0_ATI = None

MGL_CND_ATI = None

MGL_COEFF = None

MGL_COLOR = None

MGL_COLOR_ALPHA_PAIRING_ATI = None

MGL_COLOR_ARRAY = None

MGL_COLOR_ARRAY_BUFFER_BINDING_ARB = None

MGL_COLOR_ARRAY_COUNT_EXT = None

MGL_COLOR_ARRAY_EXT = None

MGL_COLOR_ARRAY_POINTER = None

MGL_COLOR_ARRAY_POINTER_EXT = None

MGL_COLOR_ARRAY_SIZE = None

MGL_COLOR_ARRAY_SIZE_EXT = None

MGL_COLOR_ARRAY_STRIDE = None

MGL_COLOR_ARRAY_STRIDE_EXT = None

MGL_COLOR_ARRAY_TYPE = None

MGL_COLOR_ARRAY_TYPE_EXT = None

MGL_COLOR_BUFFER_BIT = None

MGL_COLOR_CLEAR_UNCLAMPED_VALUE_ATI = None

MGL_COLOR_CLEAR_VALUE = None

MGL_COLOR_INDEX = None

MGL_COLOR_INDEX12_EXT = None

MGL_COLOR_INDEX16_EXT = None

MGL_COLOR_INDEX1_EXT = None

MGL_COLOR_INDEX2_EXT = None

MGL_COLOR_INDEX4_EXT = None

MGL_COLOR_INDEX8_EXT = None

MGL_COLOR_INDEXES = None

MGL_COLOR_LOGIC_OP = None

MGL_COLOR_MATERIAL = None

MGL_COLOR_MATERIAL_FACE = None

MGL_COLOR_MATERIAL_PARAMETER = None

MGL_COLOR_SUM = None

MGL_COLOR_SUM_CLAMP_NV = None

MGL_COLOR_SUM_EXT = None

MGL_COLOR_TABLE_ALPHA_SIZE_EXT = None

MGL_COLOR_TABLE_BLUE_SIZE_EXT = None

MGL_COLOR_TABLE_FORMAT_EXT = None

MGL_COLOR_TABLE_GREEN_SIZE_EXT = None

MGL_COLOR_TABLE_INTENSITY_SIZE_EXT = None

MGL_COLOR_TABLE_LUMINANCE_SIZE_EXT = None

MGL_COLOR_TABLE_RED_SIZE_EXT = None

MGL_COLOR_TABLE_WIDTH_EXT = None

MGL_COLOR_WRITEMASK = None

MGL_COMBINE = None

MGL_COMBINE4_NV = None

MGL_COMBINER0_NV = None

MGL_COMBINER1_NV = None

MGL_COMBINER2_NV = None

MGL_COMBINER3_NV = None

MGL_COMBINER4_NV = None

MGL_COMBINER5_NV = None

MGL_COMBINER6_NV = None

MGL_COMBINER7_NV = None

MGL_COMBINER_AB_DOT_PRODUCT_NV = None

MGL_COMBINER_AB_OUTPUT_NV = None

MGL_COMBINER_BIAS_NV = None

MGL_COMBINER_CD_DOT_PRODUCT_NV = None

MGL_COMBINER_CD_OUTPUT_NV = None

MGL_COMBINER_COMPONENT_USAGE_NV = None

MGL_COMBINER_INPUT_NV = None

MGL_COMBINER_MAPPING_NV = None

MGL_COMBINER_MUX_SUM_NV = None

MGL_COMBINER_SCALE_NV = None

MGL_COMBINER_SUM_OUTPUT_NV = None

MGL_COMBINE_ALPHA = None

MGL_COMBINE_ALPHA_EXT = None

MGL_COMBINE_EXT = None

MGL_COMBINE_RGB = None

MGL_COMBINE_RGB_EXT = None

MGL_COMPARE_R_TO_TEXTURE_ARB = None

MGL_COMPILE = None

MGL_COMPILE_AND_EXECUTE = None

MGL_COMPRESSED_ALPHA = None

MGL_COMPRESSED_ALPHA_ARB = None

MGL_COMPRESSED_INTENSITY = None

MGL_COMPRESSED_INTENSITY_ARB = None

MGL_COMPRESSED_LUMINANCE = None

MGL_COMPRESSED_LUMINANCE_ALPHA = None

MGL_COMPRESSED_LUMINANCE_ALPHA_ARB = None

MGL_COMPRESSED_LUMINANCE_ARB = None

MGL_COMPRESSED_RGB = None

MGL_COMPRESSED_RGBA = None

MGL_COMPRESSED_RGBA_ARB = None

MGL_COMPRESSED_RGBA_S3TC_DXT1_EXT = None

MGL_COMPRESSED_RGBA_S3TC_DXT3_EXT = None

MGL_COMPRESSED_RGBA_S3TC_DXT5_EXT = None

MGL_COMPRESSED_RGB_ARB = None

MGL_COMPRESSED_RGB_S3TC_DXT1_EXT = None

MGL_COMPRESSED_TEXTURE_FORMATS = None

MGL_COMPRESSED_TEXTURE_FORMATS_ARB = None

MGL_COMP_BIT_ATI = None

MGL_CONSTANT = None

MGL_CONSTANT_ALPHA = None

MGL_CONSTANT_ALPHA_EXT = None

MGL_CONSTANT_ATTENUATION = None

MGL_CONSTANT_COLOR = None

MGL_CONSTANT_COLOR0_NV = None

MGL_CONSTANT_COLOR1_NV = None

MGL_CONSTANT_COLOR_EXT = None

MGL_CONSTANT_EXT = None

MGL_CONST_EYE_NV = None

MGL_CON_0_ATI = None

MGL_CON_10_ATI = None

MGL_CON_11_ATI = None

MGL_CON_12_ATI = None

MGL_CON_13_ATI = None

MGL_CON_14_ATI = None

MGL_CON_15_ATI = None

MGL_CON_16_ATI = None

MGL_CON_17_ATI = None

MGL_CON_18_ATI = None

MGL_CON_19_ATI = None

MGL_CON_1_ATI = None

MGL_CON_20_ATI = None

MGL_CON_21_ATI = None

MGL_CON_22_ATI = None

MGL_CON_23_ATI = None

MGL_CON_24_ATI = None

MGL_CON_25_ATI = None

MGL_CON_26_ATI = None

MGL_CON_27_ATI = None

MGL_CON_28_ATI = None

MGL_CON_29_ATI = None

MGL_CON_2_ATI = None

MGL_CON_30_ATI = None

MGL_CON_31_ATI = None

MGL_CON_3_ATI = None

MGL_CON_4_ATI = None

MGL_CON_5_ATI = None

MGL_CON_6_ATI = None

MGL_CON_7_ATI = None

MGL_CON_8_ATI = None

MGL_CON_9_ATI = None

MGL_COPY = None

MGL_COPY_INVERTED = None

MGL_COPY_PIXEL_TOKEN = None

MGL_CULL_FACE = None

MGL_CULL_FACE_MODE = None

MGL_CULL_FRAGMENT_NV = None

MGL_CULL_MODES_NV = None

MGL_CULL_VERTEX_EXT = None

MGL_CULL_VERTEX_EYE_POSITION_EXT = None

MGL_CULL_VERTEX_OBJECT_POSITION_EXT = None

MGL_CURRENT_ATTRIB_NV = None

MGL_CURRENT_BIT = None

MGL_CURRENT_COLOR = None

MGL_CURRENT_FOG_COORDINATE_EXT = None

MGL_CURRENT_INDEX = None

MGL_CURRENT_MATRIX = None

MGL_CURRENT_MATRIX_NV = None

MGL_CURRENT_MATRIX_STACK_DEPTH = None

MGL_CURRENT_MATRIX_STACK_DEPTH_NV = None

MGL_CURRENT_NORMAL = None

MGL_CURRENT_OCCLUSION_QUERY_ID_NV = None

MGL_CURRENT_QUERY_ARB = None

MGL_CURRENT_RASTER_COLOR = None

MGL_CURRENT_RASTER_DISTANCE = None

MGL_CURRENT_RASTER_INDEX = None

MGL_CURRENT_RASTER_POSITION = None

MGL_CURRENT_RASTER_POSITION_VALID = None

MGL_CURRENT_RASTER_TEXTURE_COORDS = None

MGL_CURRENT_SECONDARY_COLOR_EXT = None

MGL_CURRENT_TEXTURE_COORDS = None

MGL_CURRENT_VERTEX_ATTRIB = None

MGL_CURRENT_VERTEX_EXT = None

MGL_CURRENT_VERTEX_WEIGHT_EXT = None

MGL_CW = None

MGL_DECAL = None

MGL_DECR = None

MGL_DECR_WRAP_EXT = None

MGL_DEPENDENT_AR_TEXTURE_2D_NV = None

MGL_DEPENDENT_GB_TEXTURE_2D_NV = None

MGL_DEPTH = None

MGL_DEPTH_BIAS = None

MGL_DEPTH_BITS = None

MGL_DEPTH_BUFFER_BIT = None

MGL_DEPTH_CLEAR_VALUE = None

MGL_DEPTH_COMPONENT = None

MGL_DEPTH_COMPONENT16_ARB = None

MGL_DEPTH_COMPONENT24_ARB = None

MGL_DEPTH_COMPONENT32_ARB = None

MGL_DEPTH_FUNC = None

MGL_DEPTH_RANGE = None

MGL_DEPTH_SCALE = None

MGL_DEPTH_TEST = None

MGL_DEPTH_TEXTURE_MODE_ARB = None

MGL_DEPTH_WRITEMASK = None

MGL_DIFFUSE = None

MGL_DISCARD_NV = None

MGL_DITHER = None

MGL_DOMAIN = None

MGL_DONT_CARE = None

MGL_DOT2_ADD_ATI = None

MGL_DOT3_ATI = None

MGL_DOT3_RGB = None

MGL_DOT3_RGBA = None

MGL_DOT3_RGBA_ARB = None

MGL_DOT3_RGBA_EXT = None

MGL_DOT3_RGB_ARB = None

MGL_DOT3_RGB_EXT = None

MGL_DOT4_ATI = None

MGL_DOT_PRODUCT_CONST_EYE_REFLECT_CUBE_MAP_NV = None

MGL_DOT_PRODUCT_DEPTH_REPLACE_NV = None

MGL_DOT_PRODUCT_DIFFUSE_CUBE_MAP_NV = None

MGL_DOT_PRODUCT_NV = None

MGL_DOT_PRODUCT_REFLECT_CUBE_MAP_NV = None

MGL_DOT_PRODUCT_TEXTURE_2D_NV = None

MGL_DOT_PRODUCT_TEXTURE_3D_NV = None

MGL_DOT_PRODUCT_TEXTURE_CUBE_MAP_NV = None

MGL_DOUBLE = None

MGL_DOUBLEBUFFER = None

MGL_DRAW_BUFFER = None

MGL_DRAW_PIXEL_TOKEN = None

MGL_DSDT8_MAG8_INTENSITY8_NV = None

MGL_DSDT8_MAG8_NV = None

MGL_DSDT8_NV = None

MGL_DSDT_MAG_INTENSITY_NV = None

MGL_DSDT_MAG_NV = None

MGL_DSDT_MAG_VIB_NV = None

MGL_DSDT_NV = None

MGL_DST_ALPHA = None

MGL_DST_COLOR = None

MGL_DS_BIAS_NV = None

MGL_DS_SCALE_NV = None

MGL_DT_BIAS_NV = None

MGL_DT_SCALE_NV = None

MGL_DYNAMIC_COPY_ARB = None

MGL_DYNAMIC_DRAW_ARB = None

MGL_DYNAMIC_READ_ARB = None

MGL_EDGE_FLAG = None

MGL_EDGE_FLAG_ARRAY = None

MGL_EDGE_FLAG_ARRAY_BUFFER_BINDING_ARB = None

MGL_EDGE_FLAG_ARRAY_COUNT_EXT = None

MGL_EDGE_FLAG_ARRAY_EXT = None

MGL_EDGE_FLAG_ARRAY_POINTER = None

MGL_EDGE_FLAG_ARRAY_POINTER_EXT = None

MGL_EDGE_FLAG_ARRAY_STRIDE = None

MGL_EDGE_FLAG_ARRAY_STRIDE_EXT = None

MGL_EIGHTH_BIT_ATI = None

MGL_ELEMENT_ARRAY_BUFFER_ARB = None

MGL_ELEMENT_ARRAY_BUFFER_BINDING_ARB = None

MGL_EMBOSS_CONSTANT_NV = None

MGL_EMBOSS_LIGHT_NV = None

MGL_EMBOSS_MAP_NV = None

MGL_EMISSION = None

MGL_ENABLE_BIT = None

MGL_EQUAL = None

MGL_EQUIV = None

MGL_EVAL_BIT = None

MGL_EXP = None

MGL_EXP2 = None

MGL_EXPAND_NEGATE_NV = None

MGL_EXPAND_NORMAL_NV = None

MGL_EXTENSIONS = None

MGL_EXT_vertex_shader = None

MGL_EYE_LINEAR = None

MGL_EYE_PLANE = None

MGL_E_TIMES_F_NV = None

MGL_FALSE = None

MGL_FASTEST = None

MGL_FEEDBACK = None

MGL_FEEDBACK_BUFFER_POINTER = None

MGL_FEEDBACK_BUFFER_SIZE = None

MGL_FEEDBACK_BUFFER_TYPE = None

MGL_FENCE_CONDITION_NV = None

MGL_FENCE_STATUS_NV = None

MGL_FILL = None

MGL_FIXED_ONLY = None

MGL_FLAT = None

MGL_FLOAT = None

MGL_FLOAT_CLEAR_COLOR_VALUE_NV = None

MGL_FLOAT_MAT2_ARB = None

MGL_FLOAT_MAT3_ARB = None

MGL_FLOAT_MAT4_ARB = None

MGL_FLOAT_R16_NV = None

MGL_FLOAT_R32_NV = None

MGL_FLOAT_RG16_NV = None

MGL_FLOAT_RG32_NV = None

MGL_FLOAT_RGB16_NV = None

MGL_FLOAT_RGB32_NV = None

MGL_FLOAT_RGBA16_NV = None

MGL_FLOAT_RGBA32_NV = None

MGL_FLOAT_RGBA_MODE_NV = None

MGL_FLOAT_RGBA_NV = None

MGL_FLOAT_RGB_NV = None

MGL_FLOAT_RG_NV = None

MGL_FLOAT_R_NV = None

MGL_FLOAT_VEC2_ARB = None

MGL_FLOAT_VEC3_ARB = None

MGL_FLOAT_VEC4_ARB = None

MGL_FOG = None

MGL_FOG_BIT = None

MGL_FOG_COLOR = None

MGL_FOG_COORDINATE_ARRAY_BUFFER_BINDING_ARB = None

MGL_FOG_COORDINATE_ARRAY_EXT = None

MGL_FOG_COORDINATE_ARRAY_POINTER_EXT = None

MGL_FOG_COORDINATE_ARRAY_STRIDE_EXT = None

MGL_FOG_COORDINATE_ARRAY_TYPE_EXT = None

MGL_FOG_COORDINATE_EXT = None

MGL_FOG_COORDINATE_SOURCE_EXT = None

MGL_FOG_DENSITY = None

MGL_FOG_END = None

MGL_FOG_HINT = None

MGL_FOG_INDEX = None

MGL_FOG_MODE = None

MGL_FOG_SPECULAR_TEXTURE_WIN = None

MGL_FOG_START = None

MGL_FRAGMENT_DEPTH_EXT = None

MGL_FRAGMENT_PROGRAM_ARB = None

MGL_FRAGMENT_PROGRAM_BINDING_NV = None

MGL_FRAGMENT_PROGRAM_NV = None

MGL_FRAGMENT_SHADER_ARB = None

MGL_FRAGMENT_SHADER_ATI = None

MGL_FRONT = None

MGL_FRONT_AND_BACK = None

MGL_FRONT_FACE = None

MGL_FRONT_LEFT = None

MGL_FRONT_RIGHT = None

MGL_FULL_RANGE_EXT = None

MGL_FUNC_ADD = None

MGL_FUNC_ADD_EXT = None

MGL_FUNC_REVERSE_SUBTRACT = None

MGL_FUNC_REVERSE_SUBTRACT_EXT = None

MGL_FUNC_SUBTRACT = None

MGL_FUNC_SUBTRACT_EXT = None

MGL_GENERATE_MIPMAP_SGIS = None

MGL_GEQUAL = None

MGL_GREATER = None

MGL_GREEN = None

MGL_GREEN_BIAS = None

MGL_GREEN_BITS = None

MGL_GREEN_BIT_ATI = None

MGL_GREEN_SCALE = None

MGL_HALF_BIAS_NEGATE_NV = None

MGL_HALF_BIAS_NORMAL_NV = None

MGL_HALF_BIT_ATI = None

MGL_HALF_FLOAT = None

MGL_HILO16_NV = None

MGL_HILO_NV = None

MGL_HINT_BIT = None

MGL_HI_BIAS_NV = None

MGL_HI_SCALE_NV = None

MGL_IBM_TEXTURE_MIRRORED_REPEAT = None

MGL_IDENTITY_NV = None

MGL_INCR = None

MGL_INCR_WRAP_EXT = None

MGL_INDEX_ARRAY = None

MGL_INDEX_ARRAY_BUFFER_BINDING_ARB = None

MGL_INDEX_ARRAY_COUNT_EXT = None

MGL_INDEX_ARRAY_EXT = None

MGL_INDEX_ARRAY_POINTER = None

MGL_INDEX_ARRAY_POINTER_EXT = None

MGL_INDEX_ARRAY_STRIDE = None

MGL_INDEX_ARRAY_STRIDE_EXT = None

MGL_INDEX_ARRAY_TYPE = None

MGL_INDEX_ARRAY_TYPE_EXT = None

MGL_INDEX_BITS = None

MGL_INDEX_CLEAR_VALUE = None

MGL_INDEX_LOGIC_OP = None

MGL_INDEX_MODE = None

MGL_INDEX_OFFSET = None

MGL_INDEX_SHIFT = None

MGL_INDEX_WRITEMASK = None

MGL_INT = None

MGL_INTENSITY = None

MGL_INTENSITY12 = None

MGL_INTENSITY16 = None

MGL_INTENSITY16F = None

MGL_INTENSITY32F = None

MGL_INTENSITY4 = None

MGL_INTENSITY8 = None

MGL_INTERPOLATE = None

MGL_INTERPOLATE_EXT = None

MGL_INT_VEC2_ARB = None

MGL_INT_VEC3_ARB = None

MGL_INT_VEC4_ARB = None

MGL_INVALID_ENUM = None

MGL_INVALID_OPERATION = None

MGL_INVALID_VALUE = None

MGL_INVARIANT_DATATYPE_EXT = None

MGL_INVARIANT_EXT = None

MGL_INVARIANT_VALUE_EXT = None

MGL_INVERSE_NV = None

MGL_INVERSE_TRANSPOSE_NV = None

MGL_INVERT = None

MGL_ISOTROPIC_BRDF_NV = None

MGL_KEEP = None

MGL_LEFT = None

MGL_LEQUAL = None

MGL_LERP_ATI = None

MGL_LESS = None

MGL_LIGHT0 = None

MGL_LIGHT1 = None

MGL_LIGHT2 = None

MGL_LIGHT3 = None

MGL_LIGHT4 = None

MGL_LIGHT5 = None

MGL_LIGHT6 = None

MGL_LIGHT7 = None

MGL_LIGHTING = None

MGL_LIGHTING_BIT = None

MGL_LIGHT_MODEL_AMBIENT = None

MGL_LIGHT_MODEL_COLOR_CONTROL = None

MGL_LIGHT_MODEL_LOCAL_VIEWER = None

MGL_LIGHT_MODEL_TWO_SIDE = None

MGL_LINE = None

MGL_LINEAR = None

MGL_LINEAR_ATTENUATION = None

MGL_LINEAR_MIPMAP_LINEAR = None

MGL_LINEAR_MIPMAP_NEAREST = None

MGL_LINES = None

MGL_LINE_BIT = None

MGL_LINE_LOOP = None

MGL_LINE_RESET_TOKEN = None

MGL_LINE_SMOOTH = None

MGL_LINE_SMOOTH_HINT = None

MGL_LINE_STIPPLE = None

MGL_LINE_STIPPLE_PATTERN = None

MGL_LINE_STIPPLE_REPEAT = None

MGL_LINE_STRIP = None

MGL_LINE_TOKEN = None

MGL_LINE_WIDTH = None

MGL_LINE_WIDTH_GRANULARITY = None

MGL_LINE_WIDTH_RANGE = None

MGL_LIST_BASE = None

MGL_LIST_BIT = None

MGL_LIST_INDEX = None

MGL_LIST_MODE = None

MGL_LOAD = None

MGL_LOCAL_CONSTANT_DATATYPE_EXT = None

MGL_LOCAL_CONSTANT_EXT = None

MGL_LOCAL_CONSTANT_VALUE_EXT = None

MGL_LOCAL_EXT = None

MGL_LOGIC_OP = None

MGL_LOGIC_OP_MODE = None

MGL_LO_BIAS_NV = None

MGL_LO_SCALE_NV = None

MGL_LUMINANCE = None

MGL_LUMINANCE12 = None

MGL_LUMINANCE12_ALPHA12 = None

MGL_LUMINANCE12_ALPHA4 = None

MGL_LUMINANCE16 = None

MGL_LUMINANCE16F = None

MGL_LUMINANCE16_ALPHA16 = None

MGL_LUMINANCE32F = None

MGL_LUMINANCE4 = None

MGL_LUMINANCE4_ALPHA4 = None

MGL_LUMINANCE6_ALPHA2 = None

MGL_LUMINANCE8 = None

MGL_LUMINANCE8_ALPHA8 = None

MGL_LUMINANCE_ALPHA = None

MGL_LUMINANCE_ALPHA16F = None

MGL_LUMINANCE_ALPHA32F = None

MGL_MAD_ATI = None

MGL_MAGNITUDE_BIAS_NV = None

MGL_MAGNITUDE_SCALE_NV = None

MGL_MAP1_COLOR_4 = None

MGL_MAP1_GRID_DOMAIN = None

MGL_MAP1_GRID_SEGMENTS = None

MGL_MAP1_INDEX = None

MGL_MAP1_NORMAL = None

MGL_MAP1_TEXTURE_COORD_1 = None

MGL_MAP1_TEXTURE_COORD_2 = None

MGL_MAP1_TEXTURE_COORD_3 = None

MGL_MAP1_TEXTURE_COORD_4 = None

MGL_MAP1_VERTEX_3 = None

MGL_MAP1_VERTEX_4 = None

MGL_MAP1_VERTEX_ATTRIB0_4_NV = None

MGL_MAP1_VERTEX_ATTRIB10_4_NV = None

MGL_MAP1_VERTEX_ATTRIB11_4_NV = None

MGL_MAP1_VERTEX_ATTRIB12_4_NV = None

MGL_MAP1_VERTEX_ATTRIB13_4_NV = None

MGL_MAP1_VERTEX_ATTRIB14_4_NV = None

MGL_MAP1_VERTEX_ATTRIB15_4_NV = None

MGL_MAP1_VERTEX_ATTRIB1_4_NV = None

MGL_MAP1_VERTEX_ATTRIB2_4_NV = None

MGL_MAP1_VERTEX_ATTRIB3_4_NV = None

MGL_MAP1_VERTEX_ATTRIB4_4_NV = None

MGL_MAP1_VERTEX_ATTRIB5_4_NV = None

MGL_MAP1_VERTEX_ATTRIB6_4_NV = None

MGL_MAP1_VERTEX_ATTRIB7_4_NV = None

MGL_MAP1_VERTEX_ATTRIB8_4_NV = None

MGL_MAP1_VERTEX_ATTRIB9_4_NV = None

MGL_MAP2_COLOR_4 = None

MGL_MAP2_GRID_DOMAIN = None

MGL_MAP2_GRID_SEGMENTS = None

MGL_MAP2_INDEX = None

MGL_MAP2_NORMAL = None

MGL_MAP2_TEXTURE_COORD_1 = None

MGL_MAP2_TEXTURE_COORD_2 = None

MGL_MAP2_TEXTURE_COORD_3 = None

MGL_MAP2_TEXTURE_COORD_4 = None

MGL_MAP2_VERTEX_3 = None

MGL_MAP2_VERTEX_4 = None

MGL_MAP2_VERTEX_ATTRIB0_4_NV = None

MGL_MAP2_VERTEX_ATTRIB10_4_NV = None

MGL_MAP2_VERTEX_ATTRIB11_4_NV = None

MGL_MAP2_VERTEX_ATTRIB12_4_NV = None

MGL_MAP2_VERTEX_ATTRIB13_4_NV = None

MGL_MAP2_VERTEX_ATTRIB14_4_NV = None

MGL_MAP2_VERTEX_ATTRIB15_4_NV = None

MGL_MAP2_VERTEX_ATTRIB1_4_NV = None

MGL_MAP2_VERTEX_ATTRIB2_4_NV = None

MGL_MAP2_VERTEX_ATTRIB3_4_NV = None

MGL_MAP2_VERTEX_ATTRIB4_4_NV = None

MGL_MAP2_VERTEX_ATTRIB5_4_NV = None

MGL_MAP2_VERTEX_ATTRIB6_4_NV = None

MGL_MAP2_VERTEX_ATTRIB7_4_NV = None

MGL_MAP2_VERTEX_ATTRIB8_4_NV = None

MGL_MAP2_VERTEX_ATTRIB9_4_NV = None

MGL_MAP_COLOR = None

MGL_MAP_STENCIL = None

MGL_MATRIX0 = None

MGL_MATRIX0_NV = None

MGL_MATRIX1 = None

MGL_MATRIX10 = None

MGL_MATRIX11 = None

MGL_MATRIX12 = None

MGL_MATRIX13 = None

MGL_MATRIX14 = None

MGL_MATRIX15 = None

MGL_MATRIX16 = None

MGL_MATRIX17 = None

MGL_MATRIX18 = None

MGL_MATRIX19 = None

MGL_MATRIX1_NV = None

MGL_MATRIX2 = None

MGL_MATRIX20 = None

MGL_MATRIX21 = None

MGL_MATRIX22 = None

MGL_MATRIX23 = None

MGL_MATRIX24 = None

MGL_MATRIX25 = None

MGL_MATRIX26 = None

MGL_MATRIX27 = None

MGL_MATRIX28 = None

MGL_MATRIX29 = None

MGL_MATRIX2_NV = None

MGL_MATRIX3 = None

MGL_MATRIX30 = None

MGL_MATRIX31 = None

MGL_MATRIX3_NV = None

MGL_MATRIX4 = None

MGL_MATRIX4_NV = None

MGL_MATRIX5 = None

MGL_MATRIX5_NV = None

MGL_MATRIX6 = None

MGL_MATRIX6_NV = None

MGL_MATRIX7 = None

MGL_MATRIX7_NV = None

MGL_MATRIX8 = None

MGL_MATRIX9 = None

MGL_MATRIX_EXT = None

MGL_MATRIX_MODE = None

MGL_MAX = None

MGL_MAX_3D_TEXTURE_SIZE = None

MGL_MAX_3D_TEXTURE_SIZE_EXT = None

MGL_MAX_ATTRIB_STACK_DEPTH = None

MGL_MAX_CLIENT_ATTRIB_STACK_DEPTH = None

MGL_MAX_CLIP_PLANES = None

MGL_MAX_COMBINED_TEXTURE_IMAGE_UNITS_ARB = None

MGL_MAX_CUBE_MAP_TEXTURE_SIZE = None

MGL_MAX_CUBE_MAP_TEXTURE_SIZE_ARB = None

MGL_MAX_EVAL_ORDER = None

MGL_MAX_EXT = None

MGL_MAX_FRAGMENT_PROGRAM_LOCAL_PARAMETERS_NV = None

MGL_MAX_FRAGMENT_UNIFORM_COMPONENTS_ARB = None

MGL_MAX_GENERAL_COMBINERS_NV = None

MGL_MAX_LIGHTS = None

MGL_MAX_LIST_NESTING = None

MGL_MAX_MODELVIEW_STACK_DEPTH = None

MGL_MAX_NAME_STACK_DEPTH = None

MGL_MAX_OPTIMIZED_VERTEX_SHADER_INSTRUCTIONS_EXT = None

MGL_MAX_OPTIMIZED_VERTEX_SHADER_INVARIANTS_EXT = None

MGL_MAX_OPTIMIZED_VERTEX_SHADER_LOCALS_EXT = None

MGL_MAX_OPTIMIZED_VERTEX_SHADER_LOCAL_CONSTANTS_EXT = None

MGL_MAX_OPTIMIZED_VERTEX_SHADER_VARIANTS_EXT = None

MGL_MAX_PIXEL_MAP_TABLE = None

MGL_MAX_PN_TRIANGLES_TESSELATION_LEVEL_ATI = None

MGL_MAX_PROGRAM_ADDRESS_REGISTERS = None

MGL_MAX_PROGRAM_ALU_INSTRUCTIONS_ARB = None

MGL_MAX_PROGRAM_ATTRIBS = None

MGL_MAX_PROGRAM_ENV_PARAMETERS = None

MGL_MAX_PROGRAM_INSTRUCTIONS = None

MGL_MAX_PROGRAM_LOCAL_PARAMETERS = None

MGL_MAX_PROGRAM_MATRICES = None

MGL_MAX_PROGRAM_MATRIX_STACK_DEPTH = None

MGL_MAX_PROGRAM_NATIVE_ADDRESS_REGISTERS = None

MGL_MAX_PROGRAM_NATIVE_ALU_INSTRUCTIONS_ARB = None

MGL_MAX_PROGRAM_NATIVE_ATTRIBS = None

MGL_MAX_PROGRAM_NATIVE_INSTRUCTIONS = None

MGL_MAX_PROGRAM_NATIVE_PARAMETERS = None

MGL_MAX_PROGRAM_NATIVE_TEMPORARIES = None

MGL_MAX_PROGRAM_NATIVE_TEX_INDIRECTIONS_ARB = None

MGL_MAX_PROGRAM_NATIVE_TEX_INSTRUCTIONS_ARB = None

MGL_MAX_PROGRAM_PARAMETERS = None

MGL_MAX_PROGRAM_TEMPORARIES = None

MGL_MAX_PROGRAM_TEX_INDIRECTIONS_ARB = None

MGL_MAX_PROGRAM_TEX_INSTRUCTIONS_ARB = None

MGL_MAX_PROJECTION_STACK_DEPTH = None

MGL_MAX_RECTANGLE_TEXTURE_SIZE = None

MGL_MAX_TEXTURE_COORDS_ARB = None

MGL_MAX_TEXTURE_COORDS_NV = None

MGL_MAX_TEXTURE_IMAGE_UNITS_ARB = None

MGL_MAX_TEXTURE_IMAGE_UNITS_NV = None

MGL_MAX_TEXTURE_MAX_ANISOTROPY_EXT = None

MGL_MAX_TEXTURE_SIZE = None

MGL_MAX_TEXTURE_STACK_DEPTH = None

MGL_MAX_TEXTURE_UNITS = None

MGL_MAX_TEXTURE_UNITS_ARB = None

MGL_MAX_TRACK_MATRICES_NV = None

MGL_MAX_TRACK_MATRIX_STACK_DEPTH_NV = None

MGL_MAX_VARYING_FLOATS_ARB = None

MGL_MAX_VERTEX_ARRAY_RANGE_ELEMENT_NV = None

MGL_MAX_VERTEX_ATTRIBS = None

MGL_MAX_VERTEX_SHADER_INSTRUCTIONS_EXT = None

MGL_MAX_VERTEX_SHADER_INVARIANTS_EXT = None

MGL_MAX_VERTEX_SHADER_LOCALS_EXT = None

MGL_MAX_VERTEX_SHADER_LOCAL_CONSTANTS_EXT = None

MGL_MAX_VERTEX_SHADER_VARIANTS_EXT = None

MGL_MAX_VERTEX_TEXTURE_IMAGE_UNITS_ARB = None

MGL_MAX_VERTEX_UNIFORM_COMPONENTS_ARB = None

MGL_MAX_VIEWPORT_DIMS = None

MGL_MIN = None

MGL_MIN_EXT = None

MGL_MIRRORED_REPEAT_IBM = None

MGL_MIRROR_CLAMP_ATI = None

MGL_MIRROR_CLAMP_TO_EDGE_ATI = None

MGL_MODELVIEW = None

MGL_MODELVIEW0_EXT = None

MGL_MODELVIEW0_MATRIX_EXT = None

MGL_MODELVIEW0_STACK_DEPTH_EXT = None

MGL_MODELVIEW1_EXT = None

MGL_MODELVIEW1_STACK_DEPTH_EXT = None

MGL_MODELVIEW_MATRIX = None

MGL_MODELVIEW_MATRIX1_EXT = None

MGL_MODELVIEW_PROJECTION_NV = None

MGL_MODELVIEW_STACK_DEPTH = None

MGL_MODULATE = None

MGL_MOV_ATI = None

MGL_MULT = None

MGL_MULTISAMPLE = None

MGL_MULTISAMPLE_ARB = None

MGL_MULTISAMPLE_BIT = None

MGL_MULTISAMPLE_BIT_ARB = None

MGL_MUL_ATI = None

MGL_MVP_MATRIX_EXT = None

MGL_N3F_V3F = None

MGL_NAME_STACK_DEPTH = None

MGL_NAND = None

MGL_NEAREST = None

MGL_NEAREST_MIPMAP_LINEAR = None

MGL_NEAREST_MIPMAP_NEAREST = None

MGL_NEGATE_BIT_ATI = None

MGL_NEGATIVE_ONE_EXT = None

MGL_NEGATIVE_W_EXT = None

MGL_NEGATIVE_X_EXT = None

MGL_NEGATIVE_Y_EXT = None

MGL_NEGATIVE_Z_EXT = None

MGL_NEVER = None

MGL_NICEST = None

MGL_NONE = None

MGL_NOOP = None

MGL_NOR = None

MGL_NORMALIZE = None

MGL_NORMALIZED_RANGE_EXT = None

MGL_NORMAL_ARRAY = None

MGL_NORMAL_ARRAY_BUFFER_BINDING_ARB = None

MGL_NORMAL_ARRAY_COUNT_EXT = None

MGL_NORMAL_ARRAY_EXT = None

MGL_NORMAL_ARRAY_POINTER = None

MGL_NORMAL_ARRAY_POINTER_EXT = None

MGL_NORMAL_ARRAY_STRIDE = None

MGL_NORMAL_ARRAY_STRIDE_EXT = None

MGL_NORMAL_ARRAY_TYPE = None

MGL_NORMAL_ARRAY_TYPE_EXT = None

MGL_NORMAL_MAP = None

MGL_NORMAL_MAP_ARB = None

MGL_NOTEQUAL = None

MGL_NO_ERROR = None

MGL_NUM_COMPRESSED_TEXTURE_FORMATS = None

MGL_NUM_COMPRESSED_TEXTURE_FORMATS_ARB = None

MGL_NUM_FRAGMENT_CONSTANTS_ATI = None

MGL_NUM_FRAGMENT_REGISTERS_ATI = None

MGL_NUM_GENERAL_COMBINERS_NV = None

MGL_NUM_INPUT_INTERPOLATOR_COMPONENTS_ATI = None

MGL_NUM_INSTRUCTIONS_PER_PASS_ATI = None

MGL_NUM_INSTRUCTIONS_TOTAL_ATI = None

MGL_NUM_LOOPBACK_COMPONENTS_ATI = None

MGL_NUM_PASSES_ATI = None

MGL_OBJECT_ACTIVE_ATTRIBUTES_ARB = None

MGL_OBJECT_ACTIVE_ATTRIBUTE_MAX_LENGTH_ARB = None

MGL_OBJECT_ACTIVE_UNIFORMS_ARB = None

MGL_OBJECT_ACTIVE_UNIFORM_MAX_LENGTH_ARB = None

MGL_OBJECT_ATTACHED_OBJECTS_ARB = None

MGL_OBJECT_COMPILE_STATUS_ARB = None

MGL_OBJECT_DELETE_STATUS_ARB = None

MGL_OBJECT_INFO_LOG_LENGTH_ARB = None

MGL_OBJECT_LINEAR = None

MGL_OBJECT_LINK_STATUS_ARB = None

MGL_OBJECT_PLANE = None

MGL_OBJECT_SHADER_SOURCE_LENGTH_ARB = None

MGL_OBJECT_SUBTYPE_ARB = None

MGL_OBJECT_TYPE_ARB = None

MGL_OBJECT_VALIDATE_STATUS_ARB = None

MGL_OCCLUSION_TEST_HP = None

MGL_OCCLUSION_TEST_RESULT_HP = None

MGL_OFFSET_TEXTURE_2D_BIAS_NV = None

MGL_OFFSET_TEXTURE_2D_MATRIX_NV = None

MGL_OFFSET_TEXTURE_2D_NV = None

MGL_OFFSET_TEXTURE_2D_SCALE_NV = None

MGL_ONE = None

MGL_ONE_EXT = None

MGL_ONE_MINUS_CONSTANT_ALPHA = None

MGL_ONE_MINUS_CONSTANT_ALPHA_EXT = None

MGL_ONE_MINUS_CONSTANT_COLOR = None

MGL_ONE_MINUS_CONSTANT_COLOR_EXT = None

MGL_ONE_MINUS_DST_ALPHA = None

MGL_ONE_MINUS_DST_COLOR = None

MGL_ONE_MINUS_SRC_ALPHA = None

MGL_ONE_MINUS_SRC_COLOR = None

MGL_OPERAND0_ALPHA = None

MGL_OPERAND0_ALPHA_EXT = None

MGL_OPERAND0_RGB = None

MGL_OPERAND0_RGB_EXT = None

MGL_OPERAND1_ALPHA = None

MGL_OPERAND1_ALPHA_EXT = None

MGL_OPERAND1_RGB = None

MGL_OPERAND1_RGB_EXT = None

MGL_OPERAND2_ALPHA = None

MGL_OPERAND2_ALPHA_EXT = None

MGL_OPERAND2_RGB = None

MGL_OPERAND2_RGB_EXT = None

MGL_OPERAND3_ALPHA_NV = None

MGL_OPERAND3_RGB_NV = None

MGL_OP_ADD_EXT = None

MGL_OP_CLAMP_EXT = None

MGL_OP_CROSS_PRODUCT_EXT = None

MGL_OP_DOT3_EXT = None

MGL_OP_DOT4_EXT = None

MGL_OP_EXP_BASE_2_EXT = None

MGL_OP_FLOOR_EXT = None

MGL_OP_FRAC_EXT = None

MGL_OP_INDEX_EXT = None

MGL_OP_LOG_BASE_2_EXT = None

MGL_OP_MADD_EXT = None

MGL_OP_MAX_EXT = None

MGL_OP_MIN_EXT = None

MGL_OP_MOV_EXT = None

MGL_OP_MULTIPLY_MATRIX_EXT = None

MGL_OP_MUL_EXT = None

MGL_OP_NEGATE_EXT = None

MGL_OP_POWER_EXT = None

MGL_OP_RECIP_EXT = None

MGL_OP_RECIP_SQRT_EXT = None

MGL_OP_ROUND_EXT = None

MGL_OP_SET_GE_EXT = None

MGL_OP_SET_LT_EXT = None

MGL_OP_SUB_EXT = None

MGL_OR = None

MGL_ORDER = None

MGL_OR_INVERTED = None

MGL_OR_REVERSE = None

MGL_OUTPUT_COLOR0_EXT = None

MGL_OUTPUT_COLOR1_EXT = None

MGL_OUTPUT_FOG_EXT = None

MGL_OUTPUT_TEXTURE_COORD0_EXT = None

MGL_OUTPUT_TEXTURE_COORD10_EXT = None

MGL_OUTPUT_TEXTURE_COORD11_EXT = None

MGL_OUTPUT_TEXTURE_COORD12_EXT = None

MGL_OUTPUT_TEXTURE_COORD13_EXT = None

MGL_OUTPUT_TEXTURE_COORD14_EXT = None

MGL_OUTPUT_TEXTURE_COORD15_EXT = None

MGL_OUTPUT_TEXTURE_COORD16_EXT = None

MGL_OUTPUT_TEXTURE_COORD17_EXT = None

MGL_OUTPUT_TEXTURE_COORD18_EXT = None

MGL_OUTPUT_TEXTURE_COORD19_EXT = None

MGL_OUTPUT_TEXTURE_COORD1_EXT = None

MGL_OUTPUT_TEXTURE_COORD20_EXT = None

MGL_OUTPUT_TEXTURE_COORD21_EXT = None

MGL_OUTPUT_TEXTURE_COORD22_EXT = None

MGL_OUTPUT_TEXTURE_COORD23_EXT = None

MGL_OUTPUT_TEXTURE_COORD24_EXT = None

MGL_OUTPUT_TEXTURE_COORD25_EXT = None

MGL_OUTPUT_TEXTURE_COORD26_EXT = None

MGL_OUTPUT_TEXTURE_COORD27_EXT = None

MGL_OUTPUT_TEXTURE_COORD28_EXT = None

MGL_OUTPUT_TEXTURE_COORD29_EXT = None

MGL_OUTPUT_TEXTURE_COORD2_EXT = None

MGL_OUTPUT_TEXTURE_COORD30_EXT = None

MGL_OUTPUT_TEXTURE_COORD31_EXT = None

MGL_OUTPUT_TEXTURE_COORD3_EXT = None

MGL_OUTPUT_TEXTURE_COORD4_EXT = None

MGL_OUTPUT_TEXTURE_COORD5_EXT = None

MGL_OUTPUT_TEXTURE_COORD6_EXT = None

MGL_OUTPUT_TEXTURE_COORD7_EXT = None

MGL_OUTPUT_TEXTURE_COORD8_EXT = None

MGL_OUTPUT_TEXTURE_COORD9_EXT = None

MGL_OUTPUT_VERTEX_EXT = None

MGL_OUT_OF_MEMORY = None

MGL_PACK_ALIGNMENT = None

MGL_PACK_IMAGE_HEIGHT = None

MGL_PACK_IMAGE_HEIGHT_EXT = None

MGL_PACK_LSB_FIRST = None

MGL_PACK_ROW_LENGTH = None

MGL_PACK_SKIP_IMAGES = None

MGL_PACK_SKIP_IMAGES_EXT = None

MGL_PACK_SKIP_PIXELS = None

MGL_PACK_SKIP_ROWS = None

MGL_PACK_SWAP_BYTES = None

MGL_PASS_THROUGH_NV = None

MGL_PASS_THROUGH_TOKEN = None

MGL_PERSPECTIVE_CORRECTION_HINT = None

MGL_PHONG_HINT_WIN = None

MGL_PHONG_WIN = None

MGL_PIXEL_COUNTER_BITS_ARB = None

MGL_PIXEL_COUNTER_BITS_NV = None

MGL_PIXEL_COUNT_AVAILABLE_NV = None

MGL_PIXEL_COUNT_NV = None

MGL_PIXEL_MAP_A_TO_A = None

MGL_PIXEL_MAP_A_TO_A_SIZE = None

MGL_PIXEL_MAP_B_TO_B = None

MGL_PIXEL_MAP_B_TO_B_SIZE = None

MGL_PIXEL_MAP_G_TO_G = None

MGL_PIXEL_MAP_G_TO_G_SIZE = None

MGL_PIXEL_MAP_I_TO_A = None

MGL_PIXEL_MAP_I_TO_A_SIZE = None

MGL_PIXEL_MAP_I_TO_B = None

MGL_PIXEL_MAP_I_TO_B_SIZE = None

MGL_PIXEL_MAP_I_TO_G = None

MGL_PIXEL_MAP_I_TO_G_SIZE = None

MGL_PIXEL_MAP_I_TO_I = None

MGL_PIXEL_MAP_I_TO_I_SIZE = None

MGL_PIXEL_MAP_I_TO_R = None

MGL_PIXEL_MAP_I_TO_R_SIZE = None

MGL_PIXEL_MAP_R_TO_R = None

MGL_PIXEL_MAP_R_TO_R_SIZE = None

MGL_PIXEL_MAP_S_TO_S = None

MGL_PIXEL_MAP_S_TO_S_SIZE = None

MGL_PIXEL_MODE_BIT = None

MGL_PN_TRIANGLES_ATI = None

MGL_PN_TRIANGLES_NORMAL_MODE_ATI = None

MGL_PN_TRIANGLES_NORMAL_MODE_LINEAR_ATI = None

MGL_PN_TRIANGLES_NORMAL_MODE_QUADRATIC_ATI = None

MGL_PN_TRIANGLES_POINT_MODE_ATI = None

MGL_PN_TRIANGLES_POINT_MODE_CUBIC_ATI = None

MGL_PN_TRIANGLES_POINT_MODE_LINEAR_ATI = None

MGL_PN_TRIANGLES_TESSELATION_LEVEL_ATI = None

MGL_POINT = None

MGL_POINTS = None

MGL_POINT_BIT = None

MGL_POINT_DISTANCE_ATTENUATION_ARB = None

MGL_POINT_FADE_THRESHOLD_SIZE_ARB = None

MGL_POINT_SIZE = None

MGL_POINT_SIZE_GRANULARITY = None

MGL_POINT_SIZE_MAX_ARB = None

MGL_POINT_SIZE_MIN_ARB = None

MGL_POINT_SIZE_RANGE = None

MGL_POINT_SMOOTH = None

MGL_POINT_SMOOTH_HINT = None

MGL_POINT_TOKEN = None

MGL_POLYGON = None

MGL_POLYGON_BIT = None

MGL_POLYGON_MODE = None

MGL_POLYGON_OFFSET_FACTOR = None

MGL_POLYGON_OFFSET_FILL = None

MGL_POLYGON_OFFSET_LINE = None

MGL_POLYGON_OFFSET_POINT = None

MGL_POLYGON_OFFSET_UNITS = None

MGL_POLYGON_SMOOTH = None

MGL_POLYGON_SMOOTH_HINT = None

MGL_POLYGON_STIPPLE = None

MGL_POLYGON_STIPPLE_BIT = None

MGL_POLYGON_TOKEN = None

MGL_POSITION = None

MGL_PREVIOUS = None

MGL_PREVIOUS_EXT = None

MGL_PREVIOUS_TEXTURE_INPUT_NV = None

MGL_PRIMARY_COLOR = None

MGL_PRIMARY_COLOR_EXT = None

MGL_PRIMARY_COLOR_NV = None

MGL_PRIMITIVE_RESTART_INDEX_NV = None

MGL_PRIMITIVE_RESTART_NV = None

MGL_PROGRAM_ADDRESS_REGISTERS = None

MGL_PROGRAM_ALU_INSTRUCTIONS_ARB = None

MGL_PROGRAM_ATTRIBS = None

MGL_PROGRAM_BINDING = None

MGL_PROGRAM_ERROR_POSITION = None

MGL_PROGRAM_ERROR_POSITION_NV = None

MGL_PROGRAM_ERROR_STRING = None

MGL_PROGRAM_ERROR_STRING_NV = None

MGL_PROGRAM_FORMAT = None

MGL_PROGRAM_FORMAT_ASCII = None

MGL_PROGRAM_INSTRUCTIONS = None

MGL_PROGRAM_LENGTH = None

MGL_PROGRAM_LENGTH_NV = None

MGL_PROGRAM_NATIVE_ADDRESS_REGISTERS = None

MGL_PROGRAM_NATIVE_ALU_INSTRUCTIONS_ARB = None

MGL_PROGRAM_NATIVE_ATTRIBS = None

MGL_PROGRAM_NATIVE_INSTRUCTIONS = None

MGL_PROGRAM_NATIVE_PARAMETERS = None

MGL_PROGRAM_NATIVE_TEMPORARIES = None

MGL_PROGRAM_NATIVE_TEX_INDIRECTIONS_ARB = None

MGL_PROGRAM_NATIVE_TEX_INSTRUCTIONS_ARB = None

MGL_PROGRAM_OBJECT_ARB = None

MGL_PROGRAM_PARAMETERS = None

MGL_PROGRAM_PARAMETER_NV = None

MGL_PROGRAM_RESIDENT_NV = None

MGL_PROGRAM_STRING = None

MGL_PROGRAM_STRING_NV = None

MGL_PROGRAM_TARGET_NV = None

MGL_PROGRAM_TEMPORARIES = None

MGL_PROGRAM_TEX_INDIRECTIONS_ARB = None

MGL_PROGRAM_TEX_INSTRUCTIONS_ARB = None

MGL_PROGRAM_UNDER_NATIVE_LIMITS = None

MGL_PROJECTION = None

MGL_PROJECTION_MATRIX = None

MGL_PROJECTION_STACK_DEPTH = None

MGL_PROXY_TEXTURE_1D = None

MGL_PROXY_TEXTURE_2D = None

MGL_PROXY_TEXTURE_3D = None

MGL_PROXY_TEXTURE_3D_EXT = None

MGL_PROXY_TEXTURE_CUBE_MAP = None

MGL_PROXY_TEXTURE_CUBE_MAP_ARB = None

MGL_PROXY_TEXTURE_RECTANGLE = None

MGL_Q = None

MGL_QUADRATIC_ATTENUATION = None

MGL_QUADS = None

MGL_QUAD_STRIP = None

MGL_QUARTER_BIT_ATI = None

MGL_QUERY_RESULT_ARB = None

MGL_QUERY_RESULT_AVAILABLE_ARB = None

MGL_R = None

MGL_R3_G3_B2 = None

MGL_READ_BUFFER = None

MGL_READ_ONLY_ARB = None

MGL_READ_WRITE_ARB = None

MGL_RED = None

MGL_RED_BIAS = None

MGL_RED_BITS = None

MGL_RED_BIT_ATI = None

MGL_RED_SCALE = None

MGL_REFLECTION_MAP = None

MGL_REFLECTION_MAP_ARB = None

MGL_REGISTER_COMBINERS_NV = None

MGL_REG_0_ATI = None

MGL_REG_10_ATI = None

MGL_REG_11_ATI = None

MGL_REG_12_ATI = None

MGL_REG_13_ATI = None

MGL_REG_14_ATI = None

MGL_REG_15_ATI = None

MGL_REG_16_ATI = None

MGL_REG_17_ATI = None

MGL_REG_18_ATI = None

MGL_REG_19_ATI = None

MGL_REG_1_ATI = None

MGL_REG_20_ATI = None

MGL_REG_21_ATI = None

MGL_REG_22_ATI = None

MGL_REG_23_ATI = None

MGL_REG_24_ATI = None

MGL_REG_25_ATI = None

MGL_REG_26_ATI = None

MGL_REG_27_ATI = None

MGL_REG_28_ATI = None

MGL_REG_29_ATI = None

MGL_REG_2_ATI = None

MGL_REG_30_ATI = None

MGL_REG_31_ATI = None

MGL_REG_3_ATI = None

MGL_REG_4_ATI = None

MGL_REG_5_ATI = None

MGL_REG_6_ATI = None

MGL_REG_7_ATI = None

MGL_REG_8_ATI = None

MGL_REG_9_ATI = None

MGL_RENDER = None

MGL_RENDERER = None

MGL_RENDER_MODE = None

MGL_REPEAT = None

MGL_REPLACE = None

MGL_RESCALE_NORMAL_EXT = None

MGL_RETURN = None

MGL_RGB = None

MGL_RGB10 = None

MGL_RGB10_A2 = None

MGL_RGB12 = None

MGL_RGB16 = None

MGL_RGB16F = None

MGL_RGB32F = None

MGL_RGB4 = None

MGL_RGB5 = None

MGL_RGB5_A1 = None

MGL_RGB8 = None

MGL_RGBA = None

MGL_RGBA12 = None

MGL_RGBA16 = None

MGL_RGBA16F = None

MGL_RGBA2 = None

MGL_RGBA32F = None

MGL_RGBA4 = None

MGL_RGBA8 = None

MGL_RGBA_FLOAT_MODE = None

MGL_RGBA_FLOAT_MODE_ATI = None

MGL_RGBA_MODE = None

MGL_RGBA_UNSIGNED_DOT_PRODUCT_MAPPING_NV = None

MGL_RGB_SCALE = None

MGL_RGB_SCALE_EXT = None

MGL_RIGHT = None

MGL_S = None

MGL_SAMPLES = None

MGL_SAMPLES_ARB = None

MGL_SAMPLES_PASSED_ARB = None

MGL_SAMPLE_ALPHA_TO_COVERAGE = None

MGL_SAMPLE_ALPHA_TO_COVERAGE_ARB = None

MGL_SAMPLE_ALPHA_TO_ONE = None

MGL_SAMPLE_ALPHA_TO_ONE_ARB = None

MGL_SAMPLE_BUFFERS = None

MGL_SAMPLE_BUFFERS_ARB = None

MGL_SAMPLE_COVERAGE = None

MGL_SAMPLE_COVERAGE_ARB = None

MGL_SAMPLE_COVERAGE_INVERT = None

MGL_SAMPLE_COVERAGE_INVERT_ARB = None

MGL_SAMPLE_COVERAGE_VALUE = None

MGL_SAMPLE_COVERAGE_VALUE_ARB = None

MGL_SATURATE_BIT_ATI = None

MGL_SCALAR_EXT = None

MGL_SCALE_BY_FOUR_NV = None

MGL_SCALE_BY_ONE_HALF_NV = None

MGL_SCALE_BY_TWO_NV = None

MGL_SCISSOR_BIT = None

MGL_SCISSOR_BOX = None

MGL_SCISSOR_TEST = None

MGL_SECONDARY_COLOR_ARRAY_BUFFER_BINDING_ARB = None

MGL_SECONDARY_COLOR_ARRAY_EXT = None

MGL_SECONDARY_COLOR_ARRAY_POINTER_EXT = None

MGL_SECONDARY_COLOR_ARRAY_SIZE_EXT = None

MGL_SECONDARY_COLOR_ARRAY_STRIDE_EXT = None

MGL_SECONDARY_COLOR_ARRAY_TYPE_EXT = None

MGL_SECONDARY_COLOR_NV = None

MGL_SECONDARY_INTERPOLATOR_ATI = None

MGL_SELECT = None

MGL_SELECTION_BUFFER_POINTER = None

MGL_SELECTION_BUFFER_SIZE = None

MGL_SEPARATE_SPECULAR_COLOR = None

MGL_SET = None

MGL_SHADER_CONSISTENT_NV = None

MGL_SHADER_OBJECT_ARB = None

MGL_SHADER_OPERATION_NV = None

MGL_SHADE_MODEL = None

MGL_SHININESS = None

MGL_SHORT = None

MGL_SIGNED_ALPHA8_NV = None

MGL_SIGNED_ALPHA_NV = None

MGL_SIGNED_HILO16_NV = None

MGL_SIGNED_HILO_NV = None

MGL_SIGNED_IDENTITY_NV = None

MGL_SIGNED_INTENSITY8_NV = None

MGL_SIGNED_INTENSITY_NV = None

MGL_SIGNED_LUMINANCE8_ALPHA8_NV = None

MGL_SIGNED_LUMINANCE8_NV = None

MGL_SIGNED_LUMINANCE_ALPHA_NV = None

MGL_SIGNED_LUMINANCE_NV = None

MGL_SIGNED_NEGATE_NV = None

MGL_SIGNED_RGB8_NV = None

MGL_SIGNED_RGB8_UNSIGNED_ALPHA8_NV = None

MGL_SIGNED_RGBA8_NV = None

MGL_SIGNED_RGBA_NV = None

MGL_SIGNED_RGB_NV = None

MGL_SIGNED_RGB_UNSIGNED_ALPHA_NV = None

MGL_SINGLE_COLOR = None

MGL_SMOOTH = None

MGL_SOURCE0_ALPHA = None

MGL_SOURCE0_ALPHA_EXT = None

MGL_SOURCE0_RGB = None

MGL_SOURCE0_RGB_EXT = None

MGL_SOURCE1_ALPHA = None

MGL_SOURCE1_ALPHA_EXT = None

MGL_SOURCE1_RGB = None

MGL_SOURCE1_RGB_EXT = None

MGL_SOURCE2_ALPHA = None

MGL_SOURCE2_ALPHA_EXT = None

MGL_SOURCE2_RGB = None

MGL_SOURCE2_RGB_EXT = None

MGL_SOURCE3_ALPHA_NV = None

MGL_SOURCE3_RGB_NV = None

MGL_SPARE0_NV = None

MGL_SPARE0_PLUS_SECONDARY_COLOR_NV = None

MGL_SPARE1_NV = None

MGL_SPECULAR = None

MGL_SPHERE_MAP = None

MGL_SPOT_CUTOFF = None

MGL_SPOT_DIRECTION = None

MGL_SPOT_EXPONENT = None

MGL_SRC_ALPHA = None

MGL_SRC_ALPHA_SATURATE = None

MGL_SRC_COLOR = None

MGL_STACK_OVERFLOW = None

MGL_STACK_UNDERFLOW = None

MGL_STATIC_COPY_ARB = None

MGL_STATIC_DRAW_ARB = None

MGL_STATIC_READ_ARB = None

MGL_STENCIL = None

MGL_STENCIL_BITS = None

MGL_STENCIL_BUFFER_BIT = None

MGL_STENCIL_CLEAR_VALUE = None

MGL_STENCIL_FAIL = None

MGL_STENCIL_FUNC = None

MGL_STENCIL_INDEX = None

MGL_STENCIL_PASS_DEPTH_FAIL = None

MGL_STENCIL_PASS_DEPTH_PASS = None

MGL_STENCIL_REF = None

MGL_STENCIL_TEST = None

MGL_STENCIL_VALUE_MASK = None

MGL_STENCIL_WRITEMASK = None

MGL_STEREO = None

MGL_STREAM_COPY_ARB = None

MGL_STREAM_DRAW_ARB = None

MGL_STREAM_READ_ARB = None

MGL_SUBPIXEL_BITS = None

MGL_SUBTRACT = None

MGL_SUB_ATI = None

MGL_SWIZZLE_STQ_ATI = None

MGL_SWIZZLE_STQ_DQ_ATI = None

MGL_SWIZZLE_STRQ_ATI = None

MGL_SWIZZLE_STRQ_DQ_ATI = None

MGL_SWIZZLE_STR_ATI = None

MGL_SWIZZLE_STR_DR_ATI = None

MGL_T = None

MGL_T2F_C3F_V3F = None

MGL_T2F_C4F_N3F_V3F = None

MGL_T2F_C4UB_V3F = None

MGL_T2F_N3F_V3F = None

MGL_T2F_V3F = None

MGL_T4F_C4F_N3F_V4F = None

MGL_T4F_V4F = None

MGL_TEXTURE = None

MGL_TEXTURE0 = None

MGL_TEXTURE0_ARB = None

MGL_TEXTURE1 = None

MGL_TEXTURE10 = None

MGL_TEXTURE10_ARB = None

MGL_TEXTURE11 = None

MGL_TEXTURE11_ARB = None

MGL_TEXTURE12 = None

MGL_TEXTURE12_ARB = None

MGL_TEXTURE13 = None

MGL_TEXTURE13_ARB = None

MGL_TEXTURE14 = None

MGL_TEXTURE14_ARB = None

MGL_TEXTURE15 = None

MGL_TEXTURE15_ARB = None

MGL_TEXTURE16 = None

MGL_TEXTURE16_ARB = None

MGL_TEXTURE17 = None

MGL_TEXTURE17_ARB = None

MGL_TEXTURE18 = None

MGL_TEXTURE18_ARB = None

MGL_TEXTURE19 = None

MGL_TEXTURE19_ARB = None

MGL_TEXTURE1_ARB = None

MGL_TEXTURE2 = None

MGL_TEXTURE20 = None

MGL_TEXTURE20_ARB = None

MGL_TEXTURE21 = None

MGL_TEXTURE21_ARB = None

MGL_TEXTURE22 = None

MGL_TEXTURE22_ARB = None

MGL_TEXTURE23 = None

MGL_TEXTURE23_ARB = None

MGL_TEXTURE24 = None

MGL_TEXTURE24_ARB = None

MGL_TEXTURE25 = None

MGL_TEXTURE25_ARB = None

MGL_TEXTURE26 = None

MGL_TEXTURE26_ARB = None

MGL_TEXTURE27 = None

MGL_TEXTURE27_ARB = None

MGL_TEXTURE28 = None

MGL_TEXTURE28_ARB = None

MGL_TEXTURE29 = None

MGL_TEXTURE29_ARB = None

MGL_TEXTURE2_ARB = None

MGL_TEXTURE3 = None

MGL_TEXTURE30 = None

MGL_TEXTURE30_ARB = None

MGL_TEXTURE31 = None

MGL_TEXTURE31_ARB = None

MGL_TEXTURE3_ARB = None

MGL_TEXTURE4 = None

MGL_TEXTURE4_ARB = None

MGL_TEXTURE5 = None

MGL_TEXTURE5_ARB = None

MGL_TEXTURE6 = None

MGL_TEXTURE6_ARB = None

MGL_TEXTURE7 = None

MGL_TEXTURE7_ARB = None

MGL_TEXTURE8 = None

MGL_TEXTURE8_ARB = None

MGL_TEXTURE9 = None

MGL_TEXTURE9_ARB = None

MGL_TEXTURE_1D = None

MGL_TEXTURE_1D_BINDING = None

MGL_TEXTURE_2D = None

MGL_TEXTURE_2D_BINDING = None

MGL_TEXTURE_3D = None

MGL_TEXTURE_3D_EXT = None

MGL_TEXTURE_ALPHA_SIZE = None

MGL_TEXTURE_ALPHA_TYPE = None

MGL_TEXTURE_BINDING_1D = None

MGL_TEXTURE_BINDING_2D = None

MGL_TEXTURE_BINDING_CUBE_MAP = None

MGL_TEXTURE_BINDING_CUBE_MAP_ARB = None

MGL_TEXTURE_BINDING_RECTANGLE = None

MGL_TEXTURE_BIT = None

MGL_TEXTURE_BLUE_SIZE = None

MGL_TEXTURE_BLUE_TYPE = None

MGL_TEXTURE_BORDER = None

MGL_TEXTURE_BORDER_COLOR = None

MGL_TEXTURE_BORDER_VALUES_NV = None

MGL_TEXTURE_COMPARE_FAIL_VALUE_ARB = None

MGL_TEXTURE_COMPARE_FUNC_ARB = None

MGL_TEXTURE_COMPARE_MODE_ARB = None

MGL_TEXTURE_COMPARE_OPERATOR_SGIX = None

MGL_TEXTURE_COMPARE_SGIX = None

MGL_TEXTURE_COMPONENTS = None

MGL_TEXTURE_COMPRESSED = None

MGL_TEXTURE_COMPRESSED_ARB = None

MGL_TEXTURE_COMPRESSED_IMAGE_SIZE = None

MGL_TEXTURE_COMPRESSION_HINT = None

MGL_TEXTURE_COMPRESSION_HINT_ARB = None

MGL_TEXTURE_COORD_ARRAY = None

MGL_TEXTURE_COORD_ARRAY_COUNT_EXT = None

MGL_TEXTURE_COORD_ARRAY_EXT = None

MGL_TEXTURE_COORD_ARRAY_POINTER = None

MGL_TEXTURE_COORD_ARRAY_POINTER_EXT = None

MGL_TEXTURE_COORD_ARRAY_SIZE = None

MGL_TEXTURE_COORD_ARRAY_SIZE_EXT = None

MGL_TEXTURE_COORD_ARRAY_STRIDE = None

MGL_TEXTURE_COORD_ARRAY_STRIDE_EXT = None

MGL_TEXTURE_COORD_ARRAY_TYPE = None

MGL_TEXTURE_COORD_ARRAY_TYPE_EXT = None

MGL_TEXTURE_CUBE_MAP = None

MGL_TEXTURE_CUBE_MAP_ARB = None

MGL_TEXTURE_CUBE_MAP_NEGATIVE_X = None

MGL_TEXTURE_CUBE_MAP_NEGATIVE_X_ARB = None

MGL_TEXTURE_CUBE_MAP_NEGATIVE_Y = None

MGL_TEXTURE_CUBE_MAP_NEGATIVE_Y_ARB = None

MGL_TEXTURE_CUBE_MAP_NEGATIVE_Z = None

MGL_TEXTURE_CUBE_MAP_NEGATIVE_Z_ARB = None

MGL_TEXTURE_CUBE_MAP_POSITIVE_X = None

MGL_TEXTURE_CUBE_MAP_POSITIVE_X_ARB = None

MGL_TEXTURE_CUBE_MAP_POSITIVE_Y = None

MGL_TEXTURE_CUBE_MAP_POSITIVE_Y_ARB = None

MGL_TEXTURE_CUBE_MAP_POSITIVE_Z = None

MGL_TEXTURE_CUBE_MAP_POSITIVE_Z_ARB = None

MGL_TEXTURE_DEPTH = None

MGL_TEXTURE_DEPTH_EXT = None

MGL_TEXTURE_DEPTH_SIZE_ARB = None

MGL_TEXTURE_DEPTH_TYPE = None

MGL_TEXTURE_DS_SIZE_NV = None

MGL_TEXTURE_DT_SIZE_NV = None

MGL_TEXTURE_ENV = None

MGL_TEXTURE_ENV_COLOR = None

MGL_TEXTURE_ENV_MODE = None

MGL_TEXTURE_FLOAT_COMPONENTS_NV = None

MGL_TEXTURE_GEN_MODE = None

MGL_TEXTURE_GEN_Q = None

MGL_TEXTURE_GEN_R = None

MGL_TEXTURE_GEN_S = None

MGL_TEXTURE_GEN_T = None

MGL_TEXTURE_GEQUAL_R_SGIX = None

MGL_TEXTURE_GREEN_SIZE = None

MGL_TEXTURE_GREEN_TYPE = None

MGL_TEXTURE_HEIGHT = None

MGL_TEXTURE_HI_SIZE_NV = None

MGL_TEXTURE_IMAGE_SIZE_ARB = None

MGL_TEXTURE_INTENSITY_SIZE = None

MGL_TEXTURE_INTENSITY_TYPE = None

MGL_TEXTURE_INTERNAL_FORMAT = None

MGL_TEXTURE_LEQUAL_R_SGIX = None

MGL_TEXTURE_LO_SIZE_NV = None

MGL_TEXTURE_LUMINANCE_SIZE = None

MGL_TEXTURE_LUMINANCE_TYPE = None

MGL_TEXTURE_MAG_FILTER = None

MGL_TEXTURE_MAG_SIZE_NV = None

MGL_TEXTURE_MATRIX = None

MGL_TEXTURE_MAX_ANISOTROPY_EXT = None

MGL_TEXTURE_MIN_FILTER = None

MGL_TEXTURE_PRIORITY = None

MGL_TEXTURE_RECTANGLE = None

MGL_TEXTURE_RED_SIZE = None

MGL_TEXTURE_RED_TYPE = None

MGL_TEXTURE_RESIDENT = None

MGL_TEXTURE_SHADER_NV = None

MGL_TEXTURE_STACK_DEPTH = None

MGL_TEXTURE_WIDTH = None

MGL_TEXTURE_WRAP_R = None

MGL_TEXTURE_WRAP_R_EXT = None

MGL_TEXTURE_WRAP_S = None

MGL_TEXTURE_WRAP_T = None

MGL_TRACK_MATRIX_NV = None

MGL_TRACK_MATRIX_TRANSFORM_NV = None

MGL_TRANSFORM_BIT = None

MGL_TRANSPOSE_COLOR_MATRIX = None

MGL_TRANSPOSE_COLOR_MATRIX_ARB = None

MGL_TRANSPOSE_CURRENT_MATRIX = None

MGL_TRANSPOSE_MODELVIEW_MATRIX = None

MGL_TRANSPOSE_MODELVIEW_MATRIX_ARB = None

MGL_TRANSPOSE_NV = None

MGL_TRANSPOSE_PROJECTION_MATRIX = None

MGL_TRANSPOSE_PROJECTION_MATRIX_ARB = None

MGL_TRANSPOSE_TEXTURE_MATRIX = None

MGL_TRANSPOSE_TEXTURE_MATRIX_ARB = None

MGL_TRIANGLES = None

MGL_TRIANGLE_FAN = None

MGL_TRIANGLE_STRIP = None

MGL_TRUE = None

MGL_UNPACK_ALIGNMENT = None

MGL_UNPACK_IMAGE_HEIGHT = None

MGL_UNPACK_IMAGE_HEIGHT_EXT = None

MGL_UNPACK_LSB_FIRST = None

MGL_UNPACK_ROW_LENGTH = None

MGL_UNPACK_SKIP_IMAGES = None

MGL_UNPACK_SKIP_IMAGES_EXT = None

MGL_UNPACK_SKIP_PIXELS = None

MGL_UNPACK_SKIP_ROWS = None

MGL_UNPACK_SWAP_BYTES = None

MGL_UNSIGNED_BYTE = None

MGL_UNSIGNED_BYTE_3_3_2_EXT = None

MGL_UNSIGNED_IDENTITY_NV = None

MGL_UNSIGNED_INT = None

MGL_UNSIGNED_INT_10_10_10_2_EXT = None

MGL_UNSIGNED_INT_8_8_8_8_EXT = None

MGL_UNSIGNED_INT_S8_S8_8_8_NV = None

MGL_UNSIGNED_INT_S8_S8_8_8_REV_NV = None

MGL_UNSIGNED_INVERT_NV = None

MGL_UNSIGNED_NORMALIZED = None

MGL_UNSIGNED_SHORT = None

MGL_UNSIGNED_SHORT_4_4_4_4_EXT = None

MGL_UNSIGNED_SHORT_5_5_5_1_EXT = None

MGL_V2F = None

MGL_V3F = None

MGL_VARIABLE_A_NV = None

MGL_VARIABLE_B_NV = None

MGL_VARIABLE_C_NV = None

MGL_VARIABLE_D_NV = None

MGL_VARIABLE_E_NV = None

MGL_VARIABLE_F_NV = None

MGL_VARIABLE_G_NV = None

MGL_VARIANT_ARRAY_EXT = None

MGL_VARIANT_ARRAY_POINTER_EXT = None

MGL_VARIANT_ARRAY_STRIDE_EXT = None

MGL_VARIANT_ARRAY_TYPE_EXT = None

MGL_VARIANT_DATATYPE_EXT = None

MGL_VARIANT_EXT = None

MGL_VARIANT_VALUE_EXT = None

MGL_VECTOR_EXT = None

MGL_VENDOR = None

MGL_VERSION = None

MGL_VERTEX_ARRAY = None

MGL_VERTEX_ARRAY_BUFFER_BINDING_ARB = None

MGL_VERTEX_ARRAY_COUNT_EXT = None

MGL_VERTEX_ARRAY_EXT = None

MGL_VERTEX_ARRAY_POINTER = None

MGL_VERTEX_ARRAY_POINTER_EXT = None

MGL_VERTEX_ARRAY_RANGE_LENGTH_NV = None

MGL_VERTEX_ARRAY_RANGE_NV = None

MGL_VERTEX_ARRAY_RANGE_POINTER_NV = None

MGL_VERTEX_ARRAY_RANGE_VALID_NV = None

MGL_VERTEX_ARRAY_RANGE_WITHOUT_FLUSH_NV = None

MGL_VERTEX_ARRAY_SIZE = None

MGL_VERTEX_ARRAY_SIZE_EXT = None

MGL_VERTEX_ARRAY_STRIDE = None

MGL_VERTEX_ARRAY_STRIDE_EXT = None

MGL_VERTEX_ARRAY_TYPE = None

MGL_VERTEX_ARRAY_TYPE_EXT = None

MGL_VERTEX_ATTRIB_ARRAY0_NV = None

MGL_VERTEX_ATTRIB_ARRAY10_NV = None

MGL_VERTEX_ATTRIB_ARRAY11_NV = None

MGL_VERTEX_ATTRIB_ARRAY12_NV = None

MGL_VERTEX_ATTRIB_ARRAY13_NV = None

MGL_VERTEX_ATTRIB_ARRAY14_NV = None

MGL_VERTEX_ATTRIB_ARRAY15_NV = None

MGL_VERTEX_ATTRIB_ARRAY1_NV = None

MGL_VERTEX_ATTRIB_ARRAY2_NV = None

MGL_VERTEX_ATTRIB_ARRAY3_NV = None

MGL_VERTEX_ATTRIB_ARRAY4_NV = None

MGL_VERTEX_ATTRIB_ARRAY5_NV = None

MGL_VERTEX_ATTRIB_ARRAY6_NV = None

MGL_VERTEX_ATTRIB_ARRAY7_NV = None

MGL_VERTEX_ATTRIB_ARRAY8_NV = None

MGL_VERTEX_ATTRIB_ARRAY9_NV = None

MGL_VERTEX_ATTRIB_ARRAY_BUFFER_BINDING_ARB = None

MGL_VERTEX_ATTRIB_ARRAY_ENABLED = None

MGL_VERTEX_ATTRIB_ARRAY_NORMALIZED = None

MGL_VERTEX_ATTRIB_ARRAY_POINTER = None

MGL_VERTEX_ATTRIB_ARRAY_SIZE = None

MGL_VERTEX_ATTRIB_ARRAY_STRIDE = None

MGL_VERTEX_ATTRIB_ARRAY_TYPE = None

MGL_VERTEX_PROGRAM = None

MGL_VERTEX_PROGRAM_BINDING_NV = None

MGL_VERTEX_PROGRAM_NV = None

MGL_VERTEX_PROGRAM_POINT_SIZE = None

MGL_VERTEX_PROGRAM_POINT_SIZE_NV = None

MGL_VERTEX_PROGRAM_TWO_SIDE = None

MGL_VERTEX_PROGRAM_TWO_SIDE_NV = None

MGL_VERTEX_SHADER_ARB = None

MGL_VERTEX_SHADER_BINDING_EXT = None

MGL_VERTEX_SHADER_EXT = None

MGL_VERTEX_SHADER_INSTRUCTIONS_EXT = None

MGL_VERTEX_SHADER_INVARIANTS_EXT = None

MGL_VERTEX_SHADER_LOCALS_EXT = None

MGL_VERTEX_SHADER_LOCAL_CONSTANTS_EXT = None

MGL_VERTEX_SHADER_OPTIMIZED_EXT = None

MGL_VERTEX_SHADER_VARIANTS_EXT = None

MGL_VERTEX_STATE_PROGRAM_NV = None

MGL_VERTEX_WEIGHTING_EXT = None

MGL_VERTEX_WEIGHT_ARRAY_EXT = None

MGL_VERTEX_WEIGHT_ARRAY_POINTER_EXT = None

MGL_VERTEX_WEIGHT_ARRAY_SIZE_EXT = None

MGL_VERTEX_WEIGHT_ARRAY_STRIDE_EXT = None

MGL_VERTEX_WEIGHT_ARRAY_TYPE_EXT = None

MGL_VIBRANCE_BIAS_NV = None

MGL_VIBRANCE_SCALE_NV = None

MGL_VIEWPORT = None

MGL_VIEWPORT_BIT = None

MGL_WEIGHT_ARRAY_BUFFER_BINDING_ARB = None

MGL_WRITE_ONLY_ARB = None

MGL_W_EXT = None

MGL_XOR = None

MGL_X_EXT = None

MGL_Y_EXT = None

MGL_ZERO = None

MGL_ZERO_EXT = None

MGL_ZOOM_X = None

MGL_ZOOM_Y = None

MGL_Z_EXT = None

MMatrix_kTol = None

MPoint_kTol = None

MTransformationMatrix_kTol = None

MVector_kTol = None

NULL = None

PLUGIN_COMPANY = None

TEXTURE_COORD_ARRAY_BUFFER_BINDING_ARB = None

cvar = None

kA8 = None

kA8B8G8R8 = None

kB5G5R5A1 = None

kB5G6R5 = None

kB8G8R8A8 = None

kB8G8R8X8 = None

kCubeMap = None

kD24S8 = None

kD24X8 = None

kD32_FLOAT = None

kDXT1_UNORM = None

kDXT1_UNORM_SRGB = None

kDXT2_UNORM = None

kDXT2_UNORM_PREALPHA = None

kDXT2_UNORM_SRGB = None

kDXT3_UNORM = None

kDXT3_UNORM_PREALPHA = None

kDXT3_UNORM_SRGB = None

kDXT4_SNORM = None

kDXT4_UNORM = None

kDXT5_SNORM = None

kDXT5_UNORM = None

kDefaultNodeType = None

kDepthTexture = None

kEnvCrossHoriz = None

kEnvCrossVert = None

kEnvCubemap = None

kEnvHemiSphere = None

kEnvLatLong = None

kEnvNone = None

kEnvSphere = None

kEulerRotationEpsilon = None

kImage1D = None

kImage1DArray = None

kImage2D = None

kImage2DArray = None

kL16 = None

kL8 = None

kMFnMeshInstanceUnspecified = None

kMFnMeshPointTolerance = None

kMFnMeshTolerance = None

kMFnNurbsEpsilon = None

kMFnSubdPointTolerance = None

kMFnSubdTolerance = None

kMGLext_ARB_OpenMGL20 = None

kMGLext_ARB_color_buffer_float = None

kMGLext_ARB_depth_texture = None

kMGLext_ARB_fragment_program = None

kMGLext_ARB_fragment_program_shadow = None

kMGLext_ARB_half_float_pixel = None

kMGLext_ARB_matrix_palette = None

kMGLext_ARB_occlusion_query = None

kMGLext_ARB_point_parameters = None

kMGLext_ARB_shadow = None

kMGLext_ARB_shadow_ambient = None

kMGLext_ARB_texgen_reflection = None

kMGLext_ARB_texture_env_crossbar = None

kMGLext_ARB_texture_float = None

kMGLext_ARB_texture_non_power_of_two = None

kMGLext_ARB_texture_rectangle = None

kMGLext_ARB_vertex_blend = None

kMGLext_ARB_vertex_buffer_object = None

kMGLext_ARB_vertex_program = None

kMGLext_ATI_fragment_shader = None

kMGLext_ATI_pixel_format_float = None

kMGLext_EXT_compiled_vertex_array = None

kMGLext_EXT_cull_vertex = None

kMGLext_EXT_fog_coord = None

kMGLext_EXT_secondary_color = None

kMGLext_EXT_texture_filter_anisotropic = None

kMGLext_EXT_vertex_shader = None

kMGLext_EXT_vertex_weighting = None

kMGLext_MGLX_choose_fbconfig = None

kMGLext_MGLX_choose_fbconfig_sgix = None

kMGLext_MGLX_create_context_with_config_sgix = None

kMGLext_MGLX_create_new_context = None

kMGLext_MGLX_create_pbuffer = None

kMGLext_MGLX_create_pbuffer_sgix = None

kMGLext_MGLX_destroy_pbuffer = None

kMGLext_MGLX_destroy_window = None

kMGLext_MGLX_get_visual_from_fbconfig_sgix = None

kMGLext_NUMBER_OF_EXTENSIONS = None

kMGLext_NV_fence = None

kMGLext_NV_float_buffer = None

kMGLext_NV_fragment_program = None

kMGLext_NV_occlusion_query = None

kMGLext_NV_primitive_restart = None

kMGLext_NV_register_combiners = None

kMGLext_NV_texture_shader = None

kMGLext_NV_vertex_array_range = None

kMGLext_NV_vertex_program = None

kMGLext_SGIS_generate_mipmap = None

kMGLext_SGIX_depth_texture = None

kMGLext_SGIX_shadow = None

kMGLext_WMGL_ARB_buffer_region = None

kMGLext_WMGL_ARB_extensions_string = None

kMGLext_WMGL_ARB_make_current_read = None

kMGLext_WMGL_ARB_pbuffer = None

kMGLext_WMGL_ARB_pixel_format = None

kMGLext_WMGL_ARB_render_texture = None

kMGLext_WMGL_NV_allocate_memory = None

kMGLext_bgra = None

kMGLext_blend_color = None

kMGLext_blend_minmax = None

kMGLext_blend_subtract = None

kMGLext_color_matrix = None

kMGLext_color_table = None

kMGLext_convolution = None

kMGLext_draw_range_elements = None

kMGLext_frame_buffer_object = None

kMGLext_histogram = None

kMGLext_imaging_subset = None

kMGLext_multi_draw_arrays = None

kMGLext_multisample = None

kMGLext_multitexture = None

kMGLext_packed_pixels = None

kMGLext_rescale_normal = None

kMGLext_separate_specular_color = None

kMGLext_texture3D = None

kMGLext_texture_border_clamp = None

kMGLext_texture_compression = None

kMGLext_texture_compression_s3tc = None

kMGLext_texture_cube_map = None

kMGLext_texture_edge_clamp = None

kMGLext_texture_env_add = None

kMGLext_texture_env_combine = None

kMGLext_texture_env_dot3 = None

kMGLext_texture_lod = None

kMGLext_transpose_matrix = None

kNumberOfEnvMapTypes = None

kNumberOfRasterFormats = None

kNumberOfTextureTypes = None

kQuaternionEpsilon = None

kR10G10B10A2_UINT = None

kR10G10B10A2_UNORM = None

kR16G16B16A16_FLOAT = None

kR16G16B16A16_SINT = None

kR16G16B16A16_SNORM = None

kR16G16B16A16_UINT = None

kR16G16B16A16_UNORM = None

kR16G16_FLOAT = None

kR16G16_SINT = None

kR16G16_SNORM = None

kR16G16_UINT = None

kR16G16_UNORM = None

kR16_FLOAT = None

kR16_SINT = None

kR16_SNORM = None

kR16_UINT = None

kR16_UNORM = None

kR1_UNORM = None

kR24G8 = None

kR24X8 = None

kR32G32B32A32_FLOAT = None

kR32G32B32A32_SINT = None

kR32G32B32A32_UINT = None

kR32G32B32_FLOAT = None

kR32G32B32_SINT = None

kR32G32B32_UINT = None

kR32G32_FLOAT = None

kR32G32_SINT = None

kR32G32_UINT = None

kR32_FLOAT = None

kR32_SINT = None

kR32_UINT = None

kR8G8B8A8_SINT = None

kR8G8B8A8_SNORM = None

kR8G8B8A8_UINT = None

kR8G8B8A8_UNORM = None

kR8G8B8X8 = None

kR8G8_SINT = None

kR8G8_SNORM = None

kR8G8_UINT = None

kR8G8_UNORM = None

kR8_SINT = None

kR8_SNORM = None

kR8_UINT = None

kR8_UNORM = None

kR9G9B9E5_FLOAT = None

kUnknownParameter = None

kVolumeTexture = None
